# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import stripe

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User
from django.contrib.auth.backends import ModelBackend

from u.models import UserProfile, is_pro_user

from promo.forms import UserForm, GoProForm, user_is_special
from promo.emails import send_confirm_email_address, confirm_token


def index(request):
    return render(request, 'promo/index.html')


def signup(request):
    if request.method == 'POST':
        form = UserForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            # Hash password
            user.set_password(request.POST['password'])
            user.save()

            profile = UserProfile(user=user, name=user.username)
            profile.save()

            user = authenticate(username=user.username,
                                password=request.POST['password'])
            login(request, user)

            send_confirm_email_address(user, profile)

            plan = request.GET.get('plan')
            if plan == 'free':
                return HttpResponseRedirect(reverse(
                    'u:userpage', kwargs={'username': user.username}))
            elif plan == 'pro':
                return HttpResponseRedirect(reverse('promo:gopro'))
            else:
                return HttpResponseRedirect(reverse('promo:chooseplan'))
    else:
        form = UserForm()

    return render(request, 'promo/signup.html',
                  context={'form': form})

def choose_plan(request):
    return render(request, 'promo/choose-plan.html', context={})

def go_pro(request):
    if request.method == 'POST':
        form = GoProForm(request.user, data=request.POST)

        if is_pro_user(request.user):
            form.add_error(None, 'You are already a Pro user')
        elif form.is_valid():
            profile = UserProfile.objects.get(user=request.user)
            try:
                customer = stripe.Customer.create(
                    source=request.POST['token'],
                    plan=request.POST['plan'],
                    email=request.user.email
                )
            except stripe.error.CardError as e:
                error = e.json_body['error']
                form.add_error(None, error['message'])
            else:
                profile.stripe_customer = customer['id']
                profile.has_pro = True
                profile.save()
                return HttpResponseRedirect(reverse(
                    'u:userpage', kwargs={'username': request.user.username}))
    else:
        form = GoProForm(request.user)
    return render(request, 'promo/go-pro.html',
                  context={'form': form,
                           'special_user': user_is_special(request.user)})

def validate_email(request, token):
    data = confirm_token(token)
    if data:
        username = data['username']
        user = get_object_or_404(User, username=username)
        profile = get_object_or_404(UserProfile, user=user)
        profile.has_validated_email = True
        profile.save()
        return HttpResponseRedirect(reverse('u:userpage',
                                            kwargs={'username': username}))
    return render(request, 'promo/invalidtoken.html')


@login_required()
def accounts_profile(request):
    username = request.user.username
    return HttpResponseRedirect(reverse('u:userpage',
                                        kwargs={'username': username}))


class LoginBackend(ModelBackend):
    def authenticate(self, username=None, password=None, **kwargs):
        username = username.lower() if username is not None else None
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            try:
                user = User.objects.get(email=username)
            except User.DoesNotExist:
                return None

        if getattr(user, 'is_active', False) and user.check_password(password):
            return user
        return None
