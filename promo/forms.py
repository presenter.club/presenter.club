# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.forms import (ModelForm, CharField, PasswordInput, ValidationError,
                          Form, ChoiceField)

from django.contrib.auth.models import User

from u.models import UserProfile


class UserForm(ModelForm):
    password = CharField(widget=PasswordInput())

    class Meta:
        model = User
        fields = ['username', 'email', 'password']
        help_texts = {
            'username': ('Up to 30 characters '
                         'long, only using letters, digits or "@.+-_".'),
        }

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email):
            raise ValidationError('The email "%(email)s" is already signed up',
                                  code='dup-email',
                                  params={'email': email})
        return email

    def clean_username(self):
        username = self.cleaned_data['username'].lower()
        if User.objects.filter(username=username):
            raise ValidationError('The username "%(name)s" is already taken',
                                  code='dup-username',
                                  params={'name': username})
        return username


def user_is_special(user) -> bool:
    return user.username in ['darwinpintado']

class GoProForm(Form):
    plan = ChoiceField(choices=[('pro-a', 'Pro Yearly'),
                                ('pro-m', 'Pro Monthly')])
    token = CharField()

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        choices = [('pro-a', 'Pro Yearly'),
                   ('pro-m', 'Pro Monthly')]
        if user_is_special(user):
            choices.append(('pro-a-special', 'Pro Yearly (Special)'))
        self.fields['plan'] = ChoiceField(choices=choices)
