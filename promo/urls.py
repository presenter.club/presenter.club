# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import url
from django.views.generic import TemplateView

from . import views

app_name = 'promo'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^signup$', views.signup, name='signup'),
    url(r'^signup/choose-plan$', views.choose_plan, name='chooseplan'),
    url(r'^go-pro$', views.go_pro, name='gopro'),
    url(r'^accounts/validateemail?token=(?P<token>.+)$',
        views.validate_email,
        name='validateemail'),
    url(r'^accounts/profile/$', views.accounts_profile),

    url(r'^terms$', TemplateView.as_view(template_name='promo/terms.html')),
    url(r'^plans$', TemplateView.as_view(template_name='promo/plans.html'),
        name='plans'),
]
