# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.test import TestCase
from django.test.client import Client
import unittest

from u.tests import TestUserTestCase
from .forms import GoProForm


class TestLogin(TestUserTestCase):
    def test_loginform(self):
        resp = self._c.get('/accounts/login/')
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'Username')
        self.assertContains(resp, 'Password')

    @unittest.skip('TODO: How do we test this properly?')
    def test_dologin(self):
        c = Client()
        resp = c.post('/accounts/login/',
                      {'username': 'test', 'password': 'test'})
        assert False

class TestSignupPester(TestUserTestCase):
    def test_is_visible_loggedout(self):
        resp = self._c.get('/')
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'SignupShovel')

    def test_is_not_visible_loggedin(self):
        resp = self._logged_in.get('/')
        self.assertEqual(resp.status_code, 200)
        self.assertNotContains(resp, 'SignupShovel')


class TestSignup(TestCase):
    def test_signup_form(self):
        c = Client()
        resp = c.get('/signup')
        self.assertContains(resp, 'Name')
        self.assertContains(resp, 'Username')
        self.assertContains(resp, 'Email')
        self.assertContains(resp, 'Password')

    def test_signup_free(self):
        c = Client()
        resp = c.post('/signup?plan=free',
                      {'name': 'Test Testingson',
                       'username': 'test12',
                       'email': 'test122@example.com',
                       # Best password
                       'password': 'letmein'})
        self.assertRedirects(resp, '/u/test12/')

    def test_signup_pro(self):
        c = Client()
        resp = c.post('/signup?plan=pro',
                      {'name': 'Test Testingson',
                       'username': 'test123',
                       'email': 'test123@example.com',
                       # Best password
                       'password': 'letmein'})
        self.assertRedirects(resp, '/go-pro')

    def test_signup(self):
        c = Client()
        resp = c.post('/signup', {'name': 'Test Testingson',
                                  'username': 'test1',
                                  'email': 'test1@example.com',
                                  # Best password
                                  'password': 'letmein'})
        self.assertRedirects(resp, '/signup/choose-plan')

        resp = c.get('/u/test1/')
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'New Presentation')
        self.assertContains(resp, 'Your Presentations')

    def test_signup_dup_names(self):
        c = Client()
        resp = c.post('/signup', {'name': 'Test Testingson',
                                  'username': 'test2',
                                  'email': 'test2@example.com',
                                  'password': 'letmein'})
        resp = c.post('/signup', {'name': 'Different Person',
                                  'username': 'TeSt2',
                                  'email': 'test22@example.com',
                                  'password': 'letmein'})
        self.assertEqual(resp.status_code, 200)
        self.assertFormError(resp, 'form', 'username',
                             'The username "test2" is already taken')

    def test_signup_dup_email(self):
        c = Client()
        resp = c.post('/signup', {'name': 'Test Testingson',
                                  'username': 'test3',
                                  'email': 'test3@ex.co',
                                  'password': 'letmein'})
        resp = c.post('/signup', {'name': 'Different Person',
                                  'username': 'test3',
                                  'email': 'test3@ex.co',
                                  'password': 'passwordmeme'})
        self.assertEqual(resp.status_code, 200)
        self.assertFormError(resp, 'form', 'email',
                             'The email "test3@ex.co" is already signed up')


class TestGoProForm(TestUserTestCase):
    def test_choices_for_normal_user(self):
        form = GoProForm(self.user)
        self.assertEqual(len(form.fields['plan'].choices), 2)

    def test_normal_user_can_not_get_special(self):
        form = GoProForm(self.user,
                         data={'token': 'abc', 'plan': 'pro-a-special'})
        self.assertEqual(form.is_valid(), False)

    def test_choices_for_special_user(self):
        s = self._create_user('darwinpintado', 'test', 't@e.st')
        form = GoProForm(s)
        self.assertEqual(len(form.fields['plan'].choices), 3)

    def test_special_user_can_get_special(self):
        s = self._create_user('darwinpintado', 'test', 't@e.st')
        form = GoProForm(s, data={'token': 'abc', 'plan': 'pro-a-special'})
        self.assertEqual(form.is_valid(), True)
