# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
from base64 import b32encode, b32decode

from django.core.signing import TimestampSigner, BadSignature, SignatureExpired
from django.core.mail import send_mail
from django.core.urlresolvers import reverse


_confirm_email_signer = TimestampSigner(salt='_confirm_email_signer')


def send_confirm_email_address(user, profile):
    token = _confirm_email_signer.sign(json.dumps({
        'username': user.username
    }))
    token = b32encode(token.encode('utf8'))
    url = reverse('promo:validateemail', args=[token])

    text = ('Hi {}\n\n'
            'Thanks for signing up to Presenter Club.  Please verify your '
            'email address by clicking the following link:\n\n'
            '    https://www.presenter.club{}\n\n'
            'If you did not sign up to Presenter Club, please ignore this '
            'email.  Sorry for the inconvenience.\n\n'
            'Thanks,\nSam - Presenter Club').format(profile.name, url)
    send_mail('Confirm Email for Presenter Club', text,
              'sam@presenter.club', [user.email])


_MAX_AGE = 365 * 24 * 60 * 60  # 1 year???
def confirm_token(token):
    token = b32decode(token.encode('utf8')).decode('utf8')
    try:
        return json.loads(_confirm_email_signer.unsign(token,
                                                       max_age=_MAX_AGE))
    except (BadSignature, SignatureExpired):
        return False
