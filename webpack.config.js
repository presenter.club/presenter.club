/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker')
 
module.exports = {
    entry: {
        u_view: './static/jsx/entrypoints/u_view.jsx',
        u_edit: './static/jsx/entrypoints/u_edit.jsx',
        u_genthumb: './static/jsx/entrypoints/u_genthumb.jsx',
        style_edit: './static/jsx/entrypoints/style_edit.jsx',
        style_genthumb: './static/jsx/entrypoints/style_genthumb.jsx',
    },
    output: {
        path: './static/webpack_bundles/',
        filename: '[name].js',
    },

    plugins: [
        new BundleTracker({
            filename: './webpack-stats.json'
        }),
    ],
    module: {
        loaders: [
            { test: /\.jsx$/,
              exclude: /node_modules/,
              loader: 'babel-loader' },
            /*{test: /(?!node_modules)\/.*\.js$/,
             loader: 'babel',
             query: {
                presets: ['es2015']
             }
            }*/
        ],
        noParse: /node_modules\/quill\/dist/
    },
    devtool: 'source-map'
};
