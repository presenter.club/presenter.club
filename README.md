# Presenter Club
## Make Great Presentations, Faster

<!-- FIXME:  Don't hotlink vidme -->
<a href='https://www.presenter.club/'>
![Presenter Club - Sign up for free](https://d1wst0behutosd.cloudfront.net/channel_covers/15943164.jpg?v1r1474947335)
</a>

Sign up and learn more about Presenter Club at https://www.presenter.club/

Presenter Club is a presentation maker web app.  This repo contains the full
source code; as used wwww.presenter.club.`

## License

Presenter Club is licensed under the AGPLv3.0.  The scss style is drived from
[a html5up template](https://html5up.net/uploads/demos/solid-state/), which is
[CC BY 3.0](https://html5up.net/license).  Obviously all changes on top of the
original style are AGPLv3.0.

## Feedback!

All feedback is appreciated!  You can use the live chat on the production
website (www.presenter.club), email us (hi@presenter.club) or post on GitLab
issues (https://gitlab.com/presenter.club/presenter.club).

## Technical Overview

This repo is the source behind the whole web application.  The stack is a django
backend.  Many of the boring pages (user settings, presentations, etc.) are
just plain html.  The interactive pages, and all pages with slides on them,
are using the React/ES6/Redux.  Styles are SCSS, adapted from a HTML5UP theme.

Thumbnail generation for presentations and styles is a unique part of our
stack.  The thumbnails are generated in the background.  They are web page
screenshots, rendered in a WebKitGtk 4.0 window (that means Gtk+ 3, WebKit 2).
It then screenshots the X display (not Wayland yet).

The django is structured into the following apps:

* promo - the home and signup pages (think "promotion")
* u - the userpages and presentations (think basic /u/ urls)
* styles - user styles (think /u/me/styles/ urls)
* imagesearch - the curated background search function
* medialibrary - each user's medialibrary

I (sam@sam.today) am happy to write up more technical docs if they are needed.
Just send me an email and I'll check it out.

## Environment Setup

* OS Packages for python3, npm, sass compiler, WebKitGtk+ and the python
  gobject bindings.  From Fedora 24 Server, that means installing:
    - epiphany
    - xorg-x11-server-Xvfb
    - python3-gobject
    - ImageMagick
    - nodejs
    - npm
    - rubygem-sass
    - python3-django
    - python3-gunicorn
    - python3-psycopg2
    - python3-pillow
    - python3-pip
    - python3-bleach
* `pip3 install -r requirements.txt`
* `npm update`

## Developing

This is a django app, so you need to run migrations and stuff.  By default,
it just uses the development setup - which is a sqlite database.

You need to run 4 scripts at the same time when developing, all of them in
the root of the repository:

1. `python3 manage.py runserver`
2. `python3 manage.py process_tasks`
3. `webpack --watch` (this compiles the JS)
4. `./watch-css` (this compiles the scss)

Another tip - by default the stripe is in test mode.  So to make yourself pro,
just use the credit card `4242 4242 4242 4242` with any CVC, billing zip and
any expiry date that is in the future.  [See the stripe page for more info](https://stripe.com/docs/testing#cards).

## Testing

The django app has good tests!  Please help maintain them!

The react app has no tests :'(  Suggestions are appreciated as we're 100% open
to testing.

## Style

Scss should be written in BEM style.  A careful observer would notice that only
the new css is BEM, older stuff is from a template that was not BEM.  Porting
is an aim.

Python should be flake8 - you know, it needs to be pythonic!

I'm not sure how to do style guides for JS, ideas appreciated.
