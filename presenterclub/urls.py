# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls.static import static
from django.conf.urls import url, include
from django.conf import settings
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url('^accounts/', include('django.contrib.auth.urls')),
    url('', include('promo.urls')),
    url(r'^u/(?P<username>.+)/medialibrary/', include('medialibrary.urls')),
    url(r'^u/(?P<username>.+)/styles/', include('styles.urls')),
    url(r'^u/', include('u.urls')),
    url(r'^imagesearch/', include('imagesearch.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
