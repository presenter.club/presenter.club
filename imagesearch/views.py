# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import hashlib
import requests
from requests_futures.sessions import FuturesSession

from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse

from imagesearch.models import CuratedImage


_session = FuturesSession()
_session.timeout = 2

SPLASHBASE_URL = 'https://splashbase.herokuapp.com/api/v1/images/search'
def _decode_splashbase(session, resp):
    data = resp.text
    j = resp.json()
    resp.data = []
    for image in j['images']:
        if image['copyright'] != 'CC0':
            print('NON CC0 image:', image)
            continue
        # The large_url is gigabytes, and not all photos have one
        # so we just use the url
        resp.data.append({'url': image['url'],
                          'type': 'splashbase'})

UNSPLASH_URL = 'https://api.unsplash.com/photos/search/'
UNSPLASH_CID = '4916cd19af23f42278d239afb01590f101026095721b38ff7502b23ed1de870b'
def _decode_unsplash(session, resp):
    j = resp.json()
    resp.data = []
    for image in j:
        resp.data.append({'thumb': image['urls']['thumb'],
                          'full':  image['urls']['regular'],
                          'massive':  image['urls']['full'],
                          'id': image['id'],
                          'type': 'unsplash'})

FLICKR_GOOD_LICENSES = ','.join([
  '4',  # https://creativecommons.org/licenses/by/2.0/
  '7',  # https://www.flickr.com/commons/usage/
  '9',  # CC0
  '10', # CC Public Domain Mark 1.0
])
FLICKR_URL = 'https://api.flickr.com/services/rest/?format=json&nojsoncallback=1&method=flickr.photos.search'
FLICKR_CID = '97cd0880cfaa21d3adce42206f36bda1'
def _decode_flickr(session, resp):
    j = resp.json()
    resp.data = []
    for image in j['photos']['photo']:
        resp.data.append({'type': 'flickr',
                          'farm': image['farm'],
                          'server': image['server'],
                          'id': image['id'],
                          'secret': image['secret'],
                          'ownername': image['ownername'],
                          'license': image['license']})

def search(request):
    term = request.GET['q'].lower()

    reqs = []
    reqs.append(_session.get(
        SPLASHBASE_URL,
        background_callback=_decode_splashbase,
        params={'query': term}))
    reqs.append(_session.get(
        UNSPLASH_URL,
        background_callback=_decode_unsplash,
        params={'client_id': UNSPLASH_CID, 'query': term}))

    params = {
        'api_key': FLICKR_CID,
        'text': term.encode('utf8'),
        'license': FLICKR_GOOD_LICENSES,
        # When h (1600x1600+) type photos were introduced
        'min_upload_date': '1330604836',
        'per_page': 20,
        'extras': 'license,owner_name'
    }
    reqs.append(_session.get(
        FLICKR_URL,
        background_callback=_decode_flickr,
        params=params))

    results = []
    for req in reqs:
        r = None
        try:
            r = req.result().data
        except requests.exceptions.ConnectionError:
            print('Error with', req)
        except requests.exceptions.Timeout:
            print('Error with', req)
        results += r

    #specials = CuratedImage.objects.filter(tags__contains=term)
    #for s in specials:
    #    results.insert(0, {'thumb': css_url(s.thumb_url or s.full_url),
    #                       'full':  css_url(s.full_url),
    #                       'attr':  s.attr})
    return JsonResponse({'data': results})

def curated_get(request, id):
    i = get_object_or_404(CuratedImage, id=id)
    return JsonResponse(i.to_json())
