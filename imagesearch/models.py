# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models

from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill


class CuratedImage(models.Model):
    tags = models.TextField('Tags')
    full = models.FileField(upload_to='curated/%Y-%m/')
    thumb = ImageSpecField(source='full',
                           processors=[ResizeToFill(192, 108)],
                           format='JPEG',
                           options={'quality': 60})

    LICENSES = {
        '0': 'CC0',
        'b': 'CC BY SA 3.0',
    }
    license = models.CharField(
        'License',
        max_length=1,
        choices=list(LICENSES.items())
    )

    attr_text = models.TextField('Attribution text')
    attr_url = models.TextField('Attribution url')

    tile = models.BooleanField('If the image should be a tile', default=False)

    def to_json(self):
        return {'full': self.full.url,
                'thumb': self.thumb.url,
                'attrText': self.attr_text,
                'attrURL': self.attr_url,
                'tile': self.tile,
                'id': self.id }
