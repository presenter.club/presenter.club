# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
from django import template
from u.models import is_pro_user, Presentation, UserProfile

register = template.Library()

@register.filter
def isprouser(user):
    return is_pro_user(user)

@register.filter(is_safe=True)
def jsonify(value):
    return json.dumps(value)

@register.filter
def profile__onboarding_status(user):
    return UserProfile.objects.get(user=user).onboarding_status

@register.filter
def user__n_presentations(user):
    return Presentation.objects.filter(user=user).count()
