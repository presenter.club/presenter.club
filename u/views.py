# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import time

from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, JsonResponse, Http404
from django.forms import ModelForm, CharField, Form, Textarea

from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles.storage import staticfiles_storage
from django.contrib.auth.models import User

from u.decorators import personal_user_view, presentation_view
from u.models import UserProfile, Presentation, is_pro_user
from u.tasks import generate_thumbnail, GENTHUMB_KEY

def index(request):
    return render(request, 'u/index.html')


def userpage(request, username):
    viewing = get_object_or_404(User, username=username.lower())
    profile = get_object_or_404(UserProfile, user=viewing)
    p_filter = [Presentation.PUBLIC, Presentation.NOTES_HIDDEN]
    if request.user == viewing:
        p_filter.append(Presentation.PRIVATE)

    presentations = Presentation.objects.filter(user=viewing,
                                                privacy__in=p_filter) \
                                        .order_by('-edited_time')
    return render(request, 'u/userpage-presentations.html',
                  {'viewing': viewing, 'profile': profile,
                   'presentations': presentations,
                   'is_pro': is_pro_user(viewing),
                   'page': 'presentations'})

@personal_user_view
def onboarding_status(request, user):
    profile = get_object_or_404(UserProfile, user=user)
    if request.method == 'POST':
        j = json.loads(request.body.decode('utf8'))
        assert j['status'] in [0, 1]
        profile.onboarding_status = j['status']
        profile.save()
    return JsonResponse({'status': profile.onboarding_status})


class EditProfileForm(ModelForm):
    name = CharField(help_text='Displayed on your profile page')

    class Meta:
        model = UserProfile
        fields = ['name', 'bio']

@personal_user_view
def user_settings(request, user):
    profile = get_object_or_404(UserProfile, user=user)

    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(
                reverse('u:userpage', args=[user.username]))
    else:
        form = EditProfileForm(instance=profile)
    return render(request, 'u/usersettings.html',
                  {'profile_form': form})

@personal_user_view
def user_plan(request, user):
    if is_pro_user(user):
        return render(request, 'u/userplan.html')
    else:
        return HttpResponseRedirect(reverse('promo:chooseplan'))

def can_make_presentations(f):
    @login_required
    def wrap(request, *args, **kwargs):
        return f(request, *args, **kwargs)
    return wrap

@can_make_presentations
def create(request, username):
    p = Presentation(title='New Presentation', user=request.user)
    if is_pro_user(request.user):
        p.privacy = p.PRIVATE
    p.save()
    return HttpResponseRedirect(reverse('u:edit', args=[username, p.id]))


def save_presentation_json(request, p, data) -> 'error message or None':
    is_pro = is_pro_user(p.user)

    j = json.loads(data)
    j = Presentation.bleach_data(j)

    metadata = j['metadata']
    p.title = metadata['title']
    if metadata['privacy'] == Presentation.PRIVATE and not is_pro:
        return 'You need to be a PRO user to set the privacy to private'
    p.privacy = metadata['privacy']
    if p.privacy not in [c for c, n in Presentation.PRIVACY_CHOICES]:
        return 'Choose a valid privacy option'
    del j['metadata']

    speech = j.get('speech', [])
    p.number_rows = len(speech)
    p.number_rows_slide = 0
    for row in speech:
        if row.get('slideType') is not None:
            p.number_rows_slide += 1
    p.data = json.dumps(j)

    generate_thumbnail(p.id, request.get_host())
    p.save()
    return None


class CreatePresentationJSONForm(Form):
    json = CharField(widget=Textarea)


@can_make_presentations
def create_json(request, username):
    if request.method == 'POST':
        form = CreatePresentationJSONForm(request.POST)
        if form.is_valid():
            p = Presentation(user=request.user)
            error = save_presentation_json(request, p,
                                           form.cleaned_data['json'])
            if error is None:
                return HttpResponseRedirect(reverse(
                    'u:edit',
                    args=[p.user.username, p.id]))
            else:
                form.add_error(None, error)
    else:
        form = CreatePresentationJSONForm()
    return render(request, 'u/createjson.html', {'form': form})


def json_escape(json_string):
    'Make a string I can put into a script tag and pray it is safe'
    return json_string.replace('<', '\\u003c') \
                      .replace('>', '\\u003e') \
                      .replace('&', '\\u0026') \
                      .replace('\u2028', '\\u2028') \
                      .replace('\u2029', '\\u2029')


@can_make_presentations
def edit(request, username, presentationid):
    p = get_object_or_404(Presentation, id=presentationid)
    if p.user != request.user:
        raise PermissionDenied
    profile = get_object_or_404(UserProfile, user=p.user)

    if request.method == 'GET':
        d = _presentation_json(p, request.user)
        json_string = json_escape(json.dumps(d))
        return render(request, 'u/app.html',
                      context={'presentation': p,
                               'profile': profile,
                               'viewing': p.user,
                               'json_string': json_string})
    else:
        error = save_presentation_json(request, p, request.body.decode('utf8'))
        if error is None:
            return JsonResponse({'success': True})
        else:
            return JsonResponse({'success': False, 'msg': error})


def _presentation_json(p, user):
    d = json.loads(p.data)
    d['metadata'] = {
        'title': p.title,
        'privacy': p.privacy
    }
    slides = []
    if p.privacy != Presentation.PUBLIC and p.user != user:
        for row in d['speech']:
            row['indent'] = None
            row['idea'] = None
            row['execution'] = None
            slides.append(row)
        d['speech'] = slides
    return d

@presentation_view
def view_json(request, p):
    return JsonResponse(_presentation_json(p, request.user))

@presentation_view
def view(request, p):
    profile = get_object_or_404(UserProfile, user=p.user)

    d = _presentation_json(p, request.user)
    json_string = json_escape(json.dumps(d))

    thumb = request.build_absolute_uri(reverse(
        'u:thumb', args=[p.user.username, p.id]))
    # Cache busting
    thumb = thumb + '?t={}'.format(int(time.time()))

    return render(request, 'u/view.html',
                  context={'presentation': p,
                           'viewing_user': p.user,
                           'thumb_url': thumb,
                           'viewing_profile': profile,
                           'json_string': json_string})


def genthumb(request, username, presentationid, key):
    if key != GENTHUMB_KEY:
        raise PermissionDenied

    p = get_object_or_404(Presentation, id=presentationid)
    json_string = json_escape(p.data)
    return render(request, 'u/thumbs.html',
                  context={'json_string': json_string,
                           'presentation': p})

@presentation_view
def thumb(request, p):
    if p.number_rows_slide == 0 or not p.thumb:
        return HttpResponseRedirect(
            staticfiles_storage.url('images/emptythumb.svg'))

    return HttpResponseRedirect(p.thumb.url)


class DeletePresentationForm(Form):
    pass

@can_make_presentations
def delete_presentation(request, username, presentationid):
    p = get_object_or_404(Presentation, id=presentationid)
    if p.user != request.user:
        raise PermissionDenied

    if request.method == 'GET':
        return render(request, 'u/deletepresentation.html',
                      context={'p': p,
                               'form': DeletePresentationForm(),
                               'is_pro': is_pro_user(request.user)})
    else:
        p.delete()
        return HttpResponseRedirect(
            reverse('u:userpage', args=[username]))
