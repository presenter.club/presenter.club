# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from base64 import b32encode
import os
import tempfile

# Force gtk to use X11 if wayland is running
os.environ['WAYLAND_DISPLAY'] = ''

# Need to setup virtual display BEFORE Gtk is imported
from pyvirtualdisplay import Display
Display(visible=0, size=(320, 180)).start()

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('WebKit2', '4.0')
from gi.repository import WebKit2
from gi.repository import GLib

from django.core import signing
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.core.files.base import ContentFile
from background_task import background

from u.models import Presentation


#GENTHUMB_KEY = b32encode( \
#    signing.dumps(1, salt='u.views._genthumb_signer').encode('utf8')) \
#    .decode('utf8')
GENTHUMB_KEY = 'FIXMECONSTANT'  # FIXME:  Constants are bad

class WebView():
    def __init__(self):
        self._setup = False

    def setup(self):
        self._win = Gtk.Window()
        self._win.set_default_size(320, 180)
        self._win.connect('destroy', Gtk.main_quit)

        self._webview = WebKit2.WebView()
        self._webview.connect('load-changed', self.__load_changed_cb)

        self._win.add(self._webview)
        self._win.show_all()
        self._win.move(0, 0)

    def __enter__(self):
        if not self._setup:
            self.setup()
        return self

    def load(self, uri):
        self._webview.load_uri(uri)
        Gtk.main()
        return self._screenshot

    def __exit__(self, *args):
        return self

    def __load_changed_cb(self, webview, load_event):
        if load_event == WebKit2.LoadEvent.FINISHED:
            if not webview.props.uri:
                # What did we actually load?  Nothing.
                return
            # Have to add a timeout so that it actually renders
            GLib.timeout_add(500, self.__timeout_cb)

    def __timeout_cb(self):
        fd, fp = tempfile.mkstemp(suffix='.png')
        os.system('import -window root {}'.format(fp))
        with open(fp, 'rb') as f:
            self._screenshot = f.read()
        os.remove(fp)
        Gtk.main_quit()
_webview = WebView()
def get_webview():
    return _webview


@background(queue='thumbnails')
def generate_thumbnail(presentation_id, host):
    try:
        presentation = Presentation.objects.get(id=presentation_id)
    except Presentation.DoesNotExist:
        return

    if presentation.number_rows_slide == 0:
        presentation.thumb.delete(save=False)
        presentation.thumb = None
        presentation.save()
        return

    if presentation.thumb_edited_time == presentation.edited_time:
        print('Thumb update not needed')
        return 

    url = 'http://' + host + reverse('u:genthumb',
                         args=[presentation.user.username,
                               presentation.id,
                               GENTHUMB_KEY])
    with get_webview() as wv:
        thumb = wv.load(url)
    presentation.thumb.delete(save=False)
    presentation.thumb.save('', ContentFile(thumb))
    presentation.thumb_edited_time = presentation.edited_time
    presentation.save()
