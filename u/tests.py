# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import json
import copy
import mock
from django.test import TestCase
from django.test import Client
from u.models import User, UserProfile, Presentation


@mock.patch('u.tasks.generate_thumbnail')
def generate_thumbnail(*args):
    # This is really slow, so skip it to make the rests fast
    pass


class TestUserTestCase(TestCase):
    def _create_user(self, username, password, email):
        u = User.objects.create(
            username=username,
            password=password,
            email=email)
        UserProfile.objects.create(
            user=u,
            has_validated_email=True,
            name=username)
        return u

    def setUp(self):
        self.other_user = User.objects.create(
            username='nottest',
            password='test',
            email='NOTtest@example.com')
        UserProfile.objects.create(
            user=self.other_user,
            has_validated_email=True,
            name='Other User',
            bio='I DONT have a Bio')
        self.user = User.objects.create(
            username='test', 
            password='test',
            email='test@example.com')
        UserProfile.objects.create(
            user=self.user,
            has_validated_email=True,
            name='First Last',
            bio='I have a Bio')
        self._c = Client()
        self._logged_in = Client()
        self._logged_in.force_login(self.user)

        from styles.models import Style
        s = Style(user=self.other_user, title='Default Style')
        s.save()


class ProfileTestCase(TestUserTestCase):
    def test_redirect_no_slash(self):
        resp = self._c.get('/u/test')
        self.assertRedirects(resp, '/u/test/', status_code=301)

    def test_bio_visible(self):
        resp = self._c.get('/u/test/')
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'I have a Bio')

    def test_userpage_details(self):
        resp = self._c.get('/u/test/')
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'First Last')
        self.assertContains(resp, 'test')

    def test_userpage_actions_loggedout(self):
        resp = self._c.get('/u/test/')
        self.assertEqual(resp.status_code, 200)
        self.assertNotContains(resp, 'New Presentation')

    def test_userpage_actions_loggedin(self):
        resp = self._logged_in.get('/u/test/')
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'Settings')
        self.assertContains(resp, 'New Presentation')

    def test_has_list_presentations(self):
        p = Presentation(user=self.user, title='Presentation Title TEST')
        p.save()
        resp = self._logged_in.get('/u/test/')
        self.assertContains(resp, 'Presentation Title TEST')
        self.assertContains(resp, 'Public Slides, Private Notes')
        self.assertContains(resp, '/u/test/%s/edit' % p.id)

    def test_only_shows_my_presentations(self):
        p = Presentation(user=self.other_user, title='OTHER Presentation Title')
        p.save()
        resp = self._logged_in.get('/u/test/')
        self.assertNotContains(resp, 'OTHER Presentation Title')
        self.assertNotContains(resp, '/u/test/%s/edit' % p.id)

class ThumbsTestCase(TestUserTestCase):
    def test_empty_presentation_thumb(self):
        p = Presentation(user=self.user, title='Empty Presentation')
        p.save()
        resp = self._c.get('/u/test/{}/thumb.png'.format(p.id))
        self.assertEqual(resp.status_code, 302)
        self.assertIn('emptythumb.svg', resp['Location'])


class SettingsTestCase(TestUserTestCase):
    def test_loginredirect_logggedout(self):
        resp = self._c.get('/u/test/settings/profile')
        self.assertRedirects(resp, '/accounts/login/?next=/u/test/settings/profile')

    def test_403_other_user(self):
        resp = self._logged_in.get('/u/otheruser/settings/profile')
        self.assertEqual(resp.status_code, 403)

    def test_has_original_bio(self):
        resp = self._logged_in.get('/u/test/settings/profile')
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'I have a Bio')

    def test_set_bio(self):
        resp = self._logged_in.post('/u/test/settings/profile',
                                    {'name': 'First Last', 'bio': 'NEW BIO'})
        self.assertRedirects(resp, '/u/test/')

        resp = self._c.get('/u/test/')
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'NEW BIO')
        self.assertNotContains(resp, 'I have a Bio')


class OnboardingStatusTestCase(TestUserTestCase):
    def test_GET(self):
        resp = self._logged_in.get('/u/test/onboarding-status')
        self.assertEqual(resp.status_code, 200)
        self.assertJSONEqual(str(resp.content, encoding='utf8'),
                             {'status': 0})

    def test_POST(self):
        data = {'status': 1}
        resp = self._logged_in.post('/u/test/onboarding-status',
                                    content_type='application/json',
                                    data=json.dumps(data))
        self.assertEqual(resp.status_code, 200)
        self.assertJSONEqual(str(resp.content, encoding='utf8'),
                             {'status': 1})


class CreateEditTestCase(TestUserTestCase):
    def test_create(self):
        resp = self._logged_in.get('/u/test/create')
        self.assertEqual(resp.status_code, 302)
        self.assertIn('/u/test/', resp.url)
        self.assertIn('/edit', resp.url)

    def test_create_json_get_textbox(self):
        resp = self._logged_in.get('/u/test/create.json')
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'JSON')

    def test_create_json(self):
        data = {'metadata': {'title': 'NEW TITLE', 'privacy': 'h'}}
        resp = self._logged_in.post('/u/test/create.json',
                                    {'json': json.dumps(data)})
        self.assertEqual(resp.status_code, 302)
        self.assertIn('/u/test/', resp.url)
        self.assertIn('/edit', resp.url)

    def test_create_json_invalid_privacy(self):
        data = {'metadata': {'title': 'STUPID', 'privacy': 'p'}}
        resp = self._logged_in.post('/u/test/create.json',
                                    {'json': json.dumps(data)})
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'Choose a valid privacy option')

    def test_create_json_private_pro_requirement(self):
        data = {'metadata': {'title': 'PRO', 'privacy': 'l'}}
        resp = self._logged_in.post('/u/test/create.json',
                                    {'json': json.dumps(data)})
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'You need to be a PRO')

    def test_create_json_logged_out(self):
        data = {'metadata': {'title': 'NEW TITLE', 'privacy': 'h'}}
        resp = self._c.post('/u/test/create.json', {'json': json.dumps(data)})
        self.assertRedirects(resp, '/accounts/login/?next=/u/test/create.json')

    def test_creates_notes_hidden_for_free(self):
        resp = self._logged_in.get('/u/test/create')
        self.assertEqual(resp.status_code, 302)
        self.assertIn('/u/test/', resp.url)
        self.assertIn('/edit', resp.url)
        id_ = re.search('/u/test/(\d+)/edit', resp.url).group(1)
        p = Presentation.objects.get(id=id_)
        self.assertEqual(p.privacy, Presentation.NOTES_HIDDEN)

    def test_creates_private_for_pro(self):
        profile = UserProfile.objects.get(user=self.user)
        profile.has_pro_for_free = True
        profile.save()

        resp = self._logged_in.get('/u/test/create')
        self.assertEqual(resp.status_code, 302)
        self.assertIn('/u/test/', resp.url)
        self.assertIn('/edit', resp.url)
        id_ = re.search('/u/test/(\d+)/edit', resp.url).group(1)
        p = Presentation.objects.get(id=id_)
        self.assertEqual(p.privacy, Presentation.PRIVATE)

    def test_requires_login(self):
        resp = self._c.get('/u/test/create')
        self.assertRedirects(resp, '/accounts/login/?next=/u/test/create')

    def test_denies_edit(self):
        p = Presentation(user=self.other_user, title='DONT SEE ME')
        p.save()
        resp = self._logged_in.get('/u/test/%s/edit' % p.id)
        self.assertEqual(resp.status_code, 403)

    def test_edit(self):
        p = Presentation(user=self.user, title='Presentation Title TEST')
        p.save()
        resp = self._logged_in.get('/u/test/%s/edit' % p.id)
        self.assertContains(resp, 'Presentation Title TEST')

    def test_denies_edit(self):
        p = Presentation(user=self.other_user, title='DONT SEE ME')
        p.save()
        resp = self._logged_in.get('/u/test/%s/edit' % p.id)
        self.assertEqual(resp.status_code, 403)

    def test_title_editing(self):
        p = Presentation(user=self.user, title='DONT SEE ME')
        p.save()
        data = {'metadata': {'title': 'NEW TITLE', 'privacy': 'h'}}
        resp = self._logged_in.post('/u/test/%s/edit' % p.id,
                                    content_type='application/json',
                                    data=json.dumps(data))
        self.assertEqual(resp.status_code, 200)

        p = Presentation.objects.get(id=p.id)
        self.assertEqual(p.title, 'NEW TITLE')

    _DEFAULT_METADATA = {'title': 'test', 'privacy': 'h'}

    def test_row_count_1_no_slide(self):
        p = Presentation(user=self.user)
        p.save()
        data = {'metadata': self._DEFAULT_METADATA,
                'speech': [{
                    'indent': 0,
                    'uuid': '00000000-0000-0000-0000000000000000',
                    'idea': 'Row 1',
                    'execution': '',
                }]}
        resp = self._logged_in.post('/u/test/%s/edit' % p.id,
                                    content_type='application/json',
                                    data=json.dumps(data))
        self.assertEqual(resp.status_code, 200)

        p = Presentation.objects.get(id=p.id)
        self.assertEqual(p.number_rows, 1)
        self.assertEqual(p.number_rows_slide, 0)

    def test_row_count_2_no_slide(self):
        p = Presentation(user=self.user)
        p.save()
        data = {'metadata': self._DEFAULT_METADATA,
                'speech': [{
                    'indent': 0,
                    'uuid': '00000000-0000-0000-0000000000000000',
                    'idea': 'Row 1',
                    'execution': '',
                 }, {
                    'indent': 0,
                    'uuid': '00000000-0000-0000-0000000000000001',
                    'idea': 'Row 2',
                    'execution': '',
                }]}
        resp = self._logged_in.post('/u/test/%s/edit' % p.id,
                                    content_type='application/json',
                                    data=json.dumps(data))
        self.assertEqual(resp.status_code, 200)

        p = Presentation.objects.get(id=p.id)
        self.assertEqual(p.number_rows, 2)
        self.assertEqual(p.number_rows_slide, 0)

    def test_row_count_2_1_slide(self):
        p = Presentation(user=self.user)
        p.save()
        data = {'metadata': self._DEFAULT_METADATA,
                'speech': [{
                    'indent': 0,
                    'uuid': '00000000-0000-0000-0000000000000000',
                    'idea': 'Row 1',
                    'execution': '',
                    'slideType': 'title'
                 }, {
                    'indent': 0,
                    'uuid': '00000000-0000-0000-0000000000000001',
                    'idea': 'Row 2',
                    'execution': '',
                }]}
        resp = self._logged_in.post('/u/test/%s/edit' % p.id,
                                    content_type='application/json',
                                    data=json.dumps(data))
        self.assertEqual(resp.status_code, 200)

        p = Presentation.objects.get(id=p.id)
        self.assertEqual(p.number_rows, 2)
        self.assertEqual(p.number_rows_slide, 1)

    def test_filter_allows_rich_text_row(self):
        data = {'metadata': self._DEFAULT_METADATA,
                'speech': [{
                    'idea': '<i>Hello</i> <b>World</b> <u>U</u>',
                    'execution': '<i>Hello</i> <b>World</b>'
                }]}
        original = copy.deepcopy(data)
        self.assertDictEqual(original, Presentation.bleach_data(data))

    def test_filter_blocks_script_row(self):
        data = {'metadata': self._DEFAULT_METADATA,
                'speech': [{
                    'idea': '<script>alert(1)</script>',
                    'execution': '<i onclick="alert()">Hello</i>'
                }]}
        data = Presentation.bleach_data(data)
        self.assertNotIn('script', data['speech'][0]['idea'])
        self.assertNotIn('onclick', data['speech'][0]['execution'])

    def test_filter_allows_rich_text_block(self):
        text = '''<p>Hello</p>
                  <ul><li>L</li></ul>
                  <ol><li>1st</li></ol>
                  <a href="https://google.com">abcd</a>'''
        data = {'speech': [{
                    'blocks': [{
                        'type': 'text',
                        'html': text
                    }]
               }]}
        original = copy.deepcopy(data)
        self.assertDictEqual(original, Presentation.bleach_data(data))

    def test_filter_blocks_script_block(self):
        text = '''<p onclick='alert()'>Hello</p>
                  <script>alert()</script>'''
        data = {'speech': [{
                    'blocks': [{
                        'type': 'text',
                        'html': text
                    }]
               }]}
        data = Presentation.bleach_data(data)
        new = data['speech'][0]['blocks'][0]['html']
        self.assertNotIn('script', new)
        self.assertNotIn('onclick', new)


class DeleteTestCase(TestUserTestCase):
    def test_listed_on_profile(self):
        p = Presentation(user=self.user, title='Please delete me')
        p.save()
        resp = self._logged_in.get('/u/test/')
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'delete')

    def test_requires_confirm(self):
        p = Presentation(user=self.user, title='Please delete me')
        p.save()
        resp = self._logged_in.get('/u/test/'+str(p.id)+'/delete')

        # We can still edit if not deleted
        resp = self._logged_in.get('/u/test/'+str(p.id)+'/edit')
        self.assertEqual(resp.status_code, 200)

        # This won't crash if it works
        p = Presentation.objects.get(id=p.id)

    def test_delete_redirect(self):
        p = Presentation(user=self.user, title='Please delete me')
        p.save()
        resp = self._logged_in.post('/u/test/'+str(p.id)+'/delete')
        self.assertRedirects(resp, '/u/test/')

    def test_delete_actually_delets(self):
        p = Presentation(user=self.user, title='Please delete me')
        p.save()
        resp = self._logged_in.post('/u/test/'+str(p.id)+'/delete')
        with self.assertRaises(Presentation.DoesNotExist):
            p = Presentation.objects.get(id=p.id)

    def test_needs_login(self):
        p = Presentation(user=self.user, title='Please delete me')
        p.save()
        resp = self._c.post('/u/test/'+str(p.id)+'/delete')
        self.assertNotEqual(resp.status_code, 200)
        p = Presentation.objects.get(id=p.id)

    def test_needs_right_account(self):
        p = Presentation(user=self.user, title='Please delete me')
        p.save()
        c = Client()
        c.force_login(self.other_user)
        resp = c.post('/u/test/'+str(p.id)+'/delete')
        self.assertNotEqual(resp.status_code, 200)
        p = Presentation.objects.get(id=p.id)


class PrivatePresentationTestCase(TestUserTestCase):
    def setUp(self):
        super().setUp()
        self._presentation = Presentation(
            user=self.user,
            title='MY PRIVACY',
            privacy=Presentation.PRIVATE)
        self._presentation.save()

    def test_can_view_private(self):
        resp = self._logged_in.get('/u/test/%i/view' % self._presentation.id)
        self.assertEqual(resp.status_code, 200)

    def test_annon_cannt_view_private(self):
        resp = self._c.get('/u/test/%i/view' % self._presentation.id)
        self.assertNotEqual(resp.status_code, 200)
        resp = self._c.get('/u/test/%i/thumb' % self._presentation.id)
        self.assertNotEqual(resp.status_code, 200)

    def test_other_user_cannt_view_private(self):
        c = Client()
        c.force_login(self.other_user)
        resp = c.get('/u/test/%i/view' % self._presentation.id)
        self.assertNotEqual(resp.status_code, 200)
        resp = c.get('/u/test/%i/thumb' % self._presentation.id)
        self.assertNotEqual(resp.status_code, 200)

    def test_shows_in_my_userpage(self):
        resp = self._logged_in.get('/u/test/')
        self.assertContains(resp, 'MY PRIVACY')
        self.assertContains(resp, 'Private')
        self.assertNotContains(resp, 'Public')

    def test_hidden_in_my_userpage_from_public(self):
        resp = self._c.get('/u/test/')
        self.assertNotContains(resp, 'MY PRIVACY')

    def test_hidden_in_my_userpage_from_other_user(self):
        c = Client()
        c.force_login(self.other_user)
        resp = c.get('/u/test/')
        self.assertNotContains(resp, 'MY PRIVACY')


class JsonGetTestCase(TestUserTestCase):
    def test_my_private_presentation(self):
        p = Presentation(user=self.user, privacy=Presentation.PRIVATE)
        p.save()
        resp = self._logged_in.get('/u/test/%i/view.json' % p.id)
        self.assertEqual(resp.status_code, 200)

    def test_logged_out_private_presentation(self):
        p = Presentation(user=self.user, privacy=Presentation.PRIVATE)
        p.save()
        resp = self._c.get('/u/test/%i/view.json' % p.id)
        self.assertEqual(resp.status_code, 403)

    def test_other_user_private_presentation(self):
        p = Presentation(user=self.other_user, privacy=Presentation.PRIVATE)
        p.save()
        resp = self._c.get('/u/nottest/%i/view.json' % p.id)
        self.assertEqual(resp.status_code, 403)
