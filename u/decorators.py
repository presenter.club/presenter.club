# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import login_required

from u.models import Presentation, User

def personal_user_view(wrapping):
    '''
    Adds the appropriate auth for "/u/*/" views that are only intended to
    be accessed by the user themselves.

    Converts the username argument to a user argument.
    '''
    @login_required
    def f(request, username, *args, **kwargs):
        if request.user.username.lower() != username.lower():
            raise PermissionDenied

        return wrapping(request, request.user, *args, **kwargs)
    return f


def presentation_view(wrapping):
    '''
    Wraps a /u/*/*/whatever route.  Performs auth on the view

    Passes request, presentation to the child
    '''
    def f(request, username, presentationid):
        user = get_object_or_404(User, username=username.lower())
        p = get_object_or_404(Presentation, id=presentationid, user=user)

        if p.privacy == Presentation.PRIVATE and request.user != user:
            # TODO:  Redirect annon to the login page
            raise PermissionDenied

        return wrapping(request, p)
    return f
