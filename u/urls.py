# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import url

from . import views

app_name = 'u'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    # Ex. /u/sam/
    url(r'^(?P<username>.+)/$', views.userpage, name='userpage'),
    url(r'^(?P<username>.+)/onboarding-status$', views.onboarding_status,
        name='onboardingstatus'),
    url(r'^(?P<username>.+)/create$', views.create, name='create'),
    url(r'^(?P<username>.+)/create.json$',
        views.create_json, name='createjson'),
    url(r'^(?P<username>.+)/settings/profile$',
        views.user_settings, name='usersettings'),
    url(r'^(?P<username>.+)/settings/plan$',
        views.user_plan, name='userplan'),
    url(r'^(?P<username>.+)/(?P<presentationid>.+)/view$',
        views.view, name='view'),
    url(r'^(?P<username>.+)/(?P<presentationid>.+)/view.json$',
        views.view_json, name='viewjson'),
    url(r'^(?P<username>.+)/(?P<presentationid>.+)/edit$',
        views.edit, name='edit'),
    url(r'^(?P<username>.+)/(?P<presentationid>.+)/delete$',
        views.delete_presentation, name='deletepresentation'),
    url(r'^(?P<username>.+)/(?P<presentationid>.+)/thumb.png$',
        views.thumb, name='thumb'),
    url(r'^(?P<username>.+)/(?P<presentationid>.+)/genthumb/(?P<key>.+)$',
        views.genthumb, name='genthumb'),
]
