# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib import admin

from django.contrib.auth.admin import UserAdmin
from u.models import User, UserProfile, Presentation, is_pro_user

admin.site.unregister(User)

class UserProfileInline(admin.StackedInline):
    model = UserProfile


def give_free_pro(modeladmin, request, queryset):
    gave = 0
    for item in queryset:
        profile = UserProfile.objects.get(user=item)
        if not profile.has_pro_for_free:
            gave += 1
        profile.has_pro_for_free = True
        profile.save()
    modeladmin.message_user(request, 'Gave {} users pro for free'.format(gave))
give_free_pro.short_description = 'Give Pro for Free'

def validate_email(modeladmin, request, queryset):
    done = 0
    for item in queryset:
        profile = UserProfile.objects.get(user=item)
        if not profile.has_validated_email:
            done += 1
        profile.has_validated_email = True
        profile.save()
    modeladmin.message_user(request, 'Validated {} users emails'.format(done))
validate_email.short_description = 'Mark Emails as Valid'


@admin.register(User)
class MyUserAdmin(UserAdmin):
    def _valid_email(obj):
        profile = UserProfile.objects.get(user=obj)
        return profile.has_validated_email
    _valid_email.boolean = True
    _valid_email.short_description = 'Valid E'

    def _onboarding_status(obj):
        profile = UserProfile.objects.get(user=obj)
        return profile.onboarding_status
    _onboarding_status.short_description = 'Onboard'

    def _pro(obj):
        return is_pro_user(obj)
    _pro.boolean = True
    _pro.short_description = 'Pro'

    def _n_presentations(obj):
        return Presentation.objects.filter(user=obj).count()
    _n_presentations.short_description = '# P'

    def _profile(obj):
            return '<a href="/u/{}">Profile</a>'.format(obj.username)
    _profile.allow_tags = True
    _profile.short_description = 'Profile'

    list_display = ('username', _pro, 'email', _valid_email,
                    _onboarding_status, _n_presentations,
                    'last_login', 'date_joined', _profile)
    actions = [give_free_pro, validate_email]
    inlines = [UserProfileInline]


@admin.register(Presentation)
class PresentationAdmin(admin.ModelAdmin):
    list_display = ('user', 'title', 'privacy', 'edited_time')
