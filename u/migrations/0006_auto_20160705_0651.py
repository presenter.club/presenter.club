# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-05 06:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('u', '0005_auto_20160705_0648'),
    ]

    operations = [
        migrations.AddField(
            model_name='presentation',
            name='privacy',
            field=models.CharField(choices=[('l', 'Private'), ('h', 'Notes Hidden'), ('o', 'Public')], default='h', max_length=1),
        ),
        migrations.AlterField(
            model_name='presentation',
            name='data',
            field=models.TextField(default='{"metadata": {}, "speech": [{"indent": 0, "execution": "", "idea": "Type to get started...", "uuid": "00000000-0000-0000-0000000000000000"}]}', verbose_name='Data with speech, internal JSON format'),
        ),
    ]
