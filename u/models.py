# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import bleach
from django.db import models

from django.contrib.auth.models import User

from styles.models import DEFAULT_STYLE


ALLOWED_TAGS = bleach.ALLOWED_TAGS + ['u', 'p']


def is_pro_user(user):
    profile = UserProfile.objects.get(user=user)
    return profile.has_pro or profile.has_pro_for_free


class UserProfile(models.Model):
    user = models.OneToOneField(User)

    has_validated_email = models.BooleanField(default=False)

    stripe_customer = models.TextField(null=True)
    has_pro = models.BooleanField(default=False)
    has_pro_for_free = models.BooleanField(default=False)

    name = models.TextField('Name (publicly displayed)')
    bio = models.TextField('Bio Text (publicly displayed)', blank=True)

    onboarding_status = models.IntegerField('Onboarding status', default=0)


_DEFAULT_JSON = json.dumps({
    'speech': [{
        'indent': 0,
        'uuid': '00000000-0000-0000-0000000000000000',
        'slideType': None,
    }]
})


def thumb_directory_path(instance, filename):
    return 'presentation_thumbs/{0}/{1}.png'.format(
        instance.user.id, instance.id)


class Presentation(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    created_time = models.DateTimeField('Created', auto_now_add=True)
    edited_time = models.DateTimeField('Edited', auto_now=True)

    title = models.TextField('Title (publicly displayed)')
    data = models.TextField('Data with speech, internal JSON format',
                            default=_DEFAULT_JSON)

    number_rows_slide = models.IntegerField('Number of rows with a slide',
                                            default=0)
    number_rows = models.IntegerField('Number of rows (w or wo slides)',
                                      default=1)

    PRIVATE = 'l'
    NOTES_HIDDEN = 'h'
    PUBLIC = 'o'
    PRIVACY_CHOICES = (
        (PRIVATE, 'Private'),
        (NOTES_HIDDEN, 'Public Slides, Private Notes'),
        (PUBLIC, 'Public')
    )
    privacy = models.CharField(
        'Privacy Mode',
        max_length=1,
        choices=PRIVACY_CHOICES,
        default=NOTES_HIDDEN
    )

    thumb = models.FileField(upload_to=thumb_directory_path, null=True)
    thumb_edited_time = models.DateTimeField(
        'When the thumbnail was last updated',
        null=True)

    def get_style_id(self) -> int:
        d = json.loads(self.data)
        if d.get('style') is None:
            return DEFAULT_STYLE
        return d['style'].get('id')

    @staticmethod
    def bleach_data(data: dict):
        def _fix(html):
            after = bleach.clean(html, strip=True, tags=ALLOWED_TAGS)
            return bleach.clean(html, strip=True, tags=ALLOWED_TAGS)

        def _fix_block(block):
            if 'html' in block:
                block['html'] = _fix(block['html'])
            return block

        def _fix_speech_row(row):
            if 'idea' in row:
                row['idea'] = _fix(row['idea'])
            if 'execution' in row:
                row['execution'] = _fix(row['execution'])
            if 'blocks' in row:
                row['blocks'] = list(map(_fix_block, row['blocks']))
            return row

        if 'speech' in data:
            data['speech'] = list(map(_fix_speech_row, data['speech']))
        return data
