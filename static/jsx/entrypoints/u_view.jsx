/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import ReactDOM from  'react-dom';

import AppProvider from '../app-provider.jsx';
import ViewPresentation from '../components/ViewPresentation.jsx';

function parseQueryString() {
    const query = window.location.search.substring(1);
    const vars = query.split('&');
    const ret = {};
    vars.forEach(v => {
        const pair = v.split('=');
        ret[pair[0]] = pair[1];
    });
    return ret;
}

const q = parseQueryString();
const loop = q.loop ? parseInt(q.loop) : undefined;

ReactDOM.render(
    <AppProvider><ViewPresentation loop={ loop } /></AppProvider>,
    document.getElementById('react-container')
);
