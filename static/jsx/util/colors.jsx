/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

export function colorToNumbers(s) {
    if (s[0] === '#' && s.length === 4) {
        return [parseInt(s[1], 16), parseInt(s[2], 16), parseInt(s[3], 16)];
    } else if (s[0] === '#' && s.length === 7) {
        return [parseInt(s.substr(1, 2), 16), parseInt(s.substr(3, 2), 16),
                parseInt(s.substr(5, 2), 16)];
    } else if (s === 'black') {
        return [0, 0, 0];
    } else if (s == 'white') {
        return [255, 255, 255];
    }
    console.error('Can not parse color', s);
    return [139, 7, 200];
};

export function distanceBetween(a, b) {
    const x = colorToNumbers(a),
          y = colorToNumbers(b);
    return x.map((v0, index) => {
            const v1 = y[index];
            return Math.pow(Math.abs(v0 - v1), 2);
        }).reduce((a, b) => a + b);
};

export function bestContrast(colors, color) {
    return colors.map((c) => ({ c, d: distanceBetween(c, color) }) )
                 .reduce((a, b) => a.d > b.d ? a : b)
                 .c;
};

export function colorMod(color, change) {
    const n = colorToNumbers(color);
    const c = n.map(v => Math.max(0, Math.min(255, v + change)).toFixed());
    return `rgb(${ c[0] }, ${ c[1] }, ${ c[2] })`;
}
