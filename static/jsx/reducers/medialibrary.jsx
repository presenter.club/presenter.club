/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { MediaLibraryActions } from '../constants/actions.jsx';
import { Types } from '../constants/backgrounds.jsx';

function medialibrary(state = { loading: false,
                                loaded: false,
                                images: [] },
                      action) {
    if (action.type === MediaLibraryActions.START_REQUEST) {
        if (state.request)  state.request.abort();
        return Object.assign({}, state, {
            loading: true,
            request: action.request
        });
    }
    if (action.type === MediaLibraryActions.FINISH_REQUEST) {
        const raw = (action.data || {}).images || [];
        const images = raw.map(i => Object.assign({ type: Types.MEDIALIBRARY },
                                                  i));
        const error = (action.data || {}).error ? action.data : undefined;
        return Object.assign({}, state, {
            loading: false,
            loaded: true,
            images,
            error,
            loadErr: action.err,
            request: undefined
        });
    }
    return state;
}

export default medialibrary;
