/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import expect from 'expect';

import reducer from './reducers.jsx';
import * as PresentationActions from '../actions/presentation.jsx';

const INITIAL_STATE = {
    presentation: {
        metadata: {
            title: 'Hello World',
        },
        speech: [
            {
                idea: 'My Idea',
                indent: 0
            }
        ]
    }
};

describe('presentatoin reducer', () => {
    it('should return the initial state', () => {
        expect(
            reducer(INITIAL_STATE, {})
        ).toEqual(INITIAL_STATE);
    });
});
