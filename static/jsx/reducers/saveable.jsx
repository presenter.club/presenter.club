/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function saveable(name, reducer) {
    // Call the reducer with empty action to populate the initial state
    const initialState = {
        content: reducer(undefined, {}),
        saving: false,
        dirty: false,
        request: null,
        error: null
    }

    // Return a reducer that handles saving
    return function (state = initialState, action) {
        const { content, saving, dirty, request, error } = state;

        if (action.type === name+'/START_REQUEST') {
            if (state.request)  state.request.abort();
            return {
                dirty,
                saving: true,
                request: action.request,
                error,
                content
            };
        } else if (action.type === name+'/FINISH_REQUEST') {
            window.onbeforeunload = () => null;
            let error = null;
            if (!action.data.success)  error = action.data.msg;
            return {
                dirty: false,
                saving: false,
                request: null,
                error,
                content
            };
        } else {
            // Delegate handling the action to the passed reducer
            const newContent = reducer(content, action);
            if (content === newContent) {
                return state;
            }

            if (request !== null)  request.abort();
            window.onbeforeunload = () => 'Changes are not saved';

            return {
                dirty: true,
                saving: false,
                request: null,
                error,
                content: newContent
            }
        }
    }
}
export default saveable;
