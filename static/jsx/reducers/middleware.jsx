/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { doSave } from '../actions/savestatus.jsx';

const autosaver = (store, name) => ({ dispatch, getState }) => {
    let timeout = null;
    let dataToSave = null;

    let timeoutCb = () => {
        doSave(dispatch, name, dataToSave);
        timeout = null;
    };

    return next => action => {
        window.setTimeout(() => {
            const s = getState()[store];
            if (timeout !== null && s.saving) {
                window.clearTimeout(timeout);
                timeout = null;
            }

            if (s.dirty && !s.saving) {
                window.clearTimeout(timeout);

                dataToSave = s.content.present;
                timeout = window.setTimeout(timeoutCb, 1000);
            }
        }, 10);  // Delay until after the action invocation
        return next(action);
    };
};

const thunk = reduxFunctions => next => action => {
    if (typeof action === 'function') {
        return action(reduxFunctions);
    }

    return next(action);
};

const all = [autosaver('presentation', 'Presentation'),
             autosaver('style', 'Style'),
             thunk];
export default all;
