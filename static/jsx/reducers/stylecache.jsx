/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { StyleCacheActions } from '../constants/actions.jsx';

const _DEFAULT = {};

function stylecache(state = { loaded: false,
                              data: _DEFAULT },
                      action) {
    if (action.type === StyleCacheActions.START_REQUEST) {
        if (state.request)  state.request.abort();
        return Object.assign({}, state, {
            loaded: false,
            request: action.request
        });
    }
    if (action.type === StyleCacheActions.FINISH_REQUEST) {
        return Object.assign({}, state, {
            loaded: true,
            data: action.data.style,
            request: undefined
        });
    }
    return state;
}

export default stylecache;
