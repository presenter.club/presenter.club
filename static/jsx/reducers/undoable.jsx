/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Moment from 'moment';

const MAX_STACK = 1000;
function undoable(name, reducer, shouldMergeActions = (a, b) => false) {
    // Call the reducer with empty action to populate the initial state
    const initialState = {
        past: [],
        present: reducer(undefined, {}),
        presentTime: Moment.unix(0),
        presentAction: undefined,
        future: []
    }

    // Return a reducer that handles undo and redo
    return function (state = initialState, action) {
        const { past, present, presentTime, presentAction, future } = state;

        if (action.type === name+'/UNDO') {
            const previous = past[past.length - 1];
            const newPast = past.slice(0, past.length - 1);
            return {
                past: newPast,
                present: previous.data,
                presentTime: previous.time,
                future: [ { data: present, time: presentTime,
                            action: presentAction }, ...future ]
            };
        } else if (action.type === name+'/REDO') {
            const next = future[0]
            const newFuture = future.slice(1)
            return {
                past: [ ...past, { data: present, time: presentTime,
                                   action: presentAction }],
                present: next.data,
                presentTime: next.time,
                future: newFuture
            }
        } else {
            // Delegate handling the action to the passed reducer
            const newPresent = reducer(present, action);
            if (present === newPresent) {
                return state;
            }

            let ts = Moment();
            let newPast;
            if (Math.abs(presentTime.diff(ts)) < 1000 &&
                shouldMergeActions(action, presentAction)) {
                // Merge it
                newPast = past;
                ts = presentTime;
            } else {
                newPast = [ ...past, { data: present, time: presentTime,
                                       action: presentAction } ];
                if (newPast.length > MAX_STACK) {
                    newPast = newPast.slice(newPast.length - MAX_STACK);
                }
            }
            return {
                past: newPast,
                present: newPresent,
                presentTime: ts,
                presentAction: action,
                future: []
            }
        }
    }
}
export default undoable;
