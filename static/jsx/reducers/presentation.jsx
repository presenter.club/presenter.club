/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { combineReducers } from 'redux';

import { PresentationActions } from '../constants/actions.jsx';
import { SLIDE_TYPES } from '../constants/slidetypes.jsx';

import undoable from './undoable.jsx';
import saveable from './saveable.jsx';

const NEW_SLIDE_DEFAULT = {
    indent: 0,
    //idea: 'Type your idea...',
    //execution: '',

    slideType: null,
    blocks: []
};

function presentationEditRow(state, uuid, callback) {
    const newSpeech = state.speech.map((row) => {
        if (row.uuid === uuid) {
            return callback(row);
        }
        return row;
    });
    return Object.assign({}, state, {speech: newSpeech});
};
function presentationEditBlock(state, uuid, i, doEdit) {
    return presentationEditRow(
        state, uuid,
        (row) => {
            const block = doEdit(row.blocks[i]);
            const blocks = row.blocks.slice(0, i)
                              .concat([ block ])
                              .concat(row.blocks.slice(i+1));
            return Object.assign({}, row, { blocks });
        }
    );
};

const DEFAULT_BLOCKS = {
    h1: { type: 'heading', size: 1 },
    h2: { type: 'heading', size: 2 },
    null: { type: 'empty' }
};
function fitBlocksToTemplate(blocks, suggested) {
    const blocksByType = { h1: [], h2: [], null: [] };
    blocks.forEach((b) => {
        if (b.type === 'heading' && b.size === 1) {
            blocksByType.h1.push(b);
        } else if (b.type === 'heading' && b.size === 2) {
            blocksByType.h2.push(b);
        } else {
            blocksByType.null.push(b);
        }
    });

    return suggested.map((suggestion) => {
        if (blocksByType[suggestion].length > 0) {
            return blocksByType[suggestion].shift();
        } else {
            return DEFAULT_BLOCKS[suggestion];
        }
    }).concat(
        // We don't want the user to loose the blocks, so just keep 'em
        blocksByType.h1,
        blocksByType.h2,
        blocksByType.null
    );
};

function presentation(state = window.djangosGift, action) {
    if (action.type === PresentationActions.ADD_ROW) {
        let newSpeech = [];
        state.speech.forEach((row) => {
            if (row.uuid === action.after) {
                newSpeech.push(Object.assign({}, row, action.myRow));
                newSpeech.push(Object.assign({},
                                             NEW_SLIDE_DEFAULT,
                                             action.row));
            } else {
                newSpeech.push(row);
            }
        });
        return Object.assign({}, state, {speech: newSpeech});
    }
    if (action.type === PresentationActions.DELETE_ROW) {
        if (state.speech.length <= 1) {
            return state
        } else {
            const newSpeech = state.speech.filter((row) =>
                row.uuid !== action.uuid
            );
            return Object.assign({}, state, {speech: newSpeech});
        };
    }
    if (action.type === PresentationActions.EDIT_ROW) {
        return presentationEditRow(
            state, action.uuid,
            (row) => Object.assign({}, row, action.change)
        );
    }
    if (action.type === PresentationActions.EDIT_BLOCK) {
        return presentationEditBlock(
            state, action.uuid, action.i,
            block => Object.assign({}, block, action.change)
        );
    }
    if (action.type === PresentationActions.CHANGE_BLOCK_TYPE) {
        return presentationEditBlock(
            state, action.uuid, action.i,
            block => {
                const TEXT_BLOCKS = ['text', 'quote', 'heading'];
                if (TEXT_BLOCKS.indexOf(block.type) !== -1
                    && TEXT_BLOCKS.indexOf(action.newType) !== -1) {
                    return {
                        type: action.newType,
                        html: block.html
                    }
                }
                return { type: action.newType };
            }
        );
    }
    if (action.type === PresentationActions.ADD_SLIDE) {
        return presentationEditRow(
            state, action.uuid,
            (row) => {
                return Object.assign({}, row, {
                    slideType: 'none',
                    blocks: []
                });
            }
        );
    }
    if (action.type === PresentationActions.CHANGE_TEMPLATE) {
        return presentationEditRow(
            state, action.uuid,
            (row) => {
                const type = SLIDE_TYPES[action.next];
                const change = { slideType: action.next,
                                 blocks: row.blocks };

                change.blocks = fitBlocksToTemplate(
                    row.blocks,
                    type.suggestedBlocks || []
                )
                return Object.assign({}, row, change);
            }
        );
    }
    if (action.type === PresentationActions.DELETE_SLIDE) {
        return presentationEditRow(
            state, action.uuid,
            (row) => Object.assign({}, row, { slideType: null, blocks: [] })
        );
    }
    if (action.type === PresentationActions.EDIT_METADATA) {
        const m = Object.assign({}, state.metadata, action.change);
        return Object.assign({}, state, { metadata: m });
    }
    if (action.type === PresentationActions.EDIT_ROOT) {
        return Object.assign({}, state, action.change);
    }
    return state;
};

function shouldMergeActions(a, b) {
    if (a === undefined || b === undefined)  return false;
    if (a.type !== b.type)  return false;
    if (a.type === PresentationActions.EDIT_ROW
        || a.type === PresentationActions.EDIT_BLOCK) {
        const ak = Object.keys(a.change),
              bk = Object.keys(b.change);
        if (ak.length === 1 && bk.length === 1
            && ak[0] === bk[0]
            && (ak[0] === 'idea' || ak[0] === 'execution' || ak[0] === 'html')) {
            return true;
        }
        return false;
    }
    return false;
};


export default saveable('Presentation',
                        undoable('Presentation',
                                 presentation,
                                 shouldMergeActions));

export function unwrap(presentation) {
    return presentation.content.present;
};
