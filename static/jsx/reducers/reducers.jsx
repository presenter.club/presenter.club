/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { combineReducers } from 'redux';

import { PresentationActions, EditViewActions, MediaLibraryActions,
         SaveStatusActions } from '../constants/actions.jsx';
import { doSave } from '../actions/savestatus.jsx';

import presentation from './presentation.jsx';
import medialibrary from './medialibrary.jsx';
import stylecache from './stylecache.jsx';
import style from './style.jsx';


function editview(state = { overlay: false }, action) {
    if (action.type === EditViewActions.SET_OVERLAY) {
        return Object.assign({}, state, { overlay: action.overlay });
    }
    if (action.type === EditViewActions.SELECT_SLIDE
        || action.type === PresentationActions.ADD_SLIDE) {
        return Object.assign({}, state, {
            selectedSlide: action.uuid,
            selectedSidebar: undefined,
            focusedBlock: undefined,
            blockTypeChooser: false
        });
    }
    if (action.type === EditViewActions.SELECT_SIDEBAR) {
        return Object.assign({}, state, { selectedSidebar: action.sidebar });
    }
    if (action.type === EditViewActions.FOCUS_BLOCK) {
        return Object.assign({}, state, { focusedBlock: action.i,
                                          blockTypeChooser: false });
    }
    if (action.type === EditViewActions.TOGGLE_BLOCK_TYPE_CHOOSER) {
        const blockTypeChooser = !state.blockTypeChooser;
        return Object.assign({}, state, { blockTypeChooser });
    }
    if (action.type === EditViewActions.SET_BLOCK_TYPE_CHOOSER) {
        const blockTypeChooser = action.visible;
        return Object.assign({}, state, { blockTypeChooser });
    }
    if (action.type === PresentationActions.DELETE_SLIDE) {
        return Object.assign({}, state, { selectedSlide: undefined,
                                          focusedBlock: undefined });
    }
    return state;
}


function doesActionModifyPresentation(type) {
    switch (type) {
      case PresentationActions.ADD_ROW:
      case PresentationActions.DELETE_ROW:
      case PresentationActions.EDIT_ROW:
      case PresentationActions.DELETE_SLIDE:
      case PresentationActions.CHANGE_TEMPLATE:
      case PresentationActions.ADD_SLIDE:
      case PresentationActions.EDIT_METADATA:
      case PresentationActions.EDIT_BLOCK:
        return true;
    }
    return false;
}

function savestatus(state = { saving: false, dirty: false }, action) {
    if (doesActionModifyPresentation(action.type)) {
        window.onbeforeunload = () => 'Changes are not saved';
        return Object.assign({}, state, {
            dirty: true
        });
    }
    if (action.type === SaveStatusActions.START_REQUEST) {
        if (state.request)  state.request.abort();
        return Object.assign({}, state, {
            query: action.query,
            saving: true,
            dirty: false,
            request: action.request
        });
    }
    if (action.type === SaveStatusActions.FINISH_REQUEST) {
        window.onbeforeunload = () => null;
        return Object.assign({}, state, {
            saving: false,
            request: undefined
        });
    }
    return state;
}

const AppReducer = combineReducers({
    presentation,
    medialibrary,
    stylecache,
    editview,
    savestatus,
    style
})

export default AppReducer
