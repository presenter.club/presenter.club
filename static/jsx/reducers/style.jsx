/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { StyleActions } from '../constants/actions.jsx';

import undoable from './undoable.jsx';
import saveable from './saveable.jsx';

function style(state = window.djangosGift, action) {
    if (action.type === StyleActions.EDIT_ELEMENT) {
        const element = Object.assign(
            {},
            state.style[action.element],
            action.change
        );
        const style = Object.assign(
            {},
            state.style,
            { [action.element]: element }
        );
        return Object.assign({}, state, { style });
    }
    if (action.type === StyleActions.EDIT_PREVIEW) {
        const previews = Object.assign({}, (state.style || {}).previews);
        previews[action.preview] = Object.assign(
            {},
            previews[action.preview],
            action.change,
        )
        const style = Object.assign({}, state.style, { previews });
        return Object.assign({}, state, { style });
    }
    if (action.type === StyleActions.SET_COLORS) {
        const style = Object.assign(
            {},
            state.style,
            { colors: action.colors }
        );
        return Object.assign({}, state, { style });
    }
    if (action.type === StyleActions.ADD_BACKGROUND_IMAGE) {
        let images = (state.style || {}).images || [];
        images = images.concat([action.image]);
        const style = Object.assign({}, state.style, { images });
        return Object.assign({}, state, { style });
    }
    if (action.type === StyleActions.REMOVE_BACKGROUND_IMAGE) {
        let images = (state.style || {}).images || [];
        images = images.filter(img => img !== action.image);
        const style = Object.assign({}, state.style, { images });
        return Object.assign({}, state, { style });
    }
    if (action.type === StyleActions.EDIT_METADATA) {
        const m = Object.assign({}, state.metadata, action.change);
        return Object.assign({}, state, { metadata: m });
    }
    return state;
};

function shouldMergeActions(a, b) {
    return false;
};


export default saveable('Style', undoable('Style', style, shouldMergeActions));

export const unwrap = state => state.content.present;
