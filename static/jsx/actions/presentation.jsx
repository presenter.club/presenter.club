/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { PresentationActions } from '../constants/actions.jsx';

export function editRow(uuid, change) {
    return {
        type: PresentationActions.EDIT_ROW,
        uuid,
        change,
    }
};

export function changeTemplate(uuid, next) {
    return {
        type: PresentationActions.CHANGE_TEMPLATE,
        uuid,
        next,
    }
};

export function editBlock(uuid, i, change) {
    return {
        type: PresentationActions.EDIT_BLOCK,
        uuid,
        i,
        change,
    }
};

export function changeBlockType(uuid, i, newType) {
    return {
        type: PresentationActions.CHANGE_BLOCK_TYPE,
        uuid,
        i,
        newType
    }
};

export function addSlide(uuid) {
    return {
        type: PresentationActions.ADD_SLIDE,
        uuid
    }
};

export function deleteSlide(uuid) {
    return {
        type: PresentationActions.DELETE_SLIDE,
        uuid,
    }
};

export function deleteRow(uuid) {
    return {
        type: PresentationActions.DELETE_ROW,
        uuid,
    }
};

/* Life saver http://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript */
export function uuid4() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

export function addRow(after, row = {}, myRow = {}) {
    return {
        type: PresentationActions.ADD_ROW,
        after,
        row,
        myRow
    }
};

export function editMetadata(change) {
    return {
        type: PresentationActions.EDIT_METADATA,
        change
    }
};

export function editRoot(change) {
    return {
        type: PresentationActions.EDIT_ROOT,
        change
    }
};
