/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { MediaLibraryActions } from '../constants/actions.jsx';
import { Types } from '../constants/backgrounds.jsx';
import request from 'superagent';

export const update = () => {
    return ({ getState, dispatch }) => {
        if (getState().medialibrary.loading === true)  return;

        const r = request.get(`/u/${ window.djangosUser.username }/medialibrary/api/`)
                         .end((err, resp) => {
            if (err) {
                dispatch({
                    type: MediaLibraryActions.FINISH_REQUEST,
                    err
                })
            } else {
                dispatch({
                    type: MediaLibraryActions.FINISH_REQUEST,
                    data: resp.body
                })
            }
        });

        dispatch({
            type: MediaLibraryActions.START_REQUEST,
            request: r
        });
    };
};

export const upload = (dispatch, file, onSelect) => {
    const r = request.post(`/u/${ window.djangosUser.username }/medialibrary/api/`)
                     .attach('image', file)
                     .end((err, resp) => {
        if (err) {
            dispatch({
                type: MediaLibraryActions.FINISH_REQUEST,
                err
            });
        } else {
            dispatch({
                type: MediaLibraryActions.FINISH_REQUEST,
                data: resp.body
            });
            onSelect(Object.assign(resp.body.justUploaded,
                                   { type: Types.MEDIALIBRARY }));
        }
    });

    dispatch({
        type: MediaLibraryActions.START_REQUEST,
        request: r
    });
};
