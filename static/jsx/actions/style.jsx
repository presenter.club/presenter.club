/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { StyleActions } from '../constants/actions.jsx';

export const editMetadata = function (change) {
    return {
        type: StyleActions.EDIT_METADATA,
        change
    }
};

export const editElement = function (element, change) {
    return {
        type: StyleActions.EDIT_ELEMENT,
        element,
        change
    }
};

export const setColors = function (colors) {
    return {
        type: StyleActions.SET_COLORS,
        colors
    }
};

export const addBackgroundImage = function (image) {
    return {
        type: StyleActions.ADD_BACKGROUND_IMAGE,
        image
    }
};

export const removeBackgroundImage = function (image) {
    return {
        type: StyleActions.REMOVE_BACKGROUND_IMAGE,
        image
    }
};

export const editPreview = function (preview, change) {
    return {
        type: StyleActions.EDIT_PREVIEW,
        preview,
        change
    }
};
