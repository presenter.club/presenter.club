/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { EditViewActions } from '../constants/actions.jsx';

export const setOverlay = function (overlay) {
    return {
        type: EditViewActions.SET_OVERLAY,
        overlay
    }
};

export const selectSlide = function (uuid) {
    return {
        type: EditViewActions.SELECT_SLIDE,
        uuid
    }
};

export const closeSlideView  = function (uuid) {
    return selectSlide(undefined);
};

export const selectSidebar = function (sidebar) {
    return {
        type: EditViewActions.SELECT_SIDEBAR,
        sidebar
    }
};

export const selectDefaultSidebar = function () {
    return selectSidebar(undefined);
};

export const focusBlock = function (i) {
    return {
        type: EditViewActions.FOCUS_BLOCK,
        i
    }
};

export const setBlockTypeChooser = function (visible) {
    return {
        type: EditViewActions.SET_BLOCK_TYPE_CHOOSER,
        visible
    }
};

export const toggleBlockTypeChooser = function () {
    return {
        type: EditViewActions.TOGGLE_BLOCK_TYPE_CHOOSER
    }
};
