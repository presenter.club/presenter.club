/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { colorMod } from '../util/colors.jsx';

export const TextEffects = {
    GLOW: 'glow',
    THREE_D: '3d',
    LETTERPRESS: 'letterpress'
};

export const toCSS = (blockStyle, row, scale) => {
    if (blockStyle === undefined)  blockStyle = {};
    const color = row.slideFgText || 'black',
          shadow = row.slideFgShadow;

    switch (blockStyle.textEffect) {
      case TextEffects.THREE_D:
        let textShadow = '';
        for (let i = 0; i <= 6*scale; i++) {
            textShadow += `${ (i * 0.3).toFixed(2) }px ${ i.toFixed(2) }px 0 `
                           + `${ colorMod(color, -(40 + (i/scale) * 2.5)) }, `;
        }
        textShadow += `0 0 1em ${ shadow }`;
        return {
            color: color,
            textShadow
        }
      case TextEffects.LETTERPRESS:
        return {
            color: colorMod(color, -10),
            textShadow: '0.01em 0.025em ' + color
        }
      default:
        return { 
            color: color,
            textShadow: shadow === null ? 'none' : ('0 0 1em ' + shadow),
        }
    };
};
