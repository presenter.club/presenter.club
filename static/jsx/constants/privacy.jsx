const Privacy = {
    PRIVATE: 'l',
    NOTES_HIDDEN: 'h',
    PUBLIC: 'o'
};
export default Privacy;
