/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

export const PresentationActions = {
    ADD_ROW: 'PresentationActions.ADD_ROW',
    ADD_SLIDE: 'PresentationActions.ADD_SLIDE',
    DELETE_ROW: 'PresentationActions.DELETE_ROW',
    EDIT_ROW: 'PresentationActions.EDIT_ROW',
    CHANGE_TEMPLATE: 'PresentationActions.CHANGE_TEMPLATE',
    EDIT_BLOCK: 'PresentationActions.EDIT_BLOCK',
    CHANGE_BLOCK_TYPE: 'PresentationActions.CHANGE_BLOCK_TYPE',
    DELETE_SLIDE: 'PresentationActions.DELETE_SLIDE',
    EDIT_METADATA: 'PresentationActions.EDIT_METADATA',
    EDIT_ROOT: 'PresentationActions.EDIT_ROOT'
};

export const StyleActions = {
    EDIT_METADATA: 'StyleActions.EDIT_METADATA',
    EDIT_ELEMENT: 'StyleActions.EDIT_ELEMENT',
    EDIT_PREVIEW: 'StyleActions.EDIT_PREVIEW',
    SET_COLORS: 'StyleActions.SET_COLORS',
    ADD_BACKGROUND_IMAGE: 'StyleActions.ADD_BACKGROUND_IMAGE',
    REMOVE_BACKGROUND_IMAGE: 'StyleActions.REMOVE_BACKGROUND_IMAGE',
};

export const EditViewActions = {
    SET_OVERLAY: 'EditViewActions.SET_OVERLAY',
    SELECT_SLIDE: 'EditViewActions.SELECT_SLIDE',
    SELECT_SIDEBAR: 'EditViewActions.SELECT_SIDEBAR',
    FOCUS_BLOCK: 'EditViewActions.FOCUS_BLOCK',
    TOGGLE_BLOCK_TYPE_CHOOSER: 'EditViewActions.TOGGLE_BLOCK_TYPE_CHOOSER',
    SET_BLOCK_TYPE_CHOOSER: 'EditViewActions.SET_BLOCK_TYPE_CHOOSER'
};

export const MediaLibraryActions = {
    START_REQUEST: 'MediaLibraryActions.START_REQUEST',
    FINISH_REQUEST: 'MediaLibraryActions.FINISH_REQUEST',
};

export const StyleCacheActions = {
    START_REQUEST: 'StyleCacheActions.START_REQUEST',
    FINISH_REQUEST: 'StyleCacheActions.FINISH_REQUEST',
};

export const SaveStatusActions = {
    START_REQUEST: 'SaveStateActions.START_REQUEST',
    FINISH_REQUEST: 'SaveStateActions.FINISH_REQUEST',
};
