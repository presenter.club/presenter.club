/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

export const Types = {
    UNSPLASH: 'unsplash',
    FLICKR: 'flickr',
    SPLASHBASE: 'splashbase',
    SUBTLEPATTERN: 'subtlepattern',
    COLOR: 'color',
    MEDIALIBRARY: 'medialibrary',
    CURATED: 'curated',
    GRADIENT: 'gradient'
}

export const GradientModes = {
    horizontal: {
        aLabel: 'Left',
        bLabel: 'Right',
        toCSS: ({ a, b }) => `linear-gradient(to right, ${ a }, ${ b })`
    },
    vertical: {
        aLabel: 'Top',
        bLabel: 'Bottom',
        toCSS: ({ a, b }) => `linear-gradient(${ a }, ${ b })`
    },
    radial: {
        aLabel: 'Inside',
        bLabel: 'Outside',
        toCSS: ({ a, b }) => `radial-gradient(${ a }, ${ b })`
    }
};

export const backgroundFromColor = (color) => ({ type: Types.COLOR, color });
export const colorFromBackground = (bg) => {
    if (bg === undefined)  return null;
    if (bg.type === Types.COLOR)  return bg.color;
    return null;
}

export function backgroundToUrl(bg, thumb = false) {
    switch (bg.type) {
      case Types.UNSPLASH:
      case Types.CURATED:
        return thumb ? bg.thumb : bg.full;
      case Types.FLICKR:
        return `https://farm${bg.farm}.staticflickr.com/${bg.server}/${bg.id}_${bg.secret}_${thumb ? 'm' : 'h'}.jpg`;
      case Types.SPLASHBASE:
      case Types.SUBTLEPATTERN:
        return bg.url;
      case Types.MEDIALIBRARY:
        return bg.full;
    }
    return null;
}

export function backgroundToCss(bg, thumb = false) {
    if (!bg)  return {};

    const url = backgroundToUrl(bg, thumb);
    switch (bg.type) {
      case Types.COLOR:
        return { backgroundColor: bg.color,
                 backgroundImage: null };
      case Types.UNSPLASH:
      case Types.FLICKR:
      case Types.SPLASHBASE:
      case Types.MEDIALIBRARY:
        return { backgroundImage: `url("${ url }")` };
      case Types.SUBTLEPATTERN:
        return { backgroundImage: `url("${ url }")`,
                 backgroundRepeat: 'repeat',
                 backgroundSize: 'auto' };
      case Types.CURATED:
        if (bg.tile) {
            return { backgroundImage: `url("${ url }")`,
                     backgroundRepeat: 'repeat',
                     backgroundSize: 'auto' };
        } else {
            return { backgroundImage: `url("${ url }")` };
        }
      case Types.GRADIENT:
        return { backgroundImage: GradientModes[bg.mode].toCSS(bg) };
    }
    console.error(bg);
    return {};
};

export function backgroundToKey(bg) {
    if (!bg)  return '';

    switch (bg.type) {
      case Types.COLOR:
        return 'color' + bg.color;
      case Types.UNSPLASH:
        return 'unsplash' + bg.full;
      case Types.FLICKR:
        return 'flickr' + bg.id;
      case Types.SPLASHBASE:
        return 'splashbase' + bg.url;
      case Types.SUBTLEPATTERN:
        return 'subletpattern' + bg.url;
      case Types.MEDIALIBRARY:
        return 'medialibrary' + bg.id;
      case Types.CURATED:
        return 'curated' + bg.id;
      case Types.GRADIENT:
        return JSON.stringify(bg)
    }
    console.error(bg);
    return '';
};

const FLICKR_ATTRS = {
    '4': 'By OWNERNAME, licensed CC BY 2.0, via Flickr',
    '7': 'Via Flickr Commons',
    '9': 'Licensed CC0, via Flickr',
    '10': 'Public Domain Mark 1.0, via Flickr'
}
export function backgroundToAttribution(bg) {
    if (!bg)  return null;

    switch (bg.type) {
      case Types.SPLASHBASE:
        return { text: 'Licensed CC0 via Splashbase' };
      case Types.UNSPLASH:
        return { text: 'Licensed CC0 via Unsplash',
                 url: 'https://www.unsplash.com/' };
      case Types.FLICKR:
        return { text: FLICKR_ATTRS[bg.license]
                        .replace('OWNERNAME', bg.ownername) };
      case Types.SUBTLEPATTERN:
        return { text: `By ${bg.creator}, Licensed CC BY-SA 3.0, via Subtle Patterns` };
      case Types.CURATED:
        return { text: bg.attrText, url: bg.attrURL };
    }
    return null;
};
