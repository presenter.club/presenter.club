/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const COLOR_PALETTES = {
    material: {
        title: 'Material Design',
        colors: [
            '#f44336', '#e91e63', '#9c27b0', '#673ab7',
            '#3f51b5', '#2196f3', '#03a9f4', '#00bcd4',
            '#009688', '#4caf50', '#8bc34a', '#cddc39',
            '#ffeb3b', '#ffc107', '#ff9800', '#ff5722',
            '#795548', '#9e9e9e', 'black', '#607d8b', 'white'
        ]
    },
    tango: {
        title: 'Tango',
        colors: [
            '#eeeeec', '#d3d7cf', '#babdb6',
            '#fce94f', '#edd400', '#c4a000',
            '#8ae234', '#73d216', '#4e9a06',
            '#fcaf3e', '#f57900', '#ce5c00',
            '#e9b96e', '#c17d11', '#8f5902',
            '#729fcf', '#3465a4', '#204a87',
            '#ad7fa8', '#75507b', '#5c3566',
            '#888a85', '#555753', '#2e3436',
            '#ef2929', '#cc0000', '#a40000'
        ]
    },
    metro: {
        title: 'Metro',
        colors: [
            '#99b433', '#00a300', '#1e7145', '#ff0097', '#9f00a7',
            '#7e3878', '#603cba', '#1d1d1d', '#00aba9', '#eff4ff',
            '#2d89ef', '#2b5797', '#ffc40d', '#e3a21a', '#da532c',
            '#ee1111', '#b91d47', 'white'
        ]
    },
    ios: {
        title: 'Bubble',
        colors: [
            '#5AC8FA', '#ffcc00', '#ff9500', '#ff2d55',
            '#007aff', '#4cd964', '#ff3B30', '#8e8e93',
            '#efeff4', '#cecef2', 'black', '#007aff'
        ]
    }
}
export default COLOR_PALETTES;
