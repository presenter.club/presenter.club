/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

export const Align = {
    TOP: 'top',
    BOTTOM: 'bottom',
    MIDDLE: 'middle',
    LEFT: 'left',
    RIGHT: 'right',
    CENTER: 'center'
};

export const SplitType = {
    VERTICAL: 'v',  // a is on top of b
    HORIZONTAL: 'h'  // a is left, b is right
};

export const SLIDE_TYPES = {
    'none': {
        tooltip: 'Only Background'
    },
    'title': {
        root: {
            block: 0,
            align: { x: Align.CENTER, y: Align.MIDDLE },
        },
        suggestedBlocks: [
            'h1'
        ],
    },
    'title-and-subtitle': {
        root: {
            split: SplitType.VERTICAL,
            pos: 50,
            a: {
                block: 0,
                align: { x: Align.CENTER, y: Align.BOTTOM },
            },
            b: {
                block: 1,
                align: { x: Align.CENTER, y: Align.TOP },
            }
        },
        suggestedBlocks: [
            'h1',
            'h2'
        ],
    },
    'title-bottom-left': {
        root: {
            block: 0,
            align: { x: Align.LEFT, y: Align.BOTTOM },
        },
        suggestedBlocks: [
            'h1',
        ],
    },
    'title-and-subtitle-top-bottom': {
        root: {
            split: SplitType.VERTICAL,
            pos: 50,
            a: {
                block: 0,
                align: { x: Align.CENTER, y: Align.TOP },
            },
            b: {
                block: 1,
                align: { x: Align.CENTER, y: Align.BOTTOM },
            }
        },
        suggestedBlocks: [
            'h1',
            'h2'
        ],
    },
    'title-and-subtitle-bottom-top': {
        root: {
            split: SplitType.VERTICAL,
            pos: 50,
            a: {
                block: 1,
                align: { x: Align.CENTER, y: Align.TOP },
            },
            b: {
                block: 0,
                align: { x: Align.CENTER, y: Align.BOTTOM },
            }
        },
        suggestedBlocks: [
            'h1',
            'h2'
        ],
    },
    'fullscreen-block': {
        root: {
            block: 0,
            align: { x: Align.CENTER, y: Align.MIDDLE }
        },
        suggestedBlocks: [
            null
        ],
    },
    'splith-blocks': {
        root: {
            split: SplitType.HORIZONTAL,
            pos: 50,
            a: {
                block: 0,
                align: { x: Align.RIGHT, y: Align.MIDDLE },
            },
            b: {
                block: 1,
                align: { x: Align.LEFT, y: Align.MIDDLE },
            }
        },
        suggestedBlocks: [
            null,
            null
        ],
    },
    'block-with-title': {
        root: {
            split: SplitType.VERTICAL,
            pos: 50,
            a: {
                block: 0,
                align: { x: Align.CENTER, y: Align.BOTTOM },
            },
            b: {
                block: 1,
                align: { x: Align.CENTER, y: Align.TOP },
            }
        },
        suggestedBlocks: [
            'h1',
            null
        ],
    },
    'splith-blocks-with-title': {
        root: {
            split: SplitType.VERTICAL,
            pos: 50,
            a: {
                block: 0,
                align: { x: Align.CENTER, y: Align.BOTTOM },
            },
            b: {
                split: SplitType.HORIZONTAL,
                pos: 50,
                a: {
                    block: 1,
                    align: { x: Align.RIGHT, y: Align.MIDDLE },
                },
                b: {
                    block: 2,
                    align: { x: Align.LEFT, y: Align.MIDDLE },
                }
            }
        },
        suggestedBlocks: [
            'h1',
            null,
            null
        ],
    },
    'splith-blocks-split-right': {
        root: {
            split: SplitType.HORIZONTAL,
            pos: 50,
            a: {
                block: 0,
                align: { x: Align.RIGHT, y: Align.MIDDLE },
            },
            b: {
                split: SplitType.VERTICAL,
                pos: 50,
                a: {
                    block: 1,
                    align: { x: Align.CENTER, y: Align.MIDDLE },
                },
                b: {
                    block: 2,
                    align: { x: Align.CENTER, y: Align.MIDDLE },
                }
            }
        },
        suggestedBlocks: [
            null,
            null,
            null
        ],
    },
    'splith-blocks-split-left': {
        root: {
            split: SplitType.HORIZONTAL,
            pos: 50,
            a: {
                split: SplitType.VERTICAL,
                pos: 50,
                a: {
                    block: 1,
                    align: { x: Align.CENTER, y: Align.MIDDLE },
                },
                b: {
                    block: 2,
                    align: { x: Align.CENTER, y: Align.MIDDLE },
                }
            },
            b: {
                block: 0,
                align: { x: Align.LEFT, y: Align.MIDDLE },
            },
        },
        suggestedBlocks: [
            null,
            null,
            null
        ],
    },
    'splitv-blocks': {
        root: {
            split: SplitType.VERTICAL,
            pos: 50,
            a: {
                block: 0,
                align: { x: Align.CENTER, y: Align.BOTTOM },
            },
            b: {
                block: 1,
                align: { x: Align.CENTER, y: Align.TOP },
            }
        },
        suggestedBlocks: [
            null,
            null
        ],
    },
};
