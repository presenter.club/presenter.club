/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as backgrounds from '../constants/backgrounds.jsx';

function getThemeByName(name) {
    return THEMES[name] || THEMES['presenterclub'];
}
export default getThemeByName;


const MATERIAL_COLORS = [
    '#f44336', '#e91e63', '#9c27b0', '#673ab7',
    '#3f51b5', '#2196f3', '#03a9f4', '#00bcd4',
    '#009688', '#4caf50', '#8bc34a', '#cddc39',
    '#ffeb3b', '#ffc107', '#ff9800', '#ff5722',
    '#795548', '#9e9e9e', 'black', '#607d8b', 'white'
];
export const THEMES = {
    'presenterclub': {
        title: 'Presenter Club',
        colors: MATERIAL_COLORS,
        images: [
            {
                url: 'http://subtlepatterns2015.subtlepatterns.netdna-cdn.com/patterns/congruent_pentagon.png',
                type: 'subtlepattern',
                creator: 'Atlo Mo'
            }, {
                url: 'http://subtlepatterns2015.subtlepatterns.netdna-cdn.com/patterns/asanoha-400px.png',
                type: 'subtlepattern',
                creator: 'Olga Libby'
            }, {
                url: 'http://subtlepatterns2015.subtlepatterns.netdna-cdn.com/patterns/footer_lodyas.png',
                type: 'subtlepattern',
                creator: 'Carlos Aguilar'
            }, {
                url: 'http://subtlepatterns2015.subtlepatterns.netdna-cdn.com/patterns/cork-wallet.png',
                type: 'subtlepattern',
                creator: 'Atlo Mo'
            },
        ],
    },
    'shootingstar': {
        title: 'Shooting Star',
        colors: ['#F6F6F6', '#F6F6DE', '#f9dba2', '#BCF2FF', '#DCFBD2', '#F1E3E3',
                 '#FAC564', '#89E7FF', '#B7F99F', '#FFCCCC', '#555', 'black', 'white'],
        images: [
            {
                url: 'url("http://ubtlepatterns.netdna-cdn.com/patterns/skulls.png',
                type: 'subtlepattern',
                creator: 'Adam'
            }, {
                url: 'http://subtlepatterns2015.subtlepatterns.netdna-cdn.com/patterns/restaurant_icons.png',
                type: 'subtlepattern',
                creator: 'Andrijana Jarnjak'
            }, {
                url: 'http://subtlepatterns2015.subtlepatterns.netdna-cdn.com/patterns/food.png',
                type: 'subtlepattern',
                creator: 'Ilya'
            }, {
                url: 'http://subtlepatterns2015.subtlepatterns.netdna-cdn.com/patterns/paisley.png',
                type: 'subtlepattern',
                creator: 'Swetha'
            }, {
                url: 'http://subtlepatterns2015.subtlepatterns.netdna-cdn.com/patterns/pink_rice.png',
                type: 'subtlepattern',
                creator: 'ExcogitoWeb'
            },
        ],
    },
    'risinghope': {
        title: 'Rising Hope',
        // Inspiration is https://unsplash.com/photos/W_9mOGUwR08
        colors: ['#040008', '#CC5004', '#F4A500', '#F6F700', '#F43B00',
                 '#920070', '#B43D84', '#260070', '#324BA1', '#2FB095',
                 '#6BDC84', '#C7E97C', '#F9FEAC', '#FBFFBB', 'black',
                 'white'],
        images: [
            {
                full: 'https://images.unsplash.com/photo-1443827423664-eac70d49dd0d?dpr=1&auto=format&crop=entropy&fit=crop&w=1920&q=80',
                thumb: 'https://images.unsplash.com/photo-1443827423664-eac70d49dd0d?dpr=1&auto=format&https://images.unsplash.com/photo-1443827423664-eac70d49dd0d?dpr=1&auto=format&crop=entropy&fit=crop&w=192&q=80crop=entropy&fit=crop&w=192&q=80',
                creator: 'Todd DeSantis',
                type: 'unsplash'
            }, {full:"https://images.unsplash.com/38/L2NfDz5SOm7Gbf755qpw_DSCF0490.jpg?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=1c4a8222e8d170f8124dfbee66776f84","thumb":"https://images.unsplash.com/38/L2NfDz5SOm7Gbf755qpw_DSCF0490.jpg?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=200&fit=max&s=7ff48d78c8d9225687100364572c93ae","type":"unsplash"
            }, {"full":"https://images.unsplash.com/photo-1415045384817-2f9cf7f2ed79?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=ad6d2b2fdd209da23b251b4e7d36491a","thumb":"https://images.unsplash.com/photo-1415045384817-2f9cf7f2ed79?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=200&fit=max&s=828bfd40d3787de282bfc643adb9c29c","type":"unsplash"
            }, {"url":"https://splashbase.s3.amazonaws.com/unsplash/regular/tumblr_n3tsz9iUPi1st5lhmo1_1280.jpg","type":"splashbase"
            // FIXME:  rehost pixbay images
            },/* {
                full: 'url("https://pixabay.com/get/e836b50a2ff0063ed1534705fb0938c9bd22ffd41db7164993f7c57aa2/forest-1345747_1920.jpg")',
                thumb: 'url("https://pixabay.com/static/uploads/photo/2016/04/22/13/16/forest-1345747_640.jpg")',
                attr: 'Valentin Sabau, Licensed CC0, via Pixbay'
            }, {
                full: 'url("https://pixabay.com/get/e836b50a2ff0043ed1534705fb0938c9bd22ffd41db7164993f7c47da3/rays-1345745_1920.jpg")',
                thumb: 'url("https://pixabay.com/static/uploads/photo/2016/04/22/13/16/rays-1345745_640.jpg")',
                attr: 'Valentin Sabau, Licensed CC0, via Pixbay'
            }, {
                full: 'url("https://pixabay.com/get/e836b50a2ff0073ed1534705fb0938c9bd22ffd41db7164993f7c470af/mountain-1345746_1920.jpg")',
                thumb: 'url("https://pixabay.com/static/uploads/photo/2016/04/22/13/16/mountain-1345746_640.jpg")',
                attr: 'Valentin Sabau, Licensed CC0, via Pixbay'
            }*/
        ],
    }
};
