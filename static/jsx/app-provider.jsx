/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { createStore, compose, applyMiddleware } from 'redux';
import { Provider } from 'react-redux'
import request from 'superagent';

import AppReducer from './reducers/reducers.jsx';
import ReduxMiddleware from './reducers/middleware.jsx';


try {
    const csrf = document.cookie.match(/csrftoken=(.*?)(?:$|;)/)[1];
    const oldEnd = request.Request.prototype.end;
    request.Request.prototype.end = function(fn) {
        if (this.url[0] === '/') {
            // Only if it is a request to the Presenter Club domain
            this.set('X-CSRFToken', csrf);
        }
        return oldEnd.call(this, fn);
    };
} catch (err) {
    console.error('CSRF', err);
}

const store = createStore(
    AppReducer, undefined,
    compose(
        applyMiddleware(...ReduxMiddleware),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    )
);

const AppProvider = ({ children }) => {
    return <Provider store={ store }><div>
        { children }
    </div></Provider>;
};
export default AppProvider;
