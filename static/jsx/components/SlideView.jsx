/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { backgroundToCss,
         backgroundToAttribution } from '../constants/backgrounds.jsx';

const SlideLogo = ({ presentationStyle, attr }) => {
    const model = (presentationStyle || {}).logo || {};
    const image = model.image !== undefined;
    const src = (model.image || {}).full;

    const showShovel = window.enableHeavyBranding || false;

    return <div className='SlideLogo'>
        { image ?
            <img className='SlideLogo__img'
                 style={{
                    width: model.width || 40
                 }}
                 src={ src }
                 alt='Logo' />
            : null
        }
        { attr !== null ?
            <div className='SlideLogo__attr'>Background { attr.text }</div>
            : null
        }
        { showShovel ?
            <a href='/' className='SlideLogo__shovel'>
                { 'make great presentations with,' }
                <span className='SlideLogo__shovel__next'>Sign Up!</span>
                <div className='SlideLogo__shovel__title'>Presenter Club</div>
            </a>
            : null
        }
    </div>
};

export const StaticSlideView = ({ style, logo = true, presentationStyle,
                                  onClick, className, row, children }) => {
    const defaultStyle = (((presentationStyle || {}).previews || {}).default || {});
    row = Object.assign({}, defaultStyle, row);

    let css = Object.assign({}, style, backgroundToCss(row.background));
    css.color = row.slideFgText || 'black';
    if (row.slideFgShadow === null) {
        css.textShadow = 'none';
    } else {
        css.textShadow = `0 0 15px ${ row.slideFgShadow }`;
    }

    const attr = backgroundToAttribution(row.background);


    return <div className={ 'slide-parent slide-bg-image ' + className }
                style={ css }
                onClick={ onClick }>
        { children }
        { logo ? <SlideLogo { ...{ presentationStyle, attr } } />
               : <noscript />
        }
    </div>;
};

export const SlideThumb = ({ logo = false, className, ...props }) => {
    return <StaticSlideView className={ 'SlideThumb ' + className }
                            logo={ logo }
                            {...props} />
}

