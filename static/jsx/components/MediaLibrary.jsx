/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { connect } from 'react-redux';
import Dropzone from 'react-dropzone';

import * as MediaLibraryActions from '../actions/medialibrary.jsx';
import Loader from './Loader.jsx';

export const StyledDropzone = ({ children, ...props }) => {
    return <Dropzone { ...props }
                     className='medialibrary__dropzone'
                     activeClassName='medialibrary__dropzone--active'
                     rejectClassName='medialibrary__dropzone--rejected'>
        { children }
    </Dropzone>
};

const Center = ({ children }) => {
    return <div className='Sidebar__center'>
        { children }
    </div>;
}

const ManageMediaLibrary = () => {
    return <Center>
        <a className='Sidebar__link'
           title='Opens in New Tab'
           target='blank' href='../medialibrary'>
            { /* FIXME: Move to icon font */ }
            Manage Media Library ↗
        </a>
    </Center>;
};

const _MediaLibrary = ({ medialibrary, dispatch, onSelect, selected }) => {
    if (!medialibrary.loaded) {
        setTimeout(dispatch(MediaLibraryActions.update()), 0);
        return <p>Not loaded yet</p>;
    } else if (medialibrary.loading) {
        return <Loader />;
    } else if (medialibrary.error) {
        const { error } = medialibrary;
        let text = JSON.stringify(error);
        if (error.error === 'quota')  text = 'Upload quota exceded';
        if (error.error === 'unsupported') {
            text = 'Image format not supported.  '
                   + 'Please use PNG, JPEG, GIF or WebP';
        }
        return <div>
            <p><b>Error Uploading Image</b></p>
            <p>{ text }</p>
            <p><button onClick={ () => dispatch(MediaLibraryActions.update()) }>
                Reload
            </button></p>
            <ManageMediaLibrary />
        </div>;
    } else if (medialibrary.loadErr) {
        return <div>
            <p>Error loading images</p>
            <p><button onClick={ () => dispatch(MediaLibraryActions.update()) }>
                Reload
            </button></p>
            <ManageMediaLibrary />
        </div>;
    } else {
        let content = <p>No Images</p>;
        if (medialibrary.images.length > 0) {
            content = <div className='BackgroundPreviews'>
                { medialibrary.images.map((image) => {
                    let cn = 'BackgroundPreviews__item slide-bg-image clickable';
                    if (image.id === selected ) {
                        cn = cn + ' BackgroundPreviews__item--selected';
                    }
                    return <li
                        className={ cn }
                        key={ image.id }
                        onClick={ () => onSelect(image) }
                        style={{ backgroundImage: `url("${ image.thumb }")` }} />
                })}
            </div>;
        }
        return <div>
            <StyledDropzone onDrop={ (files) => {
                MediaLibraryActions.upload(dispatch, files[0], onSelect);
                      }}
                      accept='image/*'
                      multiple={ false }>
                Upload
            </StyledDropzone>

            { content }

            <ManageMediaLibrary />
            <Center>
                <a className='Sidebar__link clickable'
                   onClick={ () => dispatch(MediaLibraryActions.update()) }>
                    Refresh Images
                </a>
            </Center>
        </div>;
    }
};
_MediaLibrary.propTypes = {
    selected: React.PropTypes.number,  // Selected image id
    onSelect: React.PropTypes.func,  // onSelect(image)
};
const mapStateToProps = (state) => {
    return {
        medialibrary: state.medialibrary
    };
};
const MediaLibrary = connect(mapStateToProps)(_MediaLibrary);
export default MediaLibrary;
