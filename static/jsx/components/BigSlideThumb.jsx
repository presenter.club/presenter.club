/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { connect } from 'react-redux';

import { unwrap } from '../reducers/presentation.jsx';
import { StaticSlideView } from './SlideView.jsx';
import { SlideInner } from './SlideInner.jsx';
import { DjangoStyleCSS } from './style/CSS.jsx';


const _BigSlideThumb = ({ row }) => {
    return <div>
        <DjangoStyleCSS />
        <StaticSlideView row={ row } className='big-slide-thumb'
                         logo={ false }>
            <SlideInner editable={ false }
                        scale={ 0.5 }
                        presentationStyle={ window.djangosStyle }
                        row={ row } />
        </StaticSlideView>
    </div>;
};
const mapStateToProps = (state) => {
    const slides = unwrap(state.presentation).speech.filter((row) => {
        return row.slideType !== null;
    });
    return {
        row: slides[0]
    };
};
const BigSlideThumb = connect(mapStateToProps)(_BigSlideThumb);
export default BigSlideThumb;
