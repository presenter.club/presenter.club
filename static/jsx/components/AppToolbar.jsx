/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { connect } from 'react-redux';

import { doSave } from '../actions/savestatus.jsx';
import { unwrap } from '../reducers/presentation.jsx';
import * as PresentationActions from '../actions/presentation.jsx';
import * as EditViewActions from '../actions/editview.jsx';

import FullScreenView from './FullScreenView.jsx';
import ToolbarButton from './ToolbarButton.jsx';
import ToolbarSpacer from './ToolbarSpacer.jsx';
import PresentationPlayer from './PresentationPlayer.jsx';
import Icons from '../constants/icons.jsx';
import Privacy from '../constants/privacy.jsx';

export const SaveButton = ({ saveable, name, dispatch, data }) => {
    if (saveable.saving) {
        return <ToolbarButton className='fontello fontello--spin'
                              title='Saveing...'>
            { Icons.SPINNER }
        </ToolbarButton>;
    } else if (saveable.error !== null) {
        // TODO: Get warning icon
        return <ToolbarButton title={ 'Error: ' + saveable.error }>
            <b>!</b>
        </ToolbarButton>;
    } else if (saveable.dirty) {
        return <ToolbarButton onClick={ () => doSave(dispatch, name, data) }
                              className='fontello'
                              title='Unsaved, click to save'>
            { Icons.SAVE }
        </ToolbarButton>;
    } else {
        return <ToolbarButton disabled
                              className='fontello'
                              title='Saved'>
            { Icons.CHECK }
        </ToolbarButton>;
    }
};
const PresentationSaveButton = connect((state) => ({
    saveable: state.presentation,
    data: unwrap(state.presentation),
    name: 'Presentation'
}))(SaveButton);

class _PresentButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = { presenting: false, slide: 0 };
    }

    render() {
        const { presenting } = this.state;
        let presentation = <noscript />;
        if (presenting) {
            const onClose = () => this.setState({ presenting: false,
                                                  slide: 0 });
            const onCS = slide => this.setState({ slide });
            presentation = <FullScreenView onClose={ onClose }>
                { ({ width, height }) =>
                    <PresentationPlayer presentation={ this.props.presentation }
                                        fullscreen={ true }
                                        onChangeSlide={ onCS }
                                        presentationStyle={ this.props.presentationStyle }
                                        slide={ this.state.slide }
                                        { ...{ width, height } } />
                }
            </FullScreenView>;
        }

        return <div className='toolbar__useless'>
            { presentation }
            <ToolbarButton className={ 'fontello' + (presenting ? 'special' : '') }
                 title={ this.state.presenting ? 'Present' : '' }
                 onClick={ () => this.setState({ presenting: !presenting }) }>
                { this.state.presenting ?
                    'Exit Presentation'
                    : Icons.PLAY }
            </ToolbarButton>
        </div>;
    }
};
const PresentButton = connect((state) => ({
    presentation: unwrap(state.presentation),
    presentationStyle: state.stylecache.data
}))(_PresentButton);

export const UndoButton = ({ name, future, past, dispatch }) => {
    /*
     * Displays an undo and redo button for a given redux store.
     * NOTE:  You can only have one of these visible at a time - it globally
     *        binds for the keyboard shortcuts
     */
    window.handleUndo = () => dispatch({ type: name+'/UNDO' });
    window.handleRedo = () => dispatch({ type: name+'/REDO' });
    return <div className='toolbar__useless'>
        <ToolbarButton onClick={ handleUndo }
                       shortcut={{ key: 122, ctrlKey: true }}
                       disabled={ past.length === 0 }
                       className='join-leftmost fontello'
                       title='Undo (C-Z)'>
            { Icons.UNDO }
        </ToolbarButton>
        <ToolbarButton onClick={ handleRedo }
                       shortcut={{ ctrlKey: true, key: 90 }}
                       disabled={ future.length === 0 }
                       className='join-rightmost fontello'
                       title='Redo (C-S-Z)'>
            { Icons.REDO }
        </ToolbarButton>
    </div>;
};
export const PresentationUndoButton = connect((state) => ({
    future: state.presentation.content.future,
    past: state.presentation.content.past,
    name: 'Presentation'
}))(UndoButton);

const _TitleBar= ({ dispatch, title, overlay, isPrivate }) => {
    const showOverlay = (event) => {
        dispatch(EditViewActions.setOverlay(!overlay));
    }

    return <ToolbarSpacer>
        <div className='clickable TitleBar'
             onClick={ showOverlay }>
            <span className='TitleBar__privacy fontello'>
                { isPrivate ? Icons.LOCKED : Icons.UNLOCKED }
            </span>
            { title }
            <span className='TitleBar__edit fontello'>
                { overlay ? Icons.UP : Icons.EDIT }
            </span>
        </div>
    </ToolbarSpacer>;
};
const TitleBar = connect((state) => ({
    title: unwrap(state.presentation).metadata.title,
    isPrivate: unwrap(state.presentation).metadata.privacy === Privacy.PRIVATE,
    overlay: state.editview.overlay
}))(_TitleBar);

export const Toolbar = ({ children }) => {
    return <div>
        <header id='header' className='toolbar'>
            { children }
        </header>
        <div className='header-padding-container' />
    </div>;
};

const AppToolbar = () => {
    return <Toolbar>
        <h1 className='toolbar__title'><a href='..'>
            Home
        </a></h1>
        <PresentationUndoButton />
        <TitleBar />
        <PresentationSaveButton />
        <PresentButton />
    </Toolbar>;
};
export default AppToolbar;
