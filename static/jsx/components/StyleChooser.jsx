/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import request from 'superagent';

import Loader from './Loader.jsx';

const StylePreview = ({ style, selected, onClick }) => {
    let cn = 'StylePreviews__item';
    if (selected)  cn += ' StylePreviews__item--selected';

    return <img onClick={ onClick }
                className={ cn }
                src={ `/u/${ style.user }/styles/${ style.id }/thumb.png` }
                alt={ `Preview for ${ style.title } style` } />;
};

const StylePreviews = ({ styles, selected, onSelect }) => {
    return <div className='StylePreviews'>
        { styles.map(style =>
            <StylePreview style={ style }
                          key={ style.id }
                          selected={ style.id === selected }
                          onClick={ () => onSelect(style) } />
        )}
    </div>;
}

class StyleChooser extends React.Component {
    constructor() {
        super();
        this.state = { loading: true };
        this.reload();
    }

    reload() {
        this.setState({ loading: true });
        
        const r = request.get(`/u/${ window.djangosUser.username }/styles/api`)
                         .end((err, resp) => {
            if (err) {
                console.error(err);
            } else {
                this.setState({
                    loading: false,
                    data: resp.body
                });
            }
        });
    }

    render() {
        if (this.state.loading)  return <Loader />;

        const userIsPro = window.djangosUser.isPro;
        const selected = (this.props.style || {}).id;
        let content;
        if (this.state.data.mine.length === 0) {
            if (userIsPro) {
                content = <p>Visit <a href='../styles'>the styles home page
                             </a> to learn more about making your own
                             styles.</p>;
            } else {
                content = <p>With Presenter Club Pro, you create make your
                     own styles.  <a href='/plans'>Learn more about Pro,
                     </a> or <a href='/go-pro'>go pro now</a>.</p>
            }
        } else {
            content = <StylePreviews styles={ this.state.data.mine }
                                     selected={ selected }
                                     onSelect={ this.props.onSelect } />;
        }

        return <div>
            <p>
                <label>My Styles</label>
                { content }
                <label>Designer Styles</label>
                <StylePreviews styles={ this.state.data.designer }
                               selected={ selected }
                               onSelect={ this.props.onSelect } />
            </p>
        </div>;
    }
}
export default StyleChooser;
