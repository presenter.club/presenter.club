/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { DraggableCore } from 'react-draggable';

import * as PresentationActions from '../actions/presentation.jsx';
import * as EditViewActions from '../actions/editview.jsx';
import { unwrap } from '../reducers/presentation.jsx';
import QuillNoToolbar from './QuillNoToolbar.jsx';
import { SlideThumb } from './SlideView.jsx';
import { SlideInner } from './SlideInner.jsx';
import { SlideEditor } from './SlideEditor.jsx';
import Icons from '../constants/icons.jsx';

const RowDragBar = ({ onPositionChange, flex }) => {
    const dragCb = (event, { node, deltaX }) => {
        onPositionChange(deltaX / document.body.clientWidth * 100);
    };

    return <DraggableCore
        axis='x'
        onStart={ dragCb }
        onStop={ dragCb }
        onDrag={ dragCb }>
        <div className='DragBar DragBar--v' />
    </DraggableCore>;
}

const Columns = {
    IDEA: 'idea',
    ELABORATION: 'elaboration',
    SLIDE: 'slide'
};
const SpeechRowCell = ({ children, header, empty, col, ...props }) => {
    const bn = v => `SpeechRow__cell${ v }`
                    + (header ? ` SpeechRow--header__cell${ v }` : '');
    let cn = bn('') + ' ' + bn('--' + col);
    if (empty)  cn += ' ' + bn('--empty');

    return <div className={ cn } { ...props }>
        { children }
    </div>;
};

const IdeaCellWidth = ({ pos }) => {
    if (pos < 1) pos = 1;
    if (pos > 99) pos = 99;
    return <style>
        .SpeechRow__cell--idea { '{' }
            width: { pos }%;
        { '}' }
        .SpeechRow__cell--elaboration { '{' }
            width: calc(100% - { pos }% - 192px - 38px);
        { '}' }
    </style>;
};

let rowIJustCreated = null;

class SpeechRow extends React.Component {
    constructor(props) {
        super(props);
    }

    onTab(shiftKey) {
        const i = shiftKey ? Math.max(this.props.row.indent - 1, 0)
                             : this.props.row.indent + 1;
        this.props.dispatch(PresentationActions.editRow(
            this.props.row.uuid,
            { indent: i }
        ));
        return false;
    }

    onEditorChange(editor) {
        return (newState) => {
            this.props.dispatch(PresentationActions.editRow(
                this.props.row.uuid,
                { [editor]: newState }
            ));
        };
    }

    onReturn() {
        const q = this.idea.getQuill();
        const sel = q.getSelection() || {index: null, length: null};
        const length = q.getLength();

        const mine = q.getText(0, sel.index);
        const theirs = q.getText(sel.index, length - 1);

        rowIJustCreated = PresentationActions.uuid4();
        this.props.dispatch(PresentationActions.addRow(
            this.props.row.uuid,
            { indent: this.props.row.indent,
              uuid: rowIJustCreated,
              idea: theirs },
            { idea: mine }
        ));
        return false;
    }

    onRight() {
        const q = this.idea.getQuill();
        const sel = q.getSelection() || {index: null, length: null};
        if (sel.index === q.getLength()-1) {
            this.execution.focus();
            return false;
        }
        return true;
    }
    onLeft() {
        const q = this.execution.getQuill();
        const sel = q.getSelection() || {index: null, length: null};
        if (sel.index === 0) {
            this.idea.focus();
            const iq = this.idea.getQuill();
            const end = iq.getLength() - 1;
            iq.setSelection(end, end);
            return false;
        }
        return true;
    }

    onBackspace() {
        const q = this.idea.getQuill();
        const sel = q.getSelection() || {index: null, length: null};
        if (sel.index === 0 && sel.length === 0) {
            if (this.props.row.indent > 0) {
                this.props.dispatch(PresentationActions.editRow(
                    this.props.row.uuid,
                    { indent: this.props.row.indent - 1 }
                ));
            } else {
                this.props.requestFocusAbove(-1, 'i');
                this.props.dispatch(PresentationActions.deleteRow(
                    this.props.row.uuid
                ));
            }
            return false;
        }
        return true;
    }

    onSlideClick() {
        if (!this.props.editable)  return;

        this.props.dispatch(EditViewActions.selectSlide(
            this.props.selected ? undefined : this.props.row.uuid
        ));
    }

    ideaRef(idea) {
        this.idea = idea;
        if (this.props.row.uuid === rowIJustCreated) {
            rowIJustCreated = null;
            idea.focus();
        }
    }

    executionRef(execution) {
        this.execution = execution;
    }

    focusIdea() {
        this.idea.focus();
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.row !== this.props.row
            || nextProps.presentationStyle !== this.props.presentationStyle
            || nextProps.selected !== this.props.selected;
    }

    onAddSlideClick() {
        this.props.dispatch(PresentationActions.addSlide(
            this.props.row.uuid
        ));
    }

    requestFocusAbove(t) {
        const editor = (t === 'i' ? this.idea : this.execution).getQuill();
        const bounds = editor.getBounds(editor.getSelection().index);
        const y = bounds.top - bounds.height;
        if (y <= 0) {
            this.props.requestFocusAbove(bounds.left, t);
            return false;
        }
        return true;
    }
    requestFocusBelow(t) {
        const editor = (t === 'i' ? this.idea : this.execution).getQuill();
        const bounds = editor.getBounds(editor.getSelection().index);
        const endBounds = editor.getBounds(editor.getLength());
        if (bounds.bottom === endBounds.bottom) {
            this.props.requestFocusBelow(bounds.left, t);
            return false;
        }
        return true;
    }

    render() {
        const { row } = this.props;

        let slideThumb = null;
        if (row.slideType === null && this.props.editable) {
            slideThumb = <button onClick={ this.onAddSlideClick.bind(this) }
                                 className='add-slide small fontello'>
                { '+' }
            </button>;
        } else if (this.props.selected) {
            slideThumb = <div className='fontello clickable SpeechRow__close'
                              onClick={ this.onSlideClick.bind(this) }>
                { Icons.UP }
            </div>;
        } else if (row.slideType !== null) {
            slideThumb = <SlideThumb
                row={ row }
                presentationStyle={ this.props.presentationStyle }
                className='clickable'
                onClick={ this.onSlideClick.bind(this) }>
                <SlideInner editable={ false }
                            scale={ 0.3 }
                            presentationStyle={ this.props.presentationStyle }
                            row={ row } />
            </SlideThumb>;
        }

        /* Never hide the elaboration in edit mode */
        let elaborationEmpty = !this.props.editable;
        if (elaborationEmpty) {
            /* Now actually check if it is empty */
            const node = document.createElement('div');
            node.innerHTML = row.execution;
            elaborationEmpty = !(node.textContent || node.innerText);
        }

        return <div className='SpeechRow' id={ 'SpeechRow--'+row.uuid }>
            <SpeechRowCell col={ Columns.IDEA }>
                <div className={ 'bullet idea n' + (row.indent % 4 + 1) }
                    style={{
                        paddingLeft: row.indent * 24
                    }}>
                    <QuillNoToolbar
                            value={ row.idea }
                            onChange={ this.onEditorChange('idea') }
                            ref={ this.ideaRef.bind(this) }
                            placeholder='Type your idea'
                            noBlockFormats
                            editable={ this.props.editable }
                            bindings={[
                                {
                                    key: 8,
                                    handler: this.onBackspace.bind(this)
                                },
                                {
                                    key: 9,
                                    handler: this.onTab.bind(this, false)
                                },
                                {
                                    key: 9,
                                    shiftKey: true,
                                    handler: this.onTab.bind(this, true)
                                },
                                {
                                    key: 38,
                                    handler: this.requestFocusAbove.bind(this, 'i')
                                },
                                {
                                    key: 40,
                                    handler: this.requestFocusBelow.bind(this, 'i')
                                },
                                {
                                    key: 39,
                                    handler: this.onRight.bind(this)
                                },
                                {
                                    key: 13,
                                    handler: this.onReturn.bind(this)
                                },
                            ]} />
                </div>
            </SpeechRowCell>
            <RowDragBar onPositionChange={ this.props.onPositionChange } />
            <SpeechRowCell col={ Columns.ELABORATION }
                           empty={ elaborationEmpty }>
                <QuillNoToolbar
                        value={ row.execution }
                        ref={ this.executionRef.bind(this) }
                        noBlockFormats
                        editable={ this.props.editable }
                        bindings={[
                            {
                                key: 37,
                                handler: this.onLeft.bind(this)
                            },
                            {
                                key: 38,
                                handler: this.requestFocusAbove.bind(this, 'e')
                            },
                            {
                                key: 40,
                                handler: this.requestFocusBelow.bind(this, 'e')
                            },
                        ]}
                        onChange={ this.onEditorChange('execution') } />
            </SpeechRowCell>
            <SpeechRowCell col={ Columns.SLIDE }
                           empty={ slideThumb === null }>
                { slideThumb }
            </SpeechRowCell>
        </div>;
    }
};

const SpeechViewDiv = ({ children }) => {
    return <div className='SpeechView'>{ children }</div>;
};

class _SpeechTableContents extends React.Component {
    constructor(props) {
        super(props);
        this.speechRows = {};
    }

    requestFocusAbove(uuid) {
        return (left, t) => {
            const editor = this._doRequestFocus(
                uuid, this.props.speech.length-1, 0, -1, t
            );
            editor.focus();
            if (left === -1) {
                editor.setSelection(editor.getLength() - 1, 0);
            } else if (left !== undefined) {
                let pos = editor.getLength() - 1;
                const bottomline = editor.getBounds(pos).top;
                while (true) {
                    if (pos <= 0) {
                        break;
                    }
                    if (editor.getBounds(pos-1).top !== bottomline ||
                        editor.getBounds(pos-1).left < left) {
                        break;
                    } else {
                        pos--;
                    }
                }
                editor.setSelection(pos, 0);
            }
        }
    }

    requestFocusBelow(uuid) {
        return (left, t) => {
            const editor = this._doRequestFocus(
                uuid, 0, this.props.speech.length-1, +1, t
            );
            editor.focus();
            if (left !== undefined) {
                let pos = 0;
                const topline = editor.getBounds(0).top;
                while (true) {
                    if (pos >= editor.getLength() - 1) {
                        break;
                    }
                    if (editor.getBounds(pos+1).top !== topline ||
                        editor.getBounds(pos+1).left > left) {
                        break;
                    } else {
                        pos++;
                    }
                }
                editor.setSelection(pos, 0);
            }
        }
    }

    _doRequestFocus(uuid, start, end, direction, t) {
        for (let i = start; i != end; i+=direction) {
            const row = this.props.speech[i];
            if (row.uuid === uuid) {
                const nextuuid = this.props.speech[i+direction].uuid;
                const row = this.speechRows[nextuuid];
                return (t === 'i' ? row.idea : row.execution).getQuill();
            }
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.speech !== this.props.speech
            || nextProps.presentationStyle !== this.props.presentationStyle
            || nextProps.selectedSlide !== this.props.selectedSlide;
    }

    render() {
        const { speech, dispatch, selectedSlide } = this.props;
        return <ReactCSSTransitionGroup
            component={ SpeechViewDiv }
            transitionName='SpeechViewTransition'
            transitionEnterTimeout={ 250 }
            transitionLeaveTimeout={ 250 }>
            { speech.map((row) => {
                const selected = selectedSlide == row.uuid
                                 && row.slideType !== null;
                const basic = <SpeechRow
                    key={ row.uuid }
                    row={ row }
                    requestFocusAbove={ this.requestFocusAbove(row.uuid) }
                    requestFocusBelow ={ this.requestFocusBelow(row.uuid) }
                    ref={ (ref) => this.speechRows[row.uuid] = ref }
                    selected={ selected }
                    dispatch={ dispatch }
                    presentationStyle={ this.props.presentationStyle }
                    editable={ this.props.editable }
                    onPositionChange={ this.props.onPositionChange } />
                return [
                    basic,
                    selected ?
                        <SlideEditor key={ row.uuid + '-editor' }
                                     scrollTo
                                     row={ row }
                                     dispatch={ dispatch } />
                        : null
                ];
            })}
        </ReactCSSTransitionGroup>
    }
};
const mapStateToProps = (state) => {
  return {
    speech: unwrap(state.presentation).speech,
    selectedSlide: state.editview.selectedSlide,
  }
};
const SpeechTableContents = connect(mapStateToProps)(_SpeechTableContents);

class SpeechTable extends React.Component {
    constructor() {
        super();
        this.state = { position: 50 };
        this.onPositionChange = delta => {
            this.setState({ position: this.state.position + delta });
        }
    }

    render() {
        return <div className='SpeechView'>
            <IdeaCellWidth pos={ this.state.position } />
            <div className='SpeechRow SpeechRow--header'>
                <SpeechRowCell header col={ Columns.IDEA }>
                    Idea
                </SpeechRowCell>
                <RowDragBar onPositionChange={ this.onPositionChange } />
                <SpeechRowCell header col={ Columns.ELABORATION }>
                    Elaboration
                </SpeechRowCell>
                <SpeechRowCell header col={ Columns.SLIDE }>
                    Slide
                </SpeechRowCell>
            </div>
            <SpeechTableContents onPositionChange={ this.onPositionChange }
                                 key='contents'
                                 presentationStyle={ this.props.presentationStyle }
                                 editable={ this.props.editable } />
        </div>;
    }
}
export default SpeechTable;
