/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { PropTypes } from 'react';

import { SLIDE_TYPES } from '../constants/slidetypes.jsx';
import { SlideBlock } from './blocks/index.jsx';
import SlideTemplateBlock from './SlideTemplateBlock.jsx';
import VisibilityStatus from '../constants/visibilityStatus.jsx';

export const SlideInner = ({ row, onBlockChange, onChange,
                             presentationStyle, visibilityStatus,
                             requestFocus, lightsOutFor,
                             scale = 1, style = {}, editable }) => {
    const type = typeof row.slideType === 'string' ?
                    SLIDE_TYPES[row.slideType]
                    : row.slideType;

    const defaultStyle = (((presentationStyle || {}).previews || {}).default || {});
    row = Object.assign({}, defaultStyle, row);

    const blocks = (row.blocks || []).map((block, i) => {
        let rf = false;
        if (!!requestFocus) {
            rf = (event) => {
                requestFocus(i);
                if (event !== undefined) {
                    event.stopPropagation();
                }
            };
        }
        return <SlideBlock key={ i } blockId={ i }
                    focused={ lightsOutFor === i }
                    onChange={ (change) => onBlockChange(i, change) }
                    requestFocus={ rf }
                    state={ block }
                    { ...{ scale, presentationStyle, editable, row,
                           visibilityStatus } } />
    });

    let onChangeSlideType = null;
    if (editable) {
        onChangeSlideType = root => onChange({
            slideType: Object.assign({}, type, { root })
        });
    }

    return <div className={ 'slide ' + type.className }
                style={ style }>
        <SlideTemplateBlock root={ type.root }
                            scale={ scale }
                            onChange={ onChangeSlideType }
                            lightsOutFor={ lightsOutFor }
                            blocks={ blocks } />
    </div>;
};
SlideInner.defaultProps = {
    visibilityStatus: VisibilityStatus.NORMAL
};
