/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';

export const GoBackTo = ({ onClick, title }) => {
    return <div className='SlideEditorSidebar__go_back clickable'
                onClick={ onClick }>
        ⟵ Back to
        <span className='SlideEditorSidebar__go_back__title'>
            { title }
        </span>
    </div>;
}

const SlideEditorSidebar = ({ title, children, goBackTo,
                              secondary = false }) => {
    let back = <noscript />;
    if (goBackTo !== undefined) {
        // FIXME:  Put the icon in the icon font for consistency
        back = <GoBackTo { ...goBackTo } />;
    }
    return <div className='SlideEditorSidebar'>
        <div className={ 'SlideEditorSidebar__header'
                        + (secondary ? ' SlideEditorSidebar__header--secondary' : '') }>
            <h3 className={ 'SlideEditorSidebar__title'
                            + (goBackTo ? '' : ' SlideEditorSidebar__title--only') }>
                { title }
            </h3>
            { back }
        </div>
        { children }
    </div>;
}
export default SlideEditorSidebar;
