/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { connect } from 'react-redux';

import { unwrap } from '../reducers/presentation.jsx';
import * as PresentationActions from '../actions/presentation.jsx';
import SlideEditorSidebar from './SlideEditorSidebar.jsx';
import SidebarSection from './SidebarSection.jsx';
import { bestContrast } from '../util/colors.jsx';

export const ColorPaletteColor = ({ onSelect, color,
                                    selected, className }) => {
    let c = 'clickable ColorPalette__color';
    if (color === 'white')  c = c + ' ColorPalette__color--white';
    if (selected)  c = c + ' ColorPalette__color--selected';
    if (className)  c = c + ' ' + className;
    return <li className={ c }
               onClick={ onSelect }
               style={{ background: color }} />
};

export const ColorPalette = ({ colors, onSelect, selected, children }) => {
    return <div className='ColorPalette'>
        { colors.map((color) =>
            <ColorPaletteColor color={ color }
                               selected={ color === selected }
                               key={ color }
                               onSelect={ () => onSelect(color) } />
        )}
        { children }
    </div>;
};

const _SidebarForeground = ({ row, dispatch, defaultStyle,
                              styleColors, goBackTo }) => {
    const onSelectForeground = (color) => {
        const contrasting = bestContrast(styleColors, color);
        dispatch(PresentationActions.editRow(
            row.uuid,
            { slideFgText: color, slideFgShadow: contrasting }
        ));
    };
    const onSelectShadow = (color) => {
        dispatch(PresentationActions.editRow(
            row.uuid,
            { slideFgShadow: color }
        ));
    };

    row = Object.assign({}, defaultStyle, row);

    return <SlideEditorSidebar title='Change Foreground Colors'
                               goBackTo={ goBackTo }>
        <SidebarSection initialExpansion title='Text Color'>
            <ColorPalette colors={ styleColors }
                          onSelect={ onSelectForeground }
                          selected={ row.slideFgText } />
        </SidebarSection>

        <SidebarSection initialExpansion title='Shadow Color'>
            <ColorPalette colors={ styleColors } onSelect={ onSelectShadow }
                          selected={ row.slideFgShadow } />
            <button className={ row.slideFgShadow === null ? 'special' : '' }
                    onClick={ () => onSelectShadow(null) }>No Shadow</button>
        </SidebarSection>
    </SlideEditorSidebar>;
};

const mapStateToProps = (state) => {
    return {
        row: unwrap(state.presentation).speech.find(
            row => row.uuid === state.editview.selectedSlide
        ),
        styleColors: state.stylecache.data.colors,
        defaultStyle: state.stylecache.data.previews.default,
    };
};
export const SidebarForeground = connect(mapStateToProps)(_SidebarForeground);
