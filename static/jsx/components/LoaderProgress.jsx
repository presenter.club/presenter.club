import React from 'react';

const _WIDTH = 192 / 2 * 1.1;

/* progress is a float (0 to 1) */
const LoaderProgress = ({ progress }) => {
    const text = Math.round(progress * 100) + '%';
    const cn = `LoaderProgress LoaderProgress--${ text.length - 1 }digit`;
    return <div className={ cn }>
        <div className='LoaderProgress__normal'>{ text }</div>
        <div className='LoaderProgress__insert'
             style={{ width: progress * _WIDTH }}>{ text }</div>
    </div>;
};
LoaderProgress.propTypes = {
    progress: React.PropTypes.number.isRequired
};
export default LoaderProgress;
