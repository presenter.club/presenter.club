/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';

class ToolbarButton extends React.Component {
    componentDidMount() {
        const { shortcut } = this.props;
        if (shortcut !== undefined) {
            document.body.addEventListener('keypress', (event) => {
                if (shortcut.ctrlKey && !event.ctrlKey)  return;
                if ((event.which || event.keyCode) === shortcut.key) {
                    this.props.onClick();
                }
            }, true);
            // Setting the 3rd argument to true gives this keypress event
            // priority over every other listener
        }
    }

    componentWillUnmount() {
        // FIXME:  Unbind shortcut
    }

    render() {
        const { children, shortcut, right, className, ...props } = this.props;

        let cn = 'toolbar__child';
        if (className)  cn = cn + ' ' + className;

        return <button className={ cn } { ...props }>
            { children }
        </button>
    }
};
export default ToolbarButton;
