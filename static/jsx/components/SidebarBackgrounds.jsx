/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { connect } from 'react-redux';
import request from 'superagent';

import { unwrap } from '../reducers/presentation.jsx';
import * as PresentationActions from '../actions/presentation.jsx';
import getThemeByName from '../constants/themes.jsx';
import { backgroundToCss, backgroundFromColor, backgroundToKey,
         colorFromBackground, Types,
         backgroundToAttribution } from '../constants/backgrounds.jsx';
import { ColorPalette } from './SidebarForeground.jsx';
import SlideEditorSidebar from './SlideEditorSidebar.jsx';
import SidebarSection from './SidebarSection.jsx';
import MediaLibrary from './MediaLibrary.jsx';
import Loader from './Loader.jsx';

export const BackgroundPreview = ({ bg, onSelect, selected }) => {
    const css = backgroundToCss(bg, true);
    const a = backgroundToAttribution(bg);
    let cn = 'BackgroundPreviews__item slide-bg-image clickable';
    if (selected)  cn = cn + ' BackgroundPreviews__item--selected';

    return <li className={ cn }
               onClick={ onSelect }
               title={ a === null ? '' : a.text }
               style={ css } />;
};

class SearchBar extends React.Component {
    onSearchChange() {
        const query = this.refs.search.value.toLowerCase();
        this.props.onChange(query);
    }

    onKeyPress(event) {
        if ((event.which || event.keyCode) === 13) {
            this.onSearchChange();
            this.props.activate();
        }
    }

    render() {
        return <input
            className='search'
            type='search'
            value={ this.props.query }
            ref='search'
            onKeyPress={ this.onKeyPress.bind(this) }
            onChange={ this.onSearchChange.bind(this) }
            placeholder='Search for images...' />;
    }
}

export const BackgroundPreviews = ({ data, selected, onSelect }) => {
    const skey = backgroundToKey(selected);
    return <div className='BackgroundPreviews'>
        { data.map((bg) =>
            <BackgroundPreview key={ backgroundToKey(bg) }
                               bg={ bg }
                               selected={ skey === backgroundToKey(bg) }
                               onSelect={ () => onSelect(bg) } />
        )}
    </div>;
}

export class ImageSearchSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: null,
            request: null,
            query: '',
            timeout: null
        }

        this.refSidebarSection = s => this._sidebarSection = s;

        this.doSearch = () => {
            this.stopSearch();
            const r = request.get('/imagesearch/search')
                             .query({ q: this.state.query })
                             .end((err, resp) => {
                if (err) {
                    this.setState({
                        loading: false,
                        error: 'Error loading results (' + err.status + ')',
                        request: null,
                        data: null
                    });
                } else {
                    this.setState({
                        loading: false,
                        error: false,
                        request: null,
                        data: resp.body.data
                    });
                }
            });
            this.setState({ loading: true, timeout: null, request: r });
            this._sidebarSection.expand();
        };
        this.searchQueryChange = query => {
            this.stopSearch();
            if (this.state.timeout)  window.clearTimeout(this.state.timeout);

            if (!query) {
                this.setState({ timeout: null, query });
                this._sidebarSection.contract();
            } else {
                const timeout = window.setTimeout(this.doSearch, 1000);
                this.setState({ query, timeout });
            }
        };

        this.onHeaderClick = (event) => {
            if (event.target.nodeName !== 'INPUT') {
                this._sidebarSection.onHeaderClick();
            }
        };
    }

    stopSearch() {
        if (this.state.request !== null) {
            this.state.request.abort();
            this.setState({ request: null, loading: false });
        }
    }

    render() {
        let content;
        if (this.state.loading) {
            content = <Loader />;
        } else if (this.state.error !== false) {
            content = this.state.error;
        } else if (this.state.data !== null) {
            const SOURCES = [
                { name: 'Presenter Club', home: 'https://www.presenter.club' },
                { name: 'Unsplash',       home: 'https://unsplash.com' },
                { name: 'Splashbase',     home: 'http://www.splashbase.co' },
                { name: 'Flickr',         home: 'https://www.flickr.com' }
            ];
            content = <div>
                <BackgroundPreviews data={ this.state.data }
                                    onSelect={ this.props.onSelect }
                                    selected={ this.props.curr } />
                <div className='Sidebar__center Sidebar__center--text'><div>
                    { 'Search sources include ' }
                    { SOURCES.map(({ name, home }, i) => {
                        let end = ', ';
                        if (i === SOURCES.length - 1)  end = '.';
                        if (i === SOURCES.length - 2)  end = ' & ';
                        return <span>
                            <a className='Sidebar__link'
                               href={ home }
                               target='blank'>
                                { name }
                            </a>
                            { end }
                        </span>
                    })}
                </div></div>
            </div>;
        }
        return <SidebarSection
            ref={ this.refSidebarSection }
            hideArrow={ this.state.data === null && !this.state.loading }
            onHeaderClick={ this.onHeaderClick }
            title={ <SearchBar query={ this.state.query }
                               activate={ this.doSearch }
                               onChange={ this.searchQueryChange } /> }>
            { content }
        </SidebarSection>;
    }
}

class _SidebarBackgrounds extends React.Component {
    render() {
        const { dispatch, styleImages, styleColors, goBackTo } = this.props;
        const row = Object.assign({}, this.props.defaultStyle, this.props.row);

        const onSelect = (background) => {
            dispatch(PresentationActions.editRow(
                row.uuid,
                { background }
            ));
        };

        return <SlideEditorSidebar title='Choose a Background'
                                   goBackTo={ goBackTo }>
            <ImageSearchSection onSelect={ onSelect }
                                selected={ row.background } />

            <SidebarSection initialExpansion
                            title='Style Suggested Backgrounds'>
                <BackgroundPreviews data={ styleImages }
                                    onSelect={ onSelect }
                                    selected={ row.background } />
            </SidebarSection>
            <SidebarSection title='Style Colors'>
                <ColorPalette colors={ styleColors }
                              selected={ colorFromBackground(row.background) }
                              onSelect={ c => onSelect(backgroundFromColor(c)) } />
            </SidebarSection>

            <SidebarSection title='Your Media Library'>
                <MediaLibrary
                    onSelect={ onSelect }
                    selected={ row.background.type === Types.MEDIALIBRARY
                               ? row.background.id : undefined } />
            </SidebarSection>
        </SlideEditorSidebar>;
    }
};

const mapStateToProps = (state) => {
    return {
        row: unwrap(state.presentation).speech.find(
            row => row.uuid === state.editview.selectedSlide
        ),
        styleImages: state.stylecache.data.images || [],
        styleColors: state.stylecache.data.colors,
        defaultStyle: state.stylecache.data.previews.default,
    };
};
export const SidebarBackgrounds = connect(mapStateToProps)(_SidebarBackgrounds);
