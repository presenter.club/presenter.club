/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { connect } from 'react-redux';
import Measure from 'react-measure';

import { unwrap } from '../reducers/presentation.jsx';
import FullScreenView from './FullScreenView.jsx';
import PresentationPlayer from './PresentationPlayer.jsx';
import SpeechView from './SpeechView.jsx';
import { Privacy } from '../constants/metadata.jsx';
import { DjangoStyleCSS } from './style/CSS.jsx';

class _ViewPresentation extends React.Component {
    constructor(props) {
        super(props);
        this.state = { fullscreen: false, slide: 0 };
        this.fullscreen = (event) => this.setState({ fullscreen: true });
        this.unfullscreen = (event) => this.setState({ fullscreen: false });

        if (this.props.loop) {
            setInterval(this.loopTimeout, this.props.loop * 1000);
        }
    }

    static propTypes = {
        /* After loop seconds, we will advance to the next slide and loop
         * if applicable */
        loop: React.PropTypes.number
    }

    loopTimeout = () => {
        const nSlides = this.props.presentation.speech
            .filter(({ slideType }) => slideType !== null)
            .length;
        let next = this.state.slide + 1;
        if (next > nSlides-1)  next = 0;
        this.setState({ slide: next });
    }

    onChangeSlide = slide => this.setState({ slide });

    render() {
        const pc = ({ width, height }) =>
                <PresentationPlayer presentation={ this.props.presentation }
                                    fullscreen={ this.state.fullscreen }
                                    onChangeSlide={ this.onChangeSlide }
                                    slide={ this.state.slide }
                                    presentationStyle={ window.djangosStyle }
                                    { ...{ width, height } } />;
        const presentation = this.state.fullscreen ?
            <FullScreenView onClose={ this.unfullscreen }>{ pc }</FullScreenView>
            : <Measure>{ pc }</Measure>;

        let speech = <noscript />;
        if (this.props.presentation.metadata.privacy === Privacy.PUBLIC
            || window.djangosViewingMyself) {
            let why = <p>Your private notes:</p>;
            if (this.props.presentation.metadata.privacy === Privacy.PUBLIC) {
                why = <p>{ window.djangosViewingProfile.name } has chosen to
                         make their notes public:</p>;
            }
            speech = [
                <hr />,
                why,
                <div className='notes-container'>
                    <SpeechView presentationStyle={ window.djangosStyle }
                                editable={ false } />
                </div>
            ]
        }

        return <div>
            <DjangoStyleCSS />
            { presentation }
            <section className='wrapper'><div className='inner'>
                <div className='title-n-button'>
                    <h1>{ this.props.presentation.metadata.title }</h1>
                    { window.djangosViewingMyself ?
                        <a href='edit' className='button'>Edit</a>
                        : null
                    }
                    { this.props.loop ?
                        <a href='?' className='button'>Stop Looping</a>
                        : null
                    }
                    <button onClick={ this.fullscreen }>Fullscreen</button>
                </div>
                <p>By <a href='..'>{ window.djangosViewingProfile.name }</a></p>

                { speech }
            </div></section>
        </div>;
    }
};
const ViewPresentation = connect((state) => ({
    presentation: unwrap(state.presentation)
}))(_ViewPresentation);
export default ViewPresentation;
