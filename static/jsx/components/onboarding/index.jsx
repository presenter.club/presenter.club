/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import request from 'superagent';
import { connect } from 'react-redux';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import Icons from '../../constants/icons.jsx';
import ToolbarButton from '../ToolbarButton.jsx';
import { unwrap } from '../../reducers/presentation.jsx';

const CompleteReason = {
    NORMAL: undefined,
    SKIPPED: 'skipped'
};

class Tip extends React.Component {
    componentDidMount() {
        const { highlight } = this.props;
        if (highlight !== undefined) {
            const el = document.querySelector(highlight);
            if (el)  el.classList.add('OnboardingHighlight');
        }
    }

    componentWillUnmount() {
        const { highlight } = this.props;
        if (highlight !== undefined) {
            const el = document.querySelector(highlight);
            if (el)  el.classList.remove('OnboardingHighlight');
        }
    }

    render() {
        const { children, onComplete } = this.props;

        return <div className='OnboardingTip'>
            <div className='OnboardingTip__body'>
                { children }
            </div>
            <div onClick={ () => onComplete(CompleteReason.SKIPPED) }
                 className='OnboardingTip__skip clickable'>Skip</div>
        </div>;
    }
};

const PressButtonTip = ({ children, onComplete }) => {
    return <div className='OnboardingTip'>
        <div className='OnboardingTip__body'>
            { children }
        </div>
        <button
            onClick={ () => onComplete(CompleteReason.NORMAL) }
            className='OnboardingTip__button special'>
            Ok
        </button>
    </div>;
};

const RTFMTip = (props) => {
    return <PressButtonTip { ...props }>
        That's all for the tips.  If you have feedback, have found a bug
        or need help, just send us a message by pressing the circular
        button on the right.
    </PressButtonTip>
};

const _SpeechViewTip = ({ ifAnyRowMatches = row => true,
                          ifAtLeastNRows = 1,
                          ifEditViewMatches = editview => true,
                          speech, editview,
                          ...props }) => {
    let finished = false;
    if (speech.length >= ifAtLeastNRows
        && ifEditViewMatches(editview)) {
        for (let i = 0; i < speech.length; i++) {
            if (ifAnyRowMatches(speech[i])) {
                finished = true;
                break;
            }
        }
    }
    if (finished) {
        setTimeout(() => props.onComplete(CompleteReason.NORMAL), 0);
    }
    return <Tip { ...props } />;
};
const SpeechViewTip = connect(
    state => ({
        speech: unwrap(state.presentation).speech,
        editview: state.editview
    })
)(_SpeechViewTip);

const SPEECH_VIEW_TIPS = [
    {
        name: 'type-first-row',
        comp: props => (
            <SpeechViewTip { ...props }
                           highlight='.SpeechRow__cell--idea:not(.SpeechRow--header__cell) .ql-editor p'
                           ifAnyRowMatches={ row => row.idea.length > 0 }>
                Use the dot points to outline your presentation.  Let's put
                something like "introduction" in the first row of the idea
                column.
            </SpeechViewTip>
        )
    },
    {
        name: 'type-2nd-row',
        comp: props => (
            <SpeechViewTip { ...props } ifAtLeastNRows={ 2 }>
                Great start.  Press enter to add another dot point, just like
                in a word processor.
            </SpeechViewTip>
        )
    },
    {
        name: 'indent-a-dot-point',
        comp: props => (
            <SpeechViewTip { ...props }
                           ifAnyRowMatches={ row => row.indent > 0 }>
                Press tab to indent a dot point.  Use indentation to add
                more detailed points.
            </SpeechViewTip>
        )
    },
    {
        name: 'use-elaboration',
        comp: props => (
            <SpeechViewTip { ...props }
                           highlight='.SpeechRow__cell--elaboration'
                           ifAnyRowMatches={ row => !!row.execution }>
                Once you've planned out your speech (using the idea column),
                use the elaboration column to attach extra information.
                Type some statistics, quotes or the words you plan to say
                in the elaboration column.
            </SpeechViewTip>
        )
    },
    {
        name: 'add-a-slide',
        comp: props => (
            <SpeechViewTip { ...props }
                           highlight='.add-slide'
                           ifAnyRowMatches={ row => row.slideType !== null  }>
                After you've written your speech, you can start adding slides!
                Click the add button in the slide row to add a slide to one
                of your important ideas.
            </SpeechViewTip>
        )
    },
    {
        name: 'open-the-overlay',
        comp: props => (
            <SpeechViewTip { ...props }
                           highlight='.TitleBar'
                           ifEditViewMatches={ ev => ev.overlay }>
                Open the overlay to change your presentation's title,
                privacy settings and style.
            </SpeechViewTip>
        )
    },
];
const SLIDE_EDITOR_VIEW_TIPS = [
    {
        name: 'choose-a-layout',
        comp: props => (
            <SpeechViewTip { ...props }
                           highlight='.SlideThumb.slide-template:not(.checked)'
                           ifAnyRowMatches={ row => row.slideType !== null
                                                 && row.slideType !== 'none' }>
                Choose a layout from the left sidebar.  The layout defines
                what "shape" the content has.  Think of it as a very blury
                version of your slide.  Click around to try different layouts
                - you won't loose your content.
            </SpeechViewTip>
        )
    },
    {
        name: 'select-a-block',
        comp: props => (
            <SpeechViewTip { ...props }
                           ifEditViewMatches={ ev => ev.focusedBlock !== undefined  }>
                Click on the content in the slide to edit it.  You will also
                see the sidebar change for editing
                the specific type of content.
            </SpeechViewTip>
        )
    },
    {
        name: 'change-block-type',
        comp: props => (
            <SpeechViewTip { ...props }
                           highlight='.BlockToolbar__change_type'
                           ifAnyRowMatches={ row => (row.blocks || []).some(
    block => [undefined, 'heading', 'empty'].indexOf(block.type) === -1
                                             )}>
                Click the "Change Type" button to select a different type for
                the block.
            </SpeechViewTip>
        )
    },
    {
        name: 'deselect-a-block',
        comp: props => (
            <SpeechViewTip { ...props }
                           highlight='.BlockToolbar__done'
                           ifEditViewMatches={ ev => ev.focusedBlock === undefined  }>
                Click "DONE" to go back to the main slide view so that
                we can change the background.
            </SpeechViewTip>
        )
    },
    {
        name: 'change-the-background',
        comp: props => (
            <SpeechViewTip { ...props }
                           highlight='.SidebarButton--bg'
                           ifAnyRowMatches={ row => row.background.color !== 'black' }>
                Give your slide impact with a background.  Click the background
                icon in the top left of the slide, then click a background
                you want to use.  Use the search box to pick from
                presentation-ready, curated images.
            </SpeechViewTip>
        )
    },
];

const Modal = ({ title, children, cta, onClose }) => {
    const onClick = (event) => {
        onClose();
    };
    return <div className='Modal__background'
                onClick={ onClick }>
        <div className='Modal'>
            <div className='Modal__header toolbar'>
                <h3 className='Modal__title'>{ title }</h3>
            </div>
            <div className='Modal__contents'>
                { children }
            </div>
            <div className='Modal__actions'>
                <button className='special' onClick={ onClick }>
                    { cta }
                </button>
            </div>
        </div>
    </div>;
};

const Welcome = ({ onComplete }) => {
    return <Modal title='Welcome to Presenter Club'
                  cta="Let's Go!"
                  onClose={ onComplete }>
        <p>Welcome to Presenter Club!  We've got some tips to help you
           get started, just look at the bottom of the screen to view
           them.</p>
    </Modal>
};

class _PresentationEditorOnboarding extends React.Component {
    constructor () {
        super();
        this.state = { completed: [] };
        this.nTips = SLIDE_EDITOR_VIEW_TIPS.length
                     + SPEECH_VIEW_TIPS.length
                     + 2;  // Welcome and RTFM
    }

    hasNotCompleted(tip) {
        if (window.djangoOnboardStatus === 1) {
            return false;
        } else {
            return this.state.completed.indexOf(tip) === -1;
        }
    }

    onAllCompleted() {
        request.post(`/u/${ window.djangosUser.username }/onboarding-status`)
               .withCredentials()
               .send({ status: 1 })
               .end((err, resp) => {
            if (err) {
                console.error('Onboarding status', err, resp);
            };
        });
    }

    onComplete(name) {
        return (reason) => {
            const completed = this.state.completed.concat([name])
            this.setState({ completed });

            if (completed.length === this.nTips) {
                this.onAllCompleted();
            }
        }
    }

    render() {
        let content = null;

        if (this.hasNotCompleted('welcome')) {
            content = <Welcome onComplete={ this.onComplete('welcome') }
                               key='welcome' />;
        } else {
            const inSlide = this.props.selectedSlide;
            const tipsList = inSlide ?
                SLIDE_EDITOR_VIEW_TIPS : SPEECH_VIEW_TIPS;
            for (let i = 0; i < tipsList.length; i++) {
                const tip = tipsList[i];
                const name = tip.name;
                if (this.hasNotCompleted(name)) {
                    const Comp = tip.comp;
                    content = <Comp onComplete={ this.onComplete(name) }
                                    key={ name } />;
                    break;
                }
            };
        }

        if (content === null && this.hasNotCompleted('rtfm')) {
            content = <RTFMTip onComplete={ this.onComplete('rtfm') }
                               key={ 'rtfm' } />;
        }

        return <ReactCSSTransitionGroup
            transitionName='OnboardingTransition'
            transitionEnterTimeout={ 250 }
            transitionLeaveTimeout={ 250 }>
            { content }
        </ReactCSSTransitionGroup>;
    }
};
export const PresentationEditorOnboarding = connect(state => ({
    selectedSlide: state.editview.selectedSlide,
}))(_PresentationEditorOnboarding);
