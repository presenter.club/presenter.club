/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { connect } from 'react-redux';
import Measure from 'react-measure';

import * as PresentationActions from '../actions/presentation.jsx';
import * as EditViewActions from '../actions/editview.jsx';
import { SlideInner } from './SlideInner.jsx';
import { StaticSlideView } from './SlideView.jsx';
import { SlideBlockSidebar, blockTypeHasSidebar } from './blocks/index.jsx';
import ToolbarButton from './ToolbarButton.jsx';
import ToolbarSpacer from './ToolbarSpacer.jsx';
import Icons from '../constants/icons.jsx';

import { SidebarTemplates } from './SidebarTemplates.jsx';
import { SidebarBackgrounds } from './SidebarBackgrounds.jsx';
import { SidebarForeground } from './SidebarForeground.jsx';
import SidebarChangeBlockType from './SidebarChangeBlockType.jsx';

const SidebarButton = ({ type, currSidebar, selectSidebar, title, icon,
                         first }) => {
    const active = type === currSidebar;

    return <ToolbarButton
        title={ title }
        onClick={ () => {
            if (active) {
                selectSidebar(null);
            } else {
                selectSidebar(type);
            }
        }}
        className={ 'fontello' + (first ? ' first' : '')
                    + (active ? ' special' : '')
                    + ' SidebarButton--'+type }>
            { icon }
    </ToolbarButton>;
}

const SidebarTypes = {
    BG: 'bg',
    FG: 'fg'
};

const MainSlideToolbar = ({ sidebar, dispatch, selectSidebar, row }) => {
    return <div className='toolbar toolbar--slide SlideEditor__toolbar SlideEditor__toolbar--main'>
        <SidebarButton title='Change Background'
                       first={ true }
                       selectSidebar={ selectSidebar }
                       currSidebar={ sidebar }
                       type={ SidebarTypes.BG }
                       icon={ Icons.BACKGROUND } />
        <SidebarButton title='Change Foreground Colors'
                       selectSidebar={ selectSidebar }
                       currSidebar={ sidebar }
                       type={ SidebarTypes.FG }
                       icon={ Icons.FOREGROUND } />
        <ToolbarSpacer />
        <ToolbarButton title='Delete Slide'
                       onClick={ () => {
                           dispatch(PresentationActions.deleteSlide(row.uuid));
                       }}
                       className='fontello'>
            { Icons.DELETE }
        </ToolbarButton>
        <ToolbarButton title='Save and Close'
                       onClick={ () => {
                           dispatch(EditViewActions.closeSlideView());
                       }}
                       className='fontello'>
            { Icons.SAVE }
        </ToolbarButton>
    </div>;
};

const BlockToolbar = ({ focusedBlock, hasSidebar, dispatch,
                        toggleBTC, row, blockTypeChooser }) => {
    let leftButton = <noscript />;
    if (hasSidebar) {
        leftButton = <ToolbarButton
                onClick={ (event) => {
                    toggleBTC();
                    event.stopPropagation();
                }}
                className={ 'small BlockToolbar__change_type'
                            + (blockTypeChooser ? ' special' : '') }>
           Change Type 
        </ToolbarButton>;
    }
    return <div className='toolbar toolbar--slide toolbar--edit SlideEditor__toolbar SlideEditor__toolbar--overlay'>
        { leftButton }
        <ToolbarSpacer>Editing block { focusedBlock + 1 }</ToolbarSpacer>
        <ToolbarButton onClick={ (event) => {
                    dispatch(EditViewActions.focusBlock(undefined));
                    event.stopPropagation();
                }}
                className='right small BlockToolbar__done'>
            Done
        </ToolbarButton>
    </div>;
};

/* http://stackoverflow.com/a/16136789 */
function easeInOut(currentTime, start, change, duration) {
    currentTime = currentTime / (duration / 2);
    if (currentTime < 1) {
        return change / 2 * currentTime * currentTime + start;
    }
    currentTime -= 1;
    return -change / 2 * (currentTime * (currentTime - 2) - 1) + start;
};
function animateScrollTo(to, duration = 125) {
    const start = window.scrollY,
          change = to - start,
          increment = 20;

    const inner = elapsedTime => {
        const position = easeInOut(elapsedTime, start, change, duration);
        window.scrollTo(0, position);
        if (elapsedTime + increment < duration) {
            setTimeout(() => inner(elapsedTime + increment), increment);
        }
    };
    inner(0);
    // Make sure we get the end
    setTimeout(() => inner(duration), duration);
};

class _SlideEditor extends React.Component {
    constructor(props) {
        super(props);

        this.onChange = change => {
            this.props.dispatch(PresentationActions.editRow(
                this.props.row.uuid,
                change
            ));
        };
        this.onBlockChange = (i, change) => {
            this.props.dispatch(PresentationActions.editBlock(
                this.props.row.uuid,
                i,
                change
            ));
        };
        this.onBlockChangeType = (i, newType) => {
            this.props.dispatch(PresentationActions.changeBlockType(
                this.props.row.uuid,
                i,
                newType
            ));
            this.props.dispatch(EditViewActions.setBlockTypeChooser(false));
        }
        this.requestFocus = (i) => {
            this.setState({ sidebar: null });
            this.props.dispatch(EditViewActions.focusBlock(i));
        }
        this.requestFocusNothing = (event) => {
            if (event.target.classList.contains('toolbar__child') ||
                event.target.classList.contains('toolbar__spacer')) {
                return;
            }
            this.props.dispatch(EditViewActions.focusBlock(undefined));
        }

        this.state = { sidebar: null };
        this.selectSidebar = (sidebar) => {
            this.setState({ sidebar });
            if (this.props.focusedBlock !== undefined) {
                this.props.dispatch(EditViewActions.focusBlock(undefined));
            }
        }
        this.goBackToTemplates = {
            title: 'Choose a Layout',
            onClick: this.selectSidebar.bind(this, null)
        };

        this.toggleBTC = () =>
            this.props.dispatch(EditViewActions.toggleBlockTypeChooser());
        this.goBackToBTC = {
            title: 'Choose Block Type',
            onClick: this.toggleBTC
        };
    }

    componentDidMount() {
        if (this.props.scrollTo) {
            const el = document.getElementById(
                'SpeechRow--' + this.props.row.uuid);
            // Magic number :(
            const top = el.getBoundingClientRect().top + window.scrollY - 70;
            setTimeout(() => animateScrollTo(top), 0);
        }
    }

    render() {
        const { blockTypeChooser, focusedBlock, row, dispatch } = this.props;

        let overlayToolbar = <noscript />, sidebarEl, cn = 'SlideEditor';
        if (focusedBlock === undefined) {
            if (this.state.sidebar === null) {
                sidebarEl = <SidebarTemplates { ...{ row, dispatch } } />;
            } else if (this.state.sidebar === SidebarTypes.BG) {
                sidebarEl = <SidebarBackgrounds
                                 goBackTo={ this.goBackToTemplates } />;
            } else if (this.state.sidebar === SidebarTypes.FG) {
                sidebarEl = <SidebarForeground
                                 goBackTo={ this.goBackToTemplates } />;
            }
        } else {
            cn = cn + ' SlideEditor--edit-block SlideEditor--overlayed-toolbar';
            const hasSidebar = blockTypeHasSidebar(row.blocks[focusedBlock].type);
            overlayToolbar = <BlockToolbar
                toggleBTC={ this.toggleBTC }
                { ...{ row, dispatch, focusedBlock, blockTypeChooser, hasSidebar} } />;

            const sprops = {
                state: row.blocks[focusedBlock],
                blockId: focusedBlock
            };
            if (blockTypeChooser || !hasSidebar) {
                sprops.onChange = t => this.onBlockChangeType(focusedBlock, t);
                sidebarEl = <SidebarChangeBlockType { ...sprops }
                                 goBackTo={ this.goBackToTemplates } />;
            } else {
                sprops.onChange = ch => this.onBlockChange(focusedBlock, ch);
                sidebarEl = <SlideBlockSidebar { ...sprops }
                                 goBackTo={ this.goBackToBTC } />;
            }
        }

       return <div className='SlideEditor__row'>
            { sidebarEl }
            <StaticSlideView className='SlideEditor__background'
                             presentationStyle={ this.props.presentationStyle }
                             row={ row }>
            <div className={ cn } onClick={ this.requestFocusNothing }>
                <MainSlideToolbar
                    selectSidebar={ this.selectSidebar }
                    sidebar={ this.state.sidebar }
                    { ...{ row, dispatch } } />
                { overlayToolbar }
                <Measure>{ dimensions => {
                    const scaleHeight = (dimensions.height - 80) / 360;
                    const scaleWidth = dimensions.width / 640;
                    const scale = Math.min(scaleHeight, scaleWidth);

                    return <div className='SlideEditor__SlideInner slide-parent'>
                        <SlideInner onBlockChange={ this.onBlockChange }
                                    onChange={ this.onChange }
                                    requestFocus={ this.requestFocus }
                                    row={ row }
                                    editable={ true }
                                    scale={ scale }
                                    lightsOutFor={ focusedBlock }
                                    presentationStyle={ this.props.presentationStyle }
                                    style={{ width: scale * 640,
                                             height: scale * 360 }} />
                    </div>;
                }}</Measure>
            </div></StaticSlideView>
        </div>;
    }
}

const mapStateToProps = (state) => {
  return {
    focusedBlock: state.editview.focusedBlock,
    blockTypeChooser: state.editview.blockTypeChooser,
    presentationStyle: state.stylecache.data
  }
}
export const SlideEditor = connect(mapStateToProps)(_SlideEditor);
