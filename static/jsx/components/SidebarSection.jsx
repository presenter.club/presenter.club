/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';

class SidebarSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = { expanded: this.props.initialExpansion || false };
        this.onHeaderClick = () => {
            this.setState({ expanded: !this.state.expanded });
        }
    }

    expand() {
        if (this.state.expanded !== true) {
            this.setState({ expanded: true });
        }
    }

    contract() {
        if (this.state.expanded !== false) {
            this.setState({ expanded: false });
        }
    }

    render() {
        const { children, title, onHeaderClick, hideArrow } = this.props;

        let bn = n => `SidebarSection${ n }`
        if (this.state.expanded) {
            bn = n => `SidebarSection${ n } SidebarSection--expanded${ n }`;
        }

        let titleEl = title;
        if (typeof title === 'string') {
            titleEl = <div className={ bn('__title') }>{ title }</div>;
        }

        return <div className={ bn('') }>
            <div className={ bn('__header') + ' clickable '
                             + (hideArrow ? bn('__header--hide-arrow') : '') }
                 onClick={ onHeaderClick || this.onHeaderClick }>
                { titleEl }
            </div>
            <div className={ bn('__children') }>
                { children }
            </div>
        </div>
    }

};

export default SidebarSection;
