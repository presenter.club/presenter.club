/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';

import { SLIDE_TYPES, SplitType } from '../constants/slidetypes.jsx';
import SlideTemplateBlock from './SlideTemplateBlock.jsx';
import ZigZagBlock from './ZigZagBlock.jsx';

var repeat = function (nTimes, string) {
    var result = '';
    for (var i = 0; i < nTimes; i++) {
        result = result + string;
    }
    return result;
};

const TemplateTextBox = ({ tagName = 'h1', ch = '█' }) => {
    return React.createElement(
        tagName,
        {},
        repeat(10, ch)
    );
};

export const SlideTemplateThumb = ({ typeName, onClick, selected }) => {
    const type = SLIDE_TYPES[typeName];
    const blocks = (type.suggestedBlocks || []).map((b, i) => {
        if (b === 'h1') {
            return <TemplateTextBox tagName='h1' ch='█' />;
        } else if (b === 'h2') {
            return <TemplateTextBox tagName='h2' ch='■' />;
        } else {
            return <ZigZagBlock number={ i } />;
        }
    });

    return <div className={ 'slide-parent SlideThumb slide-template '
                            + (selected ? 'checked ' : '') }
                title={ type.tooltip }
                onClick={ onClick }>
        <div className={ 'slide clickable ' + type.className }
             style={{ color: 'white' }}>
            <SlideTemplateBlock blocks={ blocks } root={ type.root }
                                scale={ 0.3 }
                                onChange={ null } />
        </div>
    </div>;
};

export const AllSlideTemplateThumbs = ({ selected, onSelect }) => {
    return <div className='slide-templates'>
        { Object.keys(SLIDE_TYPES).map((typeName) =>
            <SlideTemplateThumb
                onClick={ () => onSelect(typeName) }
                selected={ typeName === selected }
                typeName={ typeName }
                key={ typeName } />
        )}
    </div>;
}
