/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { PropTypes } from 'react';
import Quill from 'quill';

function handleUndo() {
    window.handleUndo();
}
function handleRedo() {
    window.handleRedo();
}

class QuillNoToolbar extends React.Component {
    constructor(props) {
        super(props);
        this.onTextChange = (delta, source) => {
            if (this.getHTML !== this.props.value) {
                this.props.onChange(this.getHTML());
            }
        };
        this.onSelectionChange = (range, oldRange, source) => {
            if (range === null) {
                if (this.props.onDeselect !== undefined) {
                    this.props.onDeselect();
                }
            } else {
                if (this.props.onSelect !== undefined) {
                    this.props.onSelect();
                }
            }
        };
        this.state = { editor: undefined, cbs: [] };
    }

    render() {
        return <div className={ 'quill-contents ' + this.props.className }
                    style={ this.props.style }
                    onClick={ this.props.onClick }
                    dangerouslySetInnerHTML={{ __html: this.props.value }}
                    ref='editor' />
    }

    componentWillReceiveProps(newProps) {
        this.withEditor((editor) => {
            if (newProps.value !== this.getHTML() && newProps.value !== undefined) {
                const s = editor.getSelection();
                editor.off('text-change', this.onTextChange);
                editor.off('selection-change', this.onSelectionChange);

                editor.pasteHTML(newProps.value);

                editor.on('text-change', this.onTextChange);
                editor.on('selection-change', this.onSelectionChange);
                if (s !== null)  editor.setSelection(s);
            }
        });
    }

    getHTML() {
        return this.state.editor.root.innerHTML;
    }

    focus() {
        this.withEditor((editor) => {
            editor.focus();
        });
    }

    withEditor(callback) {
        if (!this.state.editor) {
            // FIXME: HACK
            this.setState({ cbs: this.state.cbs.concat([callback]) });
        } else {
            callback(this.state.editor);
        }
    }

    getQuill() {
        return this.state.editor;
    }

    componentDidMount() {
        // We never want to use Quill's history, because we use redux undo
        const bindings = Object.assign({
            undo: { key: 'Z', shortKey: true,
                    handler: handleUndo },
            redo: { key: 'Z', shortKey: true, shiftKey: true,
                    handler: handleRedo },
            win32redo: { key: 'Y', shortKey: true, handler: handleRedo }
        }, this.props.bindings);
        const config = {
            placeholder: this.props.placeholder,
            readOnly: !this.props.editable,
            modules: {
                keyboard: { bindings },
                history: {
                    // Quill can't disable history, but let's save ram
                    // https://github.com/quilljs/quill/issues/691
                    maxStack: 1
                }
            }
        };

        if (this.props.noBlockFormats) {
            // From http://quilljs.com/docs/formats/
            config.formats = [
                'background',
                'bold',
                'color',
                'font',
                'code',
                'italic',
                'link',
                'size',
                'strike',
                'script',
                'underline'
            ];
        }

        const editor = new Quill(this.refs.editor, config);
        editor.on('text-change', this.onTextChange);
        editor.on('selection-change', this.onSelectionChange);

        this.state.cbs.forEach(c => c(editor));
        this.setState({ editor, cbs: []  })
    }

    componentWillUnmount() {
        // TODO: find the new way to do this
        //this.destroyEditor(this.state.editor);
    }

    shouldComponentUpdate(nextProps, nextState) {
        const ed = nextState.editor;
        if (nextState.cbs.length > 0 && ed) {
            setTimeout(() => {
                // Not fully mounted until we set the timeout
                nextState.cbs.forEach(c => c(ed));
                this.setState({ cbs: [] });
            });
        }
        // Never re-render or we loose the Quill state
        return false;
    }
};
QuillNoToolbar.defaultProps = {
    editable: true
};
export default QuillNoToolbar;
