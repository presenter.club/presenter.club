/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import SlideEditorSidebar from './SlideEditorSidebar.jsx';

const Option = ({ state, value, title, onChange }) => {
    return <img className={ 'BackgroundPreviews__item SlideThumb slide-template '
                            + (state.type === value ? 'checked ' : '') }
                onClick={ onChange.bind(this, value) }
                title={ title }
                alt={ title }
                src={ `/static/images/blocktype/${ value }.svg` } />;
};

const SidebarChangeBlockType = ({ state, goBackTo, onChange }) => {
    return <SlideEditorSidebar title='Change Block Type'
                               goBackTo={ goBackTo }>
        <div className='BackgroundPreviews'>
            <Option state={ state } onChange={ onChange }
                    value='heading'
                    title='Heading' />
            <Option state={ state } onChange={ onChange }
                    value='text'
                    title='Text' />
            <Option state={ state } onChange={ onChange }
                    value='image'
                    title='Image' />
            <Option state={ state } onChange={ onChange }
                    value='vidme'
                    title='VidMe (Video Upload)' />
            <Option state={ state } onChange={ onChange }
                    value='youtube'
                    title='YouTube Video' />
            <Option state={ state } onChange={ onChange }
                    value='quote'
                    title='Quote' />
        </div>
    </SlideEditorSidebar>;
};
export default SidebarChangeBlockType;
