/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import QuillNoToolbar from '../QuillNoToolbar.jsx';
import { BaseSidebar } from './helpers.jsx';
import SidebarSection from '../SidebarSection.jsx';
import { toCSS as textEffectToCSS } from '../../constants/textEffect.jsx';

const ToggleButton = ({ children, onToggle, active, className }) => {
    return <button onClick={ () => onToggle(!active) }
            className={ (active ? 'special ' : '') + className }>
        { children }
    </button>;
};

const block = ({ state, blockId, onChange, scale,
                 presentationStyle, row,
                 requestFocus, editable }) => {
    const style = Object.assign(
        { fontSize: ((state.size || 1) * scale) + 'em' },
        textEffectToCSS(presentationStyle.normal, row, scale)
    );

    if (editable) {
        return <div style={ style }>
            <QuillNoToolbar
                className='ql-editor-no-padding'
                value={ state.html }
                placeholder='Type your idea'
                onClick={ requestFocus }
                ref={ (ref) => {
                    if (window._textBlockRefs === undefined) {
                        window._textBlockRefs = {};
                    }
                    if (ref !== null)  window._textBlockRefs[blockId] = ref;
                }}
                onSelect={ requestFocus }
                onChange={ (html) => onChange({ html }) } />
        </div>;
    } else {
        return <div onClick={ requestFocus }
                    style={ style }
                    dangerouslySetInnerHTML={ { __html: state.html } } />
    }
};

class sidebar extends React.Component  {
    componentDidMount() {
        window.setTimeout(() => {
            // FIXME
            const comp = window._textBlockRefs[this.props.blockId];
            comp.withEditor((quill) => {
                this.quill = quill;
                this.quill.on('selection-change', this.onSelectionChange.bind(this));
                this.quill.on('text-change', this.onSelectionChange.bind(this));
                this.onSelectionChange();
            });
        }, 10);
    }

    componentWillUnmount() {
        this.quill.off('selection-change', this.onSelectionChange.bind(this));
        this.quill.off('text-change', this.onSelectionChange.bind(this));
    }

    onSelectionChange() {
        try {
            const f = this.quill.getFormat();
            this.setState({
                bold: f.bold || false,
                italic: f.italic || false,
                underline: f.underline || false,
                list: f.list
            });
        } catch (e) {
            console.log('Quill sidebar error', e);

        }
    }

    constructor(props) {
        super(props);
        this.state = {
            bold: false,
            italic: false,
            underline: false,
            list: undefined
        };
    }

    render() {
        const { state, onChange } = this.props;
        const size = state.size || 1;

        const toggleFormat = (format) => {
            return (value) => {
                this.quill.format(format, value);
            }
        };

        const toggleFormatValue = (format, value) => {
            return (btnValue) => {
                if (btnValue)  this.quill.format(format, value);
                else  this.quill.format(format, false);
            }
        };

        return <BaseSidebar title='Text Block Style'
                        goBackTo={ this.props.goBackTo }>
            <SidebarSection initialExpansion title='Font Size'>
                <button onClick={ () => {
                    onChange({ size: Math.max(0.4, size - 0.3) });
                        }}
                        className='join-leftmost'>Smaller</button>
                <button onClick={ () => {
                    onChange({ size: size + 0.3 });
                        }}
                        className='join-rightmost'>Larger</button>
                <br />
                <button onClick={ () => {
                    onChange({ size: 1 });
                        }}>Reset</button>
            </SidebarSection>

            <SidebarSection initialExpansion title='Inline Formatting'>
                <ToggleButton onToggle={ toggleFormat('bold') }
                              className='join-leftmost'
                              active={ this.state.bold }>
                    <b>B</b>
                </ToggleButton>
                <ToggleButton onToggle={ toggleFormat('italic') }
                              className='join'
                              active={ this.state.italic }>
                    <em>I</em>
                </ToggleButton>
                <ToggleButton onToggle={ toggleFormat('underline') }
                              className='join-rightmost'
                              active={ this.state.underline }>
                    <u>U</u>
                </ToggleButton>
            </SidebarSection>

            <SidebarSection initialExpansion title='Text Formtting'>
                <ToggleButton onToggle={ toggleFormatValue('list', 'bullet') }
                              className='fontello join-leftmost'
                              active={ this.state.list === 'bullet' }>
                    
                </ToggleButton>
                <ToggleButton onToggle={ toggleFormatValue('list', 'ordered') }
                              className='fontello join-rightmost'
                              active={ this.state.list === 'ordered' }>
                    
                </ToggleButton>
            </SidebarSection>
        </BaseSidebar>;
    }
};


export default { block, sidebar };
