/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import SlideEditorSidebar from '../SlideEditorSidebar.jsx';

export const BaseSidebar = ({ title, children, goBackTo }) => {
    return <SlideEditorSidebar title={ title } goBackTo={ goBackTo }>
        { children }
    </SlideEditorSidebar>;
};

export class CoveredIFrame extends React.Component {
    constructor(props) {
        super(props);

        this.refIframe = iframe => {
            this.iframe = iframe;
            if (this.props.message !== undefined && this.iframe) {
                this.iframe.contentWindow.postMessage(this.props.message, '*');
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.message !== this.props.message
            && nextProps.message !== undefined
            && this.iframe) {
            this.iframe.contentWindow.postMessage(nextProps.message, '*');
        }
    }

    render() {
        const { requestFocus, focused, src } = this.props;
        const covered = focused ? false : !!requestFocus;
        return <div className='IframeBlock'>
            { !covered?
              null
              : <div className='IframeBlock__cover' onClick={ requestFocus } />
            }
            <iframe
                ref={ this.refIframe }
                className={ 'IframeBlock__content'
                            + (!covered? '' : ' IframeBlock__content--covered' ) }
                src={ src }
                onClick={ requestFocus }
                frameBorder='0'
                allowFullScreen />
        </div>;
    }
}

export const SmartIFrame = (props) => {
    const { image, requestFocus, scale } = props;
    if (scale <= 0.5) {
        return <img
            src={ image }
            onClick={ requestFocus } />;
    } else {
        return <CoveredIFrame { ...props } />;
    }
};

export const EmptyImage = ({ requestFocus }) => {
    return <img onClick={ requestFocus }
                style={{ maxWidth: '100%',
                         maxHeight: '100%' }}
                src={ '/static/images/emptythumb.svg' } />;
}
