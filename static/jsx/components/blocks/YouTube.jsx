/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { EmptyImage, BaseSidebar, SmartIFrame } from './helpers.jsx';
import SidebarSection from '../SidebarSection.jsx';
import VisibilityStatus from '../../constants/visibilityStatus.jsx';

const Error = ({ children, onClick }) => {
    return <p { ...{ onClick } }><b>{ children }</b></p>;
}
class block extends React.Component {
    render() {
        const { state, requestFocus, scale, focused } = this.props;
        if (!state.youtubeURL)  return <EmptyImage { ...this.props } />;

        // https://regex101.com/r/uF0yV6/1
        // http://stackoverflow.com/a/27728417
        const re = /^.*(?:(?:youtu\.be\/|v\/|vi\/|u\/\w\/|embed\/)|(?:(?:watch)?\?v(?:i)?=|\&v(?:i)?=))([^#\&\?]*).*/;
        const match = state.youtubeURL.match(re);
        if (match && match[1].length) {
            const message = {
                [ VisibilityStatus.OUT_OF_VIEW ]:
                    '{"event":"command","func":"pauseVideo","args":""}',
                [ VisibilityStatus.IN_VIEW ]:
                    '{"event":"command","func":"playVideo","args":""}'
            }[this.props.visibilityStatus];
            return <SmartIFrame
                src={ 'https://www.youtube.com/embed/' + match[1] + '?enablejsapi=1' }
                image={ `http://img.youtube.com/vi/${ match[1] }/mqdefault.jpg` }
                message={ message }
                { ...this.props } />;
        } else {
            return <Error onClick={ requestFocus }>Invalid URL</Error>;
        }
    }
};

const sidebar = ({ state, onChange, goBackTo }) => {
    return <BaseSidebar title='Edit YouTube Block'
                        goBackTo={ goBackTo }>
        <b>Video URL</b>
        <input type='text'
               value={ state.youtubeURL || '' }
               placeholder='e.g. https://www.youtube.com/watch?v=O4xbKEkmqLg'
               onChange={ (e) => onChange({ youtubeURL: e.target.value }) } />
    </BaseSidebar>;
};

export default { block, sidebar };
