/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import QuillNoToolbar from '../QuillNoToolbar.jsx';
import { BaseSidebar } from './helpers.jsx';
import SidebarSection from '../SidebarSection.jsx';

const block = ({ state, onChange, scale, requestFocus, editable }) => {
    const style = { fontSize: ((state.size || 1) * scale) + 'em' };
    let content;
    if (editable) {
        content = <blockquote>
            <QuillNoToolbar
                className='ql-editor-no-padding'
                value={ state.html }
                placeholder='Type your quote'
                onClick={ requestFocus }
                onSelect={ requestFocus }
                onChange={ (html) => onChange({ html }) } />
        </blockquote>;
    } else {
        content = <blockquote
            dangerouslySetInnerHTML={ { __html: state.html } } />;
    }
    return <div onClick={ requestFocus }
                style={ style }>
        { content }
        { state.attr === undefined ?
            null
            : <div className='Quote__attribution'>— { state.attr }</div>
        }
    </div>;
};

const sidebar = ({ state, onChange, goBackTo }) => {
    return <BaseSidebar title='Change Quote Style'
                        goBackTo={ goBackTo }>
        <SidebarSection initialExpansion title='Quote Attribution'>
            <input
                type='text'
                value={ state.attr || '' }
                onChange={ (e) => onChange({ attr: e.target.value }) } />
        </SidebarSection>
    </BaseSidebar>;
};


export default { block, sidebar };
