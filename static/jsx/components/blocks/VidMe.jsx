/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { EmptyImage, BaseSidebar, SmartIFrame } from './helpers.jsx';
import SidebarSection from '../SidebarSection.jsx';
import request from 'superagent';
import { StyledDropzone } from '../MediaLibrary.jsx';
import LoaderProgress from '../LoaderProgress.jsx';

const Error = ({ children, onClick }) => {
    return <p { ...{ onClick } }><b>{ children }</b></p>;
}
const PoweredBy = ({ href, children }) => {
    return <div className='Sidebar__center Sidebar__center--text Sidebar__PoweredBy'>
        <a href={ href } target='blank' className='Sidebar__link'>
            { children }
        </a>
    </div>;
}
const SidebarButton = ({ children, ...props }) => {
    return <div className='Sidebar__center'>
        <button { ...props }>
            { children }
        </button>
    </div>;
};

const getIDForUrl = url => {
    if (!url)  return null;
    const re = /https?:\/\/vid.me\/([^\/#]+)/;
    const match = url.match(re);
    if (match && match[1].length) {
        return match[1];
    }
    return null;
}
const block = (props) => {
    const { state, requestFocus, scale, focused } = props;
    if (!state.url)  return <EmptyImage { ...props } />;

    const id = getIDForUrl(state.url);
    if (id !== null) {
        return <SmartIFrame
            src={ 'https://vid.me/e/' + id }
            image={ state.thumbURL }
            video
            { ...props } />;
    } else {
        return <Error onClick={ requestFocus }>Invalid URL</Error>;
    }
};

/* Thanks http://stackoverflow.com/a/18650828 */
function formatBytes(bytes,decimals) {
   if(bytes == 0) return '0 Byte';
   var k = 1000; // or 1024 for binary
   var dm = decimals + 1 || 3;
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
   var i = Math.floor(Math.log(bytes) / Math.log(k));
   return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

class UploadToVidMe extends React.Component {
    constructor(props) {
        super(props);
        this.state = { file: undefined };

        this.onDrop = files => this.setState({
            file: files[0],
            title: files[0].name
        });

        this.startUpload = () => {
            const form = new FormData();
            form.append('filedata', this.state.file);
            form.append('size', this.state.file.size);
            form.append('filename', this.state.file.name);
            form.append('title', this.state.title);
            form.append('private', true);

            request
                .post('https://api.vid.me/video/upload')
                .set('Authorization', 'Basic nfc4foYLqaJM6NzW1edm6lZ8HaXktU1n')
                .send(form)
                .on('progress', event => {
                    this.setState({ progress: event.percent })
                })
                .end((err, resp) => {
                    // VidMe uses text/plain, so it doesn't get decoded
                    const body = JSON.parse(resp.text);
                    if (!body.status) {
                        this.setState({ error: resp.body.error });
                        return
                    }
                    this.props.onUpload({
                        url: body.url,
                        thumbURL: `https://api.vid.me/video/${ body.video.video_id }/thumbnail`
                    });
                    this.onReset();
                });
            this.setState({ progress: 0 })
        };
        this.onReset = () => {
            this.setState({
                error: undefined,
                progress: undefined,
                file: undefined,
                title: undefined
            });
        };
    }

    render() {
        if (this.state.error !== undefined) {
            return <div>
                <p>Error: { this.state.error }</p>
                <button onClick={ this.onReset }>OK</button>
            </div>;
        }
        if (this.state.progress !== undefined) {
            return <LoaderProgress progress={ this.state.progress / 100 } />;
        }
        if (this.state.file !== undefined) {
            return <div>
                <p>
                    <label>Title</label>
                    <input type='text'
                           onChange={ e => this.setState({ title: e.target.value }) }
                           value={ this.state.title } />
                </p>
                <p>
                    <label className='UploadSizeLabel'>Upload Size</label>
                    { formatBytes(this.state.file.size) }
                </p>
                <PoweredBy href='https://vid.me/terms-of-use'>
                    By uploading, you agree to the VidMe Terms
                </PoweredBy>
                <SidebarButton className='special'
                               onClick={ this.startUpload }>
                    Upload
                </SidebarButton>
            </div>;
        }
        return <StyledDropzone onDrop={ this.onDrop }
                      accept='video/*'
                      multiple={ false }>
                Select Video
        </StyledDropzone>;
    }
}

class sidebar extends React.Component {
    constructor(props) {
        super(props);

        this.onURLInputChange = event => {
            const change = { url: event.target.value };
            if (getIDForUrl(this.props.state.url) !== getIDForUrl(change.url)) {
                change.thumbURL = undefined;
                const id = getIDForUrl(change.url);
                if (id !== null) {
                    request
                        .get(`https://api.vid.me/videoByUrl/${ id }`)
                        .set('Authorization', 'Basic nfc4foYLqaJM6NzW1edm6lZ8HaXktU1n')
                        .end((err, res) => {
                            if (id !== getIDForUrl(this.props.state.url)) {
                                return;
                            }
                            this.props.onChange({
                                thumbURL: res.body.video.thumbnail_url
                            });
                        });
                }
            }
            this.props.onChange(change);
        };
    }

    render() {
        const { state, goBackTo } = this.props;
        return <BaseSidebar title='Edit VidMe Block'
                            goBackTo={ goBackTo }>
            <SidebarSection initialExpansion title='Upload'>
                <UploadToVidMe onUpload={ this.props.onChange } />
            </SidebarSection>
            <SidebarSection initialExpansion title='Video Source'>
                <input type='text'
                       value={ state.url || '' }
                       placeholder='e.g. https://vid.me/GCG0'
                       onChange={ this.onURLInputChange } />
            </SidebarSection>
            <PoweredBy href='https://vid.me'>Powered by VidMe</PoweredBy>
        </BaseSidebar>;
    }
};

export default { block, sidebar };
