/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { BaseSidebar } from './helpers.jsx';
import MediaLibrary from '../MediaLibrary.jsx';
import SidebarSection from '../SidebarSection.jsx';

const block = ({ state, requestFocus }) => {
    let src = '/static/images/emptythumb.svg';
    if (state.image && state.image.full)  src = state.image.full;
    return <img onClick={ requestFocus }
                style={{ maxWidth: (state.maxWidth || 100)+'%',
                         maxHeight: '100%' }}
                src={ src } />;
};

const sidebar = ({ state, onChange, goBackTo }) => {
    return <BaseSidebar title='Edit Image Block'
                        goBackTo={ goBackTo }>
        <SidebarSection initialExpansion title='Image Size'>
            <input type='range' value={ state.maxWidth || 100 }
                   min='0' max='100' step='1'
                   onChange={ (e) => onChange({ maxWidth: e.target.value }) } />
        </SidebarSection>

        <SidebarSection initialExpansion title='Image Source'>
            <MediaLibrary onSelect={ (image) => onChange({ image }) } />
        </SidebarSection>
    </BaseSidebar>;
};


export default { block, sidebar };
