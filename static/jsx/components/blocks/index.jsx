/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { PropTypes } from 'react';
import SidebarChangeBlockType from '../SidebarChangeBlockType.jsx';

import EmptyBlock from './Empty.jsx';
import HeadingBlock from './Heading.jsx';
import TextBlock from './Text.jsx';
import ImageBlock from './Image.jsx';
import YouTubeBlock from './YouTube.jsx';
import QuoteBlock from './Quote.jsx';
import VidMeBlock from './VidMe.jsx';

const BlockTypes = {
    empty: EmptyBlock,
    heading: HeadingBlock,
    text: TextBlock,
    image: ImageBlock,
    vidme: VidMeBlock,
    youtube: YouTubeBlock,
    quote: QuoteBlock
};

export const SlideBlock = (props) => {
    const state = props.state;
    let type = BlockTypes[props.state.type].block;

    const nextProps = Object.assign(
        {},
        props,
        { presentationStyle: props.presentationStyle || {} }
    );

    if (type !== undefined) {
        return React.createElement(
            type,
            nextProps
        );
    }
    return <p onClick={ props.requestFocus }>{ JSON.stringify(props.state) }</p>;
};
SlideBlock.propTypes = {
    state: React.PropTypes.object.isRequired,
    blockId: React.PropTypes.number.isRequired,

    scale: React.PropTypes.number.isRequired,
    editable: React.PropTypes.bool.isRequired,
    presentationStyle: React.PropTypes.object.isRequired,
    row: React.PropTypes.object.isRequired,

    onChange: React.PropTypes.func,
    requestFocus: React.PropTypes.oneOfType(
        React.PropTypes.func,
        React.PropTypes.bool
    ),
    focused: React.PropTypes.bool
};

export const SlideBlockSidebar = (props) => {
    const state = props.state;
    let type = BlockTypes[props.state.type].sidebar;
    if (type !== undefined) {
        return React.createElement(
            type,
            props
        );
    }
    return <SidebarChangeBlockType { ...props } />;
};

export const blockTypeHasSidebar = (blockType) => {
    return BlockTypes[blockType].sidebar !== undefined;
};
