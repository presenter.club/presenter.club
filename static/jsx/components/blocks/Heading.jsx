/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import QuillNoToolbar from '../QuillNoToolbar.jsx';
import { BaseSidebar } from './helpers.jsx';

import { toCSS as textEffectToCSS } from '../../constants/textEffect.jsx';

const block = ({ state, presentationStyle, row,
                 scale, onChange, requestFocus, editable }) => {
    let s = state.size;
    if (!(s === 1 || s === 2))  s = 1;
    const style = Object.assign(
        { fontSize: scale + 'em' },
        textEffectToCSS(presentationStyle['h' + s], row, scale)
    );

    let content;
    if (editable) {
        content = <div style={ style }>
            <QuillNoToolbar
                className='ql-editor-no-padding'
                noBlockFormats
                value={ state.html }
                placeholder='Type a Heading'
                onClick={ requestFocus }
                onSelect={ requestFocus }
                onChange={ (html) => onChange({ html }) } />
        </div>;
    } else {
        content = <div style={ style }
                       dangerouslySetInnerHTML={{ __html: state.html }} />;
    }
    return React.createElement(
        'h' + s,
        { onClick: requestFocus },
        content
    );
};

const sidebar = ({ state, onChange, goBackTo }) => {
    let s = state.size;
    if (!(s === 1 || s === 2))  s = 1;

    return <BaseSidebar title='Change Heading Style'
                        goBackTo={ goBackTo }>
        <div className={ 'headingblock__preview slide' +
                         (s === 1 ? ' headingblock__preview--selected' : '') }
             onClick={ () => onChange({ size: 1 }) }>
            <h1>Heading 1</h1>
        </div>
        <div className={ 'headingblock__preview slide' +
                         (s === 2 ? ' headingblock__preview--selected' : '') }
             onClick={ () => onChange({ size: 2 }) }>
            <h2>Heading 2</h2>
        </div>
    </BaseSidebar>;
};


export default { block, sidebar };
