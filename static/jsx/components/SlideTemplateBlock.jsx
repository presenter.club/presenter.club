/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { DraggableCore } from 'react-draggable';

import { SplitType } from '../constants/slidetypes.jsx';

const SNAP_POINTS = [25, 50, 75];
const SNAP_MARGIN = 5;

const DragSnapPreviews = ({ pos, vert, scaleStyle }) => {
    return <div className='DragSnapPreviews'
                style={ scaleStyle }>
        { SNAP_POINTS.map(point => {
            let cn = `DragBar DragBar--${ vert ? 'h' : 'v' }
                      DragBar--snap-preview
                      DragBar--${ vert ? 'h' : 'v' }--snap-preview`;
            if (Math.abs(point - pos) < SNAP_MARGIN) {
                cn = cn + ' DragBar--snap-preview--highlight';
            }
            const style = { top: 0, left: 0 };
            style[vert ? 'top' : 'left'] = point + '%';
            return <div
                className={ cn }
                key={ point }
                style={ style } />
        })}
    </div>;
};

class SplitBlock extends React.Component {
    constructor(props) {
        super();
        this.state = { dragging: false };

        this.onDrag = (event, { node, deltaX, deltaY }) => {
            const change = this.props.root.split === SplitType.VERTICAL ?
                deltaY / this.refs.main.clientHeight
                : deltaX / this.refs.main.clientWidth;
            let next = this.props.root.pos + change*100;

            if (event.buttons === 0) {
                SNAP_POINTS.forEach(point => {
                    if (Math.abs(next - point) < SNAP_MARGIN) {
                        next = point;
                    }
                });
                this.setState({ dragging: false });
            } else if (!this.state.dragging) {
                this.setState({ dragging: true });
            }

            this.props.onChange(Object.assign(
                {},
                this.props.root,
                { pos: next }
            ));
        };
    }

    render() {
        const { root, onChange, blocks, style, lightsOutFor,
                scale } = this.props;
        const vert = root.split === SplitType.VERTICAL;
        const key = vert ? 'height' : 'width';
        const aStyle = { [key]: (root.pos || 50) + '%' }
        const bStyle = { [key]: ((100-root.pos) || 50) + '%' }

        let cn = c => `block__split${ c } block__split--${ vert ? 'v' : 'h' }${ c }`;
        if (this.state.dragging) {
            const oldCn = cn;
            cn = c => oldCn(c) + ` block__split--dragging${ c }`;
        }

        const scaleStyle = { fontSize: scale + 'em' };

        return <div style={ style }
                    ref='main'
                    className={ cn('') }>
            { this.state.dragging ?
              <DragSnapPreviews pos={ root.pos } vert={ vert }
                                scaleStyle={ scaleStyle } />
              : null
            }
            <SlideTemplateBlock root={ root.a } style={ aStyle }
                                onChange={ onChange === null ? null
                                           : a => onChange(Object.assign({}, root, { a })) }
                                { ...{ lightsOutFor, blocks, scale } } />
            { onChange === null ?
              <div style={ scaleStyle }
                   className={ 'SplitSpacer SplitSpacer--' + (vert ? 'h' : 'v') } />
              : <DraggableCore onStart={ this.onDrag }
                               onStop={ this.onDrag }
                               onDrag={ this.onDrag }
                               axis={ vert ? 'y' : 'x' }>
                    <div style={ scaleStyle }
                         className={ 'DragBar DragBar--' + (vert ? 'h' : 'v') } />
                </DraggableCore>
            }
            <SlideTemplateBlock root={ root.b } style={ bStyle }
                                onChange={ onChange === null ? null
                                           : b => onChange(Object.assign({}, root, { b })) }
                                { ...{ lightsOutFor, blocks, scale } } />
        </div>;
    }
};

const SlideTemplateBlock = ({ root, onChange = null, scale = 0.3,
                              blocks, style, lightsOutFor }) => {
    if (root === undefined)  return <noscript />;
    if (root.split) {
        return <SplitBlock { ...{ root, onChange, blocks, scale,
                                  style, lightsOutFor } } />;
    } else {
        let cn = 'block__container ' + Object.keys(root.align || {})
            .map((key) => 'block__container--' + root.align[key])
            .reduce((a, b) => a + ' ' + b, '');
        if (lightsOutFor !== undefined) {
            if (lightsOutFor === root.block) {
                cn = cn + ' block__container--lights-on';
            } else {
                cn = cn + ' block__container--lights-out';
            }
        }
        return <div className={ cn }
                    style={ style }>
            { blocks[root.block] }
        </div>
    }
};
export default SlideTemplateBlock;
