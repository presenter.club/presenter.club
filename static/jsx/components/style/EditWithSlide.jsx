/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { connect } from 'react-redux';
import Measure from 'react-measure';

import { unwrap } from '../../reducers/style.jsx';
import { SlideInner } from '../SlideInner.jsx';
import { StaticSlideView } from '../SlideView.jsx';
import { SlideTemplateThumb } from '../SlideTemplateThumb.jsx';

import { rowForThumb, SlideExamples } from './Thumb.jsx';
import SlideEditorSidebar from '../SlideEditorSidebar.jsx';

const Previews = ({ onSelect, selected }) => {
    return <div className='EditWithSlide__previews'>
        { Object.keys(SlideExamples).map(key => {
            const e = SlideExamples[key];
            return <SlideTemplateThumb typeName={ e.slideType  }
                                       onClick={ () => onSelect(e) }
                                       selected={ e === selected } />
        })}
    </div>
};
    

class _EditWithSlide extends React.Component {
    constructor(props) {
        super(props);
        this.state = { slide: SlideExamples[this.props.defaultSlideName] };
        this.onSelectPreview = slide => this.setState({ slide });
    }

    render() {
        const row = rowForThumb(this.props.style, this.state.slide, {});

        return <div className='EditWithSlide'>
            <SlideEditorSidebar title={ this.props.title }>
                { this.props.children }
            </SlideEditorSidebar>
            <div className='EditWithSlide__right'>
                <Previews selected={ this.state.slide }
                          onSelect={ this.onSelectPreview } />
                <StaticSlideView className='SlideEditor__background'
                                 presentationStyle={ this.props.style }
                                 row={ row }>
                    <Measure>{ dimensions => {
                        const scaleHeight = (dimensions.height - 80) / 360;
                        const scaleWidth = dimensions.width / 640;
                        const scale = Math.min(scaleHeight, scaleWidth);

                        return <div className='SlideEditor__SlideInner slide-parent'>
                            <SlideInner row={ row }
                                        editable={ false }
                                        presentationStyle={ this.props.style }
                                        scale={ scale }
                                        style={{ width: scale * 640,
                                                 height: scale * 360 }} />
                        </div>;
                    }}</Measure>
                </StaticSlideView>
            </div>
        </div>;
    }
}
const EditWithSlide = connect((state) => ({
    style: unwrap(state.style).style || {}
}))(_EditWithSlide);
export default EditWithSlide;
