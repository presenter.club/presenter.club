/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { connect } from 'react-redux';

import * as StyleActions from '../../actions/style.jsx';
import { unwrap } from '../../reducers/style.jsx';
import SidebarSection from '../SidebarSection.jsx';
import EditWithSlide from './EditWithSlide.jsx';
import MediaLibrary from '../MediaLibrary.jsx';

const _EditLogo = ({ data, onWidthChange, onSelectImage }) => {
    return <EditWithSlide title='Edit Logo'
                          defaultSlideName='h1'>
        <SidebarSection initialExpansion title='Image'>
            <button className={ !data.image ? 'special' : '' }
                    onClick={ () => onSelectImage(undefined) }>
                No Logo
            </button>
            <MediaLibrary
                onSelect={ onSelectImage }
                selected={ (data.image || {}).id } />
        </SidebarSection>
        <SidebarSection initialExpansion title='Width (px)'>
            <input id='widthInput'
                   inputMode='numeric'
                   value={ data.width || 40 }
                   onChange={ onWidthChange } />
        </SidebarSection>
    </EditWithSlide>;
};
const EditLogo = connect(state => ({
    data: (unwrap(state.style).style || {}).logo || {},
}), dispatch => {
    const onSelectImage = image => {
        dispatch(StyleActions.editElement('logo', { image }));
    };
    const onWidthChange = event => {
        dispatch(StyleActions.editElement(
            'logo', { width: parseInt(event.target.value) }
        ));
    };
    return { onSelectImage, onWidthChange };
})(_EditLogo);
export default EditLogo;
