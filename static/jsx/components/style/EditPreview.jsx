import React from 'react';
import { connect } from 'react-redux';

import { unwrap } from '../../reducers/style.jsx';
import * as StyleActions from '../../actions/style.jsx';
import { ColorPalette } from '../SidebarForeground.jsx';
import SlideEditorSidebar from '../SlideEditorSidebar.jsx';
import SidebarSection from '../SidebarSection.jsx';
import { backgroundFromColor,
         colorFromBackground } from '../../constants/backgrounds.jsx';
import { BackgroundPreviews } from '../SidebarBackgrounds.jsx';
import EditWithSlide from './EditWithSlide.jsx';

const _EditPreview = ({ colors, preview, images,
                        onFgSelect, onShadowSelect, onBgSelect }) => {
    return <EditWithSlide defaultSlideName='h1'
                          title='Edit Preview Slide'>
        <SidebarSection title='Foreground'>
            <ColorPalette onSelect={ onFgSelect }
                          colors={ colors }
                          selected={ preview.slideFgText } />
        </SidebarSection>
        <SidebarSection title='Shadow'>
            <ColorPalette onSelect={ onShadowSelect }
                          colors={ colors }
                          selected={ preview.slideFgShadow } />
        </SidebarSection>
        <SidebarSection title='Background'>
            <ColorPalette colors={ colors }
                          onSelect={ c => onBgSelect(backgroundFromColor(c)) }
                          selected={ colorFromBackground(preview.background) } />
            <BackgroundPreviews data={ images }
                                onSelect={ onBgSelect }
                                selected={ preview.background } />
        </SidebarSection>
    </EditWithSlide>;
};
const EditPreview = connect((state) => {
    const style = unwrap(state.style).style || {};
    return {
        colors: style.colors || [],
        images: style.images || [],
        preview: (style.previews || {}).default || {}
    };
}, (dispatch) => {
    const onFgSelect = slideFgText => {
        dispatch(StyleActions.editPreview(
            'default', { slideFgText }
        ));
    };
    const onShadowSelect = slideFgShadow => {
        dispatch(StyleActions.editPreview(
            'default', { slideFgShadow }
        ));
    };
    const onBgSelect = background => {
        dispatch(StyleActions.editPreview(
            'default', { background }
        ));
    };
    return { onFgSelect, onShadowSelect, onBgSelect };
})(_EditPreview);
export default EditPreview;
