/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { connect } from 'react-redux';

import * as StyleActions from '../../actions/style.jsx';
import { unwrap } from '../../reducers/style.jsx';
import { TextEffects } from '../../constants/textEffect.jsx';
import SidebarSection from '../SidebarSection.jsx';
import EditWithSlide from './EditWithSlide.jsx';
import { defaults } from './CSS.jsx';
import { Select } from './EditTextElement.jsx';

const _EditQuote = ({ data, onInputChange }) => {
    return <EditWithSlide title='Edit Quote Style'
                          defaultSlideName='quote'>
        <SidebarSection initialExpansion title='Style'>
            <Select value={ data.style }
                    onChange={ onInputChange('style') }>
                <option value='big-quotes'>Big Quotes</option>
                <option value='modern'>Modern</option>
            </Select>
        </SidebarSection>
    </EditWithSlide>;
};
const EditQuote = connect(state => ({
    data: (unwrap(state.style).style || {}).quote || {},
}), dispatch => {
    const onInputChange = key => event => dispatch(
        StyleActions.editElement('quote', { [ key ]: event.target.value })
    );
    return { onInputChange };
})(_EditQuote);
export default EditQuote;
