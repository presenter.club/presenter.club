import React from 'react';
import { connect } from 'react-redux';

import { unwrap } from '../../reducers/style.jsx';
import * as StyleActions from '../../actions/style.jsx';
import { ColorPalette, ColorPaletteColor } from '../SidebarForeground.jsx';
import COLOR_PALETTES from '../../constants/colorPalettes.jsx';
import { CenterEditView } from './util.jsx';
import SlideEditorSidebar from '../SlideEditorSidebar.jsx';

const TitledColorPalette = ({ title, colors, onClick }) => {
    return <div className='TitledColorPalette clickable'
                onClick={ onClick }>
        <h3 className='TitledColorPalette__title'>{ title }</h3>
        <div className='TitledColorPalette__colors'>
            <ColorPalette onSelect={ () => onClick() } colors={ colors } />
        </div>
    </div>;
}

const AddNewColorButton = ({ onClick }) => {
    return <div onClick={ onClick }
                className='clickable ColorPalette__color ColorPalette__color--add-new'>
        <div className='ColorPalette__color--add-new__plus'>+</div>
    </div>;
}

class CurrentPalette extends React.Component {
    constructor(props) {
        super(props);
        this.state = { selected: null, v: '' };
        this.onSelect = selected => this.setState({ selected });
        this.onInputChange = event => this.setState({ v: event.target.value });
        this.addColor = () => {
            this.props.addColor(this.state.v);
            this.setState({ v: '' });
        };
        this.deleteSelected = () => {
            this.props.deleteColor(this.state.selected);
            this.setState({ selected: null });
        };
        this.onSelectedChange = event => {
            console.log(this.state.selected, event.target.value);
            this.props.replaceColor(this.state.selected, event.target.value);
            this.setState({ selected: event.target.value });
        };
        this.onKeyPress = event => {
            if (event.key === 'Enter') {
                this.addColor();
            }
        };
    }

    render() {
        let sidebar = null;
        if (this.state.selected === null) {
            sidebar = <SlideEditorSidebar title='Add Color'>
                <input type='text'
                       value={ this.state.v  }
                       onKeyPress={ this.onKeyPress }
                       onChange={ this.onInputChange } />
                <button onClick={ this.addColor }>Add</button>
            </SlideEditorSidebar>
        } else {
            sidebar = <SlideEditorSidebar title='Selected Color'>
                <ColorPaletteColor color={ this.state.selected }
                                   className='MassiveColorPreview' />
                <input type='text'
                       value={ this.state.selected  }
                       onChange={ this.onSelectedChange } />
                <button onClick={ this.deleteSelected }>Delete</button>
            </SlideEditorSidebar>
        }
        return <div className='SidebarLeft'>
            { sidebar }
            <ColorPalette colors={ this.props.colors }
                          selected={ this.state.selected }
                          onSelect={ this.onSelect }>
                <AddNewColorButton onClick={ () => this.onSelect(null) } />
            </ColorPalette>
        </div>;
    }
}

const _EditColors = ({ colors, onSelectPreset }) => {
    const addColor = c => {
        onSelectPreset(colors.concat([c]));
    };
    const deleteColor = c => {
        onSelectPreset(colors.filter(v => v !== c));
    };
    const replaceColor = (old, next) => {
        onSelectPreset(colors.map(v => v === old ? next : v));
    };
    return <CenterEditView title='Edit Color Palette'>
        <CurrentPalette colors={ colors }
                        deleteColor={ deleteColor }
                        replaceColor={ replaceColor }
                        addColor={ addColor } />

        <hr />
        <h3>Use a Preset</h3>
            { Object.keys(COLOR_PALETTES).map((key) => {
                const { colors, title } = COLOR_PALETTES[key];
                return <TitledColorPalette
                    onClick={ () => onSelectPreset(colors) }
                    key={ key }
                    title={ title }
                    colors={ colors } />
            })}
    </CenterEditView>;
};
export const EditColors = connect((state) => ({
    colors: (unwrap(state.style).style || {}).colors || []
}), (dispatch) => {
    const onSelectPreset = c => {
        dispatch(StyleActions.setColors(c));
    };
    return { onSelectPreset };
})(_EditColors);
export default EditColors;
