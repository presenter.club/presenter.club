/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { connect } from 'react-redux';

import { unwrap } from '../../reducers/style.jsx';
import * as StyleActions from '../../actions/style.jsx';

import { Toolbar, UndoButton, SaveButton } from '../AppToolbar.jsx';
import { LiveStyleCSS, defaults } from './CSS.jsx';
import Thumb, { SlideExamples } from './Thumb.jsx';

import { Center } from './util.jsx';
import EditColors from './EditColors.jsx';
import EditBackgrounds from './EditBackgrounds.jsx';
import { EditH1, EditH2, EditNormal } from './EditTextElement.jsx';
import EditQuote from './EditQuote.jsx';
import EditPreview from './EditPreview.jsx';
import EditLogo from './EditLogo.jsx';


const StylesUndoButton = connect((state) => ({
    future: state.style.content.future,
    past: state.style.content.past,
    name: 'Style'
}))(UndoButton);
const StylesSaveButton = connect((state) => ({
    saveable: state.style,
    data: unwrap(state.style),
    name: 'Style'
}))(SaveButton);

const _EditMetadata = ({ title, onChangeTitle }) => {
    return <div>
        <p>
            <label htmlFor='titleInput'>Title</label>
            <input id='titleInput'
                   type='text'
                   value={ title }
                   onChange={ event => onChangeTitle(event.target.value) } />
        </p>
    </div>;
};
const EditMetadata = connect((state) => ({
    title: unwrap(state.style).metadata.title
}), (dispatch) => {
    const onChangeTitle = title => {
        dispatch(StyleActions.editMetadata(
            { title }
        ));
    };
    return { onChangeTitle };
})(_EditMetadata);

const EditAppIndex = ({ onChangeView }) => {
    return <Center>
        <EditMetadata />
        <hr />
        <ul>
            <li onClick={ () => onChangeView('colors') }>Colors</li>
            <li onClick={ () => onChangeView('backgrounds') }>Backgrounds</li>
            <li onClick={ () => onChangeView('h1') }>H1</li>
            <li onClick={ () => onChangeView('h2') }>H2</li>
            <li onClick={ () => onChangeView('normal') }>Normal Text</li>
            <li onClick={ () => onChangeView('quote') }>Quote Block</li>
            <li onClick={ () => onChangeView('preview') }>Preview Slide</li>
            <li onClick={ () => onChangeView('logo') }>Logo</li>
        </ul>
    </Center>;
};

class EditApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = { view: null };
        this.onChangeView = view => this.setState({ view });
        this.onExit = () => this.setState({ view: null });
    }

    render() {
        let content;
        switch (this.state.view) {
          case 'colors':
            content = <EditColors />;
            break;
          case 'backgrounds':
            content = <EditBackgrounds />;
            break;
          case 'h1':
            content = <EditH1 />;
            break;
          case 'h2':
            content = <EditH2 />;
            break;
          case 'normal':
            content = <EditNormal />;
            break;
          case 'quote':
            content = <EditQuote />;
            break;
          case 'preview':
            content = <EditPreview />;
            break;
          case 'logo':
            content = <EditLogo />;
            break;
          default:
            content = <EditAppIndex onChangeView={ this.onChangeView } />;
        }

        return <div>
            <LiveStyleCSS />
            <Toolbar>
                <h1 className='toolbar__title'>
                    { this.state.view === null ?
                      <a href='..'>Home</a>
                      : <a href='#' onClick={ this.onExit }>Home</a>
                    }
                </h1>
                <StylesUndoButton />
                { this.state.view === null ? null
                  : <a onClick={ this.onExit }>Back to Styles Home</a>
                }
                <StylesSaveButton />
            </Toolbar>
            { content }
        </div>;
    }
};

export default EditApp;
