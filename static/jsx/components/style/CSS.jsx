/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { connect } from 'react-redux';
import request from 'superagent';
import { unwrap } from '../../reducers/style.jsx';
import { unwrap as unwrapPresentation } from '../../reducers/presentation.jsx';
import * as StyleCacheActions from '../../actions/stylecache.jsx';

const transformKeys = {
    fontFace: 'font-family',
    textTransform: 'text-transform',
    fontWeight: 'font-weight'
};

export const defaults = {
    h1: {
        fontFace: 'Raleway',
        textTransform: 'uppercase',
        fontWeight: 700
    },
    h2: {
        fontFace: 'Raleway',
        textTransform: 'uppercase',
        fontWeight: 700
    },
    normal: {
        fontFace: 'Arial',
        textTransform: 'none',
        fontWeight: 400
    },
    quote: {
        style: 'big-quotes'
    },
};

function blockToCss(name, data) {
    let s = `.slide ${ name } {`;
    Object.keys(data).forEach((key) => {
        if (transformKeys[key] === undefined)  return;
        if (data[key] === undefined)  return;
        const v = ('' + data[key]).split(';');
        s = s + `${ transformKeys[key] }: ${ v };`;
    });
    return s + '}';
};

const WEB_SAFE_FONTS = [
    'arial', 'serif', 'sans-serif'
];

function allGoogleFonts(stylesList) {
    return stylesList.filter(
        ({ fontFace }) => WEB_SAFE_FONTS.indexOf(fontFace.toLowerCase()) === -1
    ).map(
        ({ fontFace, fontWeight }) => `https://fonts.googleapis.com/css?family=${ fontFace.replace(' ', '+') }:${ fontWeight }`
    ).reduce(
        (a, b) => a.indexOf(b) === -1 ? a.concat([ b ]) : a,
        []
    );
}

function makeQuoteCSS(data) {
    if (data.style === 'modern') {
        return `
            blockquote:before, blockquote:after {
                display: none;
            }
            blockquote {
                background: rgba(255, 255, 255, 0.25);
                border-left: solid 0.25em rgba(255, 255, 255, 0.5);
                padding: 0.5em;
            }

            .Quote__attribution {
                border-left: solid 0.25em rgba(255, 255, 255, 0.5);
                border-bottom: solid 0.25em transparent;
                padding: 0.5em;
                position: relative;
            }
            .Quote__attribution:before {
                display: block;
                content: '';

                border-left: 3.25em solid rgba(255, 255, 255, 0.25);
                border-bottom: 3em solid transparent;

                position: absolute;
                width: 2em;
                top: 0;
                bottom: 0;
                margin-left: -0.75em;
                margin-bottom: -0.25em;
            }`;
    } else {
        return '';  // Default in CSS
    }
};

const StyleCSS = ({ data }) => {
    if (data === undefined)  data = {};
    const h1 = Object.assign({}, defaults.h1, data.h1);
    const h2 = Object.assign({}, defaults.h2, data.h2);
    const normal = Object.assign({}, defaults.normal, data.normal);
    const quote = Object.assign({}, defaults.quote, data.quote);

    const styleText = 
        blockToCss('h1', h1) +
        blockToCss('h2', h2) +
        blockToCss('',   normal) +
        makeQuoteCSS(quote);

    return <div>
        { allGoogleFonts([h1, h2, normal]).map(url =>
            <link rel='stylesheet' type='text/css' href={ url } key={ url } />
        )}
        <style dangerouslySetInnerHTML={{ __html: styleText }} />
    </div>;
};
export default StyleCSS;

export const LiveStyleCSS = connect((state) => ({
    data: unwrap(state.style).style || {}
}))(StyleCSS);

export class _DownloadingStyleCSS extends React.Component {
    constructor(props) {
        super(props);
        this.state = { loaded: false };
        this.load(this.props.style);
    }

    componentWillReceiveProps(nextProps) {
        if ((nextProps.style || {}).id !== (this.props.style || {}).id) {
            this.load(nextProps.style);
        }
    }

    load(style) {
        if (style.id === undefined) {
            style = { user: 'lol', id: 1 };
        }
        const r = request.get(`/u/${ style.user }/styles/${ style.id }`)
                         .end((err, resp) => {
            if (!(style.id === this.props.style.id
                  || (style.id === 1 && this.props.style.id === undefined)))
                return;
            if (err) {
                console.error(err);
            } else {
                this.props.finishRequest(resp.body);
            }
        });
        this.props.startRequest(r);
    }

    render() {
        if (!this.props.loaded) {
            return <noscript />;
        }
        return <StyleCSS data={ this.props.data } />;
    }
}
export const DownloadingStyleCSS = connect(
    state => state.stylecache,
    dispatch => {
        const startRequest = r => dispatch(StyleCacheActions.startRequest(r));
        const finishRequest = d => dispatch(StyleCacheActions.finishRequest(d));
        return { startRequest, finishRequest };
    }
)(_DownloadingStyleCSS);

export const PresentationDownloadingStyleCSS = connect(state => ({
    style: unwrapPresentation(state.presentation).style || {}
}))(DownloadingStyleCSS);

export const DjangoStyleCSS = () => {
    return <StyleCSS data={ window.djangosStyle } />;
};
