/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { connect } from 'react-redux';

import { unwrap } from '../../reducers/style.jsx';
import * as StyleActions from '../../actions/style.jsx';
import Loader from '../Loader.jsx';
import { Types as BackgroundTypes, GradientModes } from '../../constants/backgrounds.jsx';
import { ColorPalette } from '../SidebarForeground.jsx';
import request from 'superagent';
import { CenterEditView } from './util.jsx';
import SlideEditorSidebar from '../SlideEditorSidebar.jsx';
import SidebarSection from '../SidebarSection.jsx';
import { BackgroundPreviews, BackgroundPreview,
         ImageSearchSection } from '../SidebarBackgrounds.jsx';
import MediaLibrary from '../MediaLibrary.jsx';

class CuratedImagesSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = { text: '', loading: false };
        this.onChange = event => this.setState({ text: event.target.value });
        this.onActivate = () => {
            this.setState({ loading: true });
            request.get(`/imagesearch/curated/${ this.state.text }`)
                   .end((err, resp) => {
                if (err) {
                    console.error(err);
                } else {
                    this.props.onSelect(
                        Object.assign(
                            { type: BackgroundTypes.CURATED },
                            resp.body
                        )
                    );
                }
                this.setState({ loading: false, text: '' });
            });
        }
    }

    render() {
        if (this.state.loading)  return <Loader />;
        return <SidebarSection title='Curated Image'>
            <label>Image ID</label>
            <input type='text'
                   onChange={ this.onChange }
                   value={ this.state.text } />
            <button onClick={ this.onActivate }>Add</button>
        </SidebarSection>
    }
}

class _GradientSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = { mode: 'horizontal', a: 'black', b: 'white' };
        this.onChangeMode = event => this.setState({ mode: event.target.value });
        this.onSelectA = a => this.setState({ a });
        this.onSelectB = b => this.setState({ b });

        this.onActivate = () => {
            this.props.onSelect(Object.assign(
                this.state,
                { type: BackgroundTypes.GRADIENT }
            ));
            this.setState({ mode: 'horizontal', a: 'black', b: 'white' });
        };
    }

    render() {
        const mode = GradientModes[this.state.mode];
        return <SidebarSection title='Create a Gradient'>
            <label>Type</label>
            <div className='Select__wrapper'>
                <select value={ this.state.mode }
                        onChange={ this.onChangeMode }>
                    <option value={ 'horizontal' }>Horizontal</option>
                    <option value={ 'vertical' }>Vertical</option>
                    <option value={ 'radial' }>Radial</option>
                </select>
            </div>
            <label>{ mode.aLabel }</label>
            <ColorPalette colors={ this.props.styleColors }
                          selected={ this.state.a }
                          onSelect={ this.onSelectA } />
            <label>{ mode.bLabel }</label>
            <ColorPalette colors={ this.props.styleColors }
                          selected={ this.state.b }
                          onSelect={ this.onSelectB } />
            <button onClick={ this.onActivate }>Add</button>
        </SidebarSection>
    }
}
const GradientSection = connect((state) => ({
    styleColors: (unwrap(state.style).style || {}).colors || []
}))(_GradientSection);

const AddSidebar = ({ addImage }) => {
    return <SlideEditorSidebar title='Add a Background'>
        <ImageSearchSection onSelect={ addImage } />
        <SidebarSection title='Your Media Library'>
            <MediaLibrary onSelect={ addImage } />
        </SidebarSection>
        <CuratedImagesSection onSelect={ addImage } />
        <GradientSection onSelect={ addImage } />
    </SlideEditorSidebar>;
};

const SelectedImageSidebar = ({ selected, onDelete, onExit }) => {
    return <SlideEditorSidebar title='View Selected'
                               goBackTo={{ title: 'Add a Background',
                                           onClick: onExit }}>
        <BackgroundPreview bg={ selected } />
        <button onClick={ onDelete }>Delete</button>
        <button onClick={ onExit }>Cancel</button>
    </SlideEditorSidebar>;
};

class _EditBackgrounds extends React.Component {
    constructor(props) {
        super(props);
        this.state = { selected: null };
        this.onSelect = selected => this.setState({ selected });
        this.deleteSelected = () => {
            this.props.removeImage(this.state.selected);
            this.setState({ selected: null });
        };
    }

    render() {
        const { backgrounds, addImage } = this.props;

        const sidebar = this.state.selected === null ?
            <AddSidebar addImage={ addImage } />
            : <SelectedImageSidebar selected={ this.state.selected }
                                    onExit={ () => this.onSelect(null) }
                                    onDelete={ this.deleteSelected } />
            
        return <CenterEditView title='Edit Style Backgrounds'>
            <p>These backgrounds will be avaliable under the "style backgrounds"
               section of the background chooser.</p>
            <div className='SidebarLeft'>
                { sidebar }
                <BackgroundPreviews data={ backgrounds }
                                    selected={ this.state.selected }
                                    onSelect={ this.onSelect } />
            </div>
        </CenterEditView>;
    }
};
const EditBackgrounds = connect((state) => ({
    backgrounds: (unwrap(state.style).style || {}).images || []
}), (dispatch) => {
    const addImage = image => {
        dispatch(StyleActions.addBackgroundImage(image));
    };
    const removeImage = image => {
        dispatch(StyleActions.removeBackgroundImage(image));
    };
    return { addImage, removeImage };
})(_EditBackgrounds);
export default EditBackgrounds;
