/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { connect } from 'react-redux';

import * as StyleActions from '../../actions/style.jsx';
import { unwrap } from '../../reducers/style.jsx';
import { TextEffects } from '../../constants/textEffect.jsx';
import SidebarSection from '../SidebarSection.jsx';
import EditWithSlide from './EditWithSlide.jsx';
import { defaults } from './CSS.jsx';

export const Select = props => {
    return <div className='Select__wrapper'>
        <select { ...props } />
    </div>;
};

const EditTextElement = ({ styleData, onInputChange, type,
                           name, defaultSlideName }) => {
    const data = Object.assign({}, defaults[type], styleData);

    return <EditWithSlide title={ `Edit ${ name } Style` }
                          defaultSlideName={ defaultSlideName }>
        <SidebarSection initialExpansion title='Font'>
            <input value={ data.fontFace || '' }
                   onChange={ onInputChange('fontFace') } />
            <Select value={ data.fontWeight }
                    onChange={ onInputChange('fontWeight') }>
                <option value='200'>Light</option>
                <option value='400'>Normal</option>
                <option value='700'>Bold</option>
            </Select>
            <Select value={ data.textTransform }
                    onChange={ onInputChange('textTransform') }>
                <option value='none'>Normal</option>
                <option value='uppercase'>UPPERCASE</option>
                <option value='lowercase'>lowercase</option>
            </Select>
        </SidebarSection>
        <SidebarSection initialExpansion title='Effect'>
            <Select value={ data.textEffect }
                    onChange={ onInputChange('textEffect') }>
                <option value={ TextEffects.GLOW }>Glow (default)</option>
                <option value={ TextEffects.THREE_D }>3D</option>
                <option value={ TextEffects.LETTERPRESS }>Letterpress</option>
            </Select>
        </SidebarSection>
    </EditWithSlide>;
};

function makeForType(type, name, defaultSlideName) {
    return connect(state => ({
        styleData: (unwrap(state.style).style || {})[type] || {},
        defaultSlideName: defaultSlideName || type,
        name,
        type
    }), dispatch => {
        const onInputChange = key => event => dispatch(
            StyleActions.editElement(type, { [key]: event.target.value })
        );
        return { onInputChange };
    })(EditTextElement);
};
export const EditH1 = makeForType('h1', 'Heading 1');
export const EditH2 = makeForType('h2', 'Heading 2');
export const EditNormal = makeForType('normal', 'Normal Text', 'text');
