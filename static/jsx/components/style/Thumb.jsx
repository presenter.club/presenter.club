/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';

import { backgroundFromColor } from '../../constants/backgrounds.jsx';
import { SlideThumb } from '../SlideView.jsx';
import { SlideInner } from '../SlideInner.jsx';
import { bestContrast, distanceBetween } from '../../util/colors.jsx';

export const SLIDE_H1 = {
    slideType: 'title',
    blocks: [{
        type: 'heading',
        html: `<p>Heading 1</p>`,
        size: 1,
    }]
};
export const SLIDE_H2 = {
    slideType: 'title-and-subtitle',
    blocks: [{
        type: 'heading',
        html: `<p>Heading 1</p>`,
        size: 1,
    }, {
        type: 'heading',
        html: `<p>Heading 2</p>`,
        size: 2
    }]
};
export const SLIDE_TEXT = {
    slideType: 'fullscreen-block',
    blocks: [{
        type: 'text',
        html: `<p>Often, Presenters may</p>
               <p>put many lines of</p>
               <p>text on the slide.</p>`,
        size: 1,
    }]
};
export const SLIDE_QUOTE = {
    slideType: 'fullscreen-block',
    blocks: [{
        type: 'quote',
        html: `<p>The single most important thing you can do to dramatically
                  improve our presentations is to have a story to tell before
                  you work on your PowerPoint file</p>`,
        attr: 'Cliff Atkinson'
    }]
};
export const SlideExamples = {
    h1: SLIDE_H1,
    h2: SLIDE_H2,
    text: SLIDE_TEXT,
    quote: SLIDE_QUOTE,
};

export const rowForThumb = (style, slide, { logo = false }) => {
    let bg, color, shadow;
    if ((style.colors || []).length >= 2) {
        bg = style.colors[0];
        color = bestContrast(style.colors, bg);
        shadow = style.colors
            .map(c => ({ c, d: distanceBetween(c, bg)
                               * distanceBetween(c, color) }))
            .reduce((a, b) => a.d > b.d ? a : b)
            .c;
    } else {
        bg = 'black';
        color = 'white';
        shadow = 'white';
    }

    return Object.assign(
        {
            uuid: 'preview',
            slideType: slide.slideType,
            blocks: slide.blocks,

            background: backgroundFromColor(bg),
            slideFgShadow: shadow,
            slideFgText: color
        },
        (style.previews || {}).default
    );
};
    

const StyleThumb = ({ style, slide, logo = false }) => {
    const row = rowForThumb(style, slide, { logo });
    return <SlideThumb row={ row }
                className='big-slide-thumb'
                logo={ logo }
                presentationStyle={ style }>
        <SlideInner editable={ false }
                    scale={ 0.5 }
                    row={ row }
                    presentationStyle={ style } />
    </SlideThumb>;
}
export default StyleThumb;
