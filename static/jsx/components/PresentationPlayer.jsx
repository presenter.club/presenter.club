/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import Measure from 'react-measure';
import Mousetrap from 'mousetrap';
import { backgroundToUrl } from '../constants/backgrounds.jsx';
import VisibilityStatus from '../constants/visibilityStatus.jsx';

import { SlideInner } from './SlideInner.jsx';
import { StaticSlideView } from './SlideView.jsx';

const Overlay = ({ onClick, onMouseEnter, onMouseLeave,
                   direction, visible }) => {
    if (!visible)  onClick = () => {};

    const base = 'PresentationPlayer__overlay'
    return <div className={ `${ base } ${ base }--${ direction } `
                            + (visible ? '' : base+'--hidden' ) }
                onMouseEnter={ onMouseEnter }
                onMouseLeave={ onMouseLeave }
                onClick={ onClick }>
        <div className={ `${ base }__arrow
                          ${ base }--${ direction }__arrow ` } />
    </div>;
};
Overlay.propTypes = {
    onClick: React.PropTypes.func,
    onMouseEnter: React.PropTypes.func,
    onMouseLeave: React.PropTypes.func,
    direction: React.PropTypes.oneOf(['left', 'right']),
    visible: React.PropTypes.bool.isRequired
};


class SlideTrans extends React.Component {
    componentWillMount() {
        // We need to force the loading of the background image,
        // because Chrome won't load it if the slide is invisible
        const defaultStyle = (
            ((this.props.presentationStyle || {}).previews || {})
                .default || {}
        );
        const row = Object.assign({}, defaultStyle, this.props.row);
        const url = backgroundToUrl(row.background);
        if (url !== null) {
            const i = new Image();
            i.src = url;
        }
    }
    render() {
        const { row, index, currentIndex, scale } = this.props;
        let cn = 'SlideTrans--current';
        if (index < currentIndex)  cn = 'SlideTrans--left';
        if (index > currentIndex)  cn = 'SlideTrans--right';
        return <div className={ 'SlideTrans ' + cn }>
            <StaticSlideView row={ row }
                             presentationStyle={ this.props.presentationStyle }>
                <SlideInner row={ row }
                       visibilityStatus={ index === currentIndex ?
                            VisibilityStatus.IN_VIEW
                            : VisibilityStatus.OUT_OF_VIEW }
                       key={ 'slideinner' + index }
                       scale={ scale }
                       presentationStyle={ this.props.presentationStyle }
                       style={{ width: scale * 640,
                                height: scale * 360 }} />
            </StaticSlideView>
        </div>;
    }
}

class PresentationPlayer extends React.Component {
    constructor(props) {
        super(props);
        this.goSlide = (dir) => {
            return () => {
                const next = this.props.slide + dir;
                if (0 <= next && next < this.getSlides().length) {
                    this.props.onChangeSlide(next);
                }
            }
        };

        this.state = { overlay: true };
        this._timeout = null;
        this.onMouseEnter = (event) => {
            this.setState({ overlay: true });
            window.clearTimeout(this._timeout);
            this._timeout = null;
        };
        this.hideOverlay = () => this.setState({ overlay: false });
        this.onMouseLeave = (event) => {
            if (this._timeout !== null)  return;
            this._timeout = window.setTimeout(this.hideOverlay, 1500);
        };
    }

    getSlides() {
        return this.props.presentation.speech
            .filter(({ slideType }) => slideType !== null);
    }

    componentDidMount() {
        Mousetrap.bind('left', this.goSlide(-1));
        Mousetrap.bind('right', this.goSlide(+1));

        if (this._timeout === null) {
            this._timeout = window.setTimeout(this.hideOverlay, 1500);
        }
    }

    componentWillUnmount() {
        Mousetrap.unbind('left');
        Mousetrap.unbind('right');

        window.clearTimeout(this._timeout);
        this._timeout = null;
    }

    render() {
        const scaleHeight = this.props.height / 360;
        // 200px is the width of 2 sidebar buttons - which would overlap with
        // iframe controls if we didn't do this change
        const scaleWidth = (this.props.width - 200) / 640;
        const scale = Math.min(scaleHeight, scaleWidth);

        const slides = this.getSlides();
        const atLastSlide = this.props.slide === slides.length-1;

        let cn = 'PresentationPlayer';
        if (this.props.fullscreen)  cn = cn+' PresentationPlayer--fullscreen';
        else  cn = cn + ' PresentationPlayer--notfullscreen';
        cn = cn + ' PresentationPlayer--trans-'
                + (this.props.presentation.transition || 'none');

        return <div className={ cn }>
            <Overlay onClick={ this.goSlide(-1) } direction='left'
                     onMouseEnter={ this.onMouseEnter }
                     onMouseLeave={ this.onMouseLeave }
                     visible={ this.props.slide > 0 && this.state.overlay } />
            <Overlay onClick={ this.goSlide(+1) } direction='right'
                     onMouseEnter={ this.onMouseEnter }
                     onMouseLeave={ this.onMouseLeave }
                     visible={ !atLastSlide && this.state.overlay } />

            { slides.map((row, index) => {
                return <SlideTrans { ...{ index, row, scale } }
                                   key={ index }
                                   presentationStyle={ this.props.presentationStyle }
                                   currentIndex={ this.props.slide } />
            })}
        </div>;
    }
};
PresentationPlayer.propTypes = {
    presentation: React.PropTypes.object.isRequired,
    slide: React.PropTypes.number.isRequired,
    onChangeSlide: React.PropTypes.func.isRequired,
    width: React.PropTypes.number.isRequired,
    height: React.PropTypes.number.isRequired,
    fullscreen: React.PropTypes.bool,
    presentationStyle: React.PropTypes.object.isRequired,
};
export default PresentationPlayer;
