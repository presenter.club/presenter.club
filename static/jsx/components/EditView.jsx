/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { connect } from 'react-redux';

import { unwrap } from '../reducers/presentation.jsx';
import * as EditViewActions from '../actions/editview.jsx';
import * as PresentationActions from '../actions/presentation.jsx';
import Icons from '../constants/icons.jsx';
import Privacy from '../constants/privacy.jsx';
import { THEMES } from '../constants/themes.jsx';
import AppToolbar from './AppToolbar.jsx';
import SpeechView from './SpeechView.jsx';
import StyleChooser from './StyleChooser.jsx';

import { PresentationEditorOnboarding } from './onboarding/index.jsx';
import { PresentationDownloadingStyleCSS } from './style/CSS.jsx';

const _ThemeSelector = ({ dispatch, theme }) => {
    return <p>
        <label htmlFor='id_theme'>Theme</label>
        <div className='ThemeSelector'>
            { Object.keys(THEMES).map((key) =>
                <div className='ThemeSelector__theme'>
                    { key }
                </div>
            )}
        </div>
    </p>;
};
const ThemeSelector = connect((state) => ({
    theme: unwrap(state.presentation).theme || 'presenterclub',
}))(_ThemeSelector);

const _TransitionSelector = ({ dispatch, transition }) => {
    const onChange = (event) => {
        dispatch(PresentationActions.editRoot({
            transition: event.target.value
        }));
    }

    return <p>
        <label htmlFor='id_transitionselector'>Slide Transition</label>
        <div className='Select__wrapper'>
            <select id='id_transitionselector'
                    value={ transition }
                    onChange={ onChange }>
                <option value='none'>No Transition</option>
                <option value='fade'>Fade</option>
                <option value='zoom'>Zoom</option>
                <option value='slide'>Slide</option>
                <option value='fastslide'>Fast Slide</option>
                <option value='stack'>Card Stack</option>
            </select>
        </div>
    </p>
};
const TransitionSelector = connect((state) => ({
    transition: unwrap(state.presentation).transition || 'none'
}))(_TransitionSelector);

const Section = ({ n = 1, children }) => {
    let cn = `wrapper spotlight style${ n } force-left`;
    if (n % 2 == 0) cn = cn + ' alt';
    return <div className={ cn }>
        <div className='inner'>
            <div className='Overlay__content'>
                { children }
            </div>
        </div>
    </div>;
};

const _SectionMetadata = ({ title, privacy, dispatch }) => {
    const onChangeMetadata = (thing) => {
        return (event) => {
            dispatch(PresentationActions.editMetadata({
                [thing]: event.target.value
            }));
        }
    };
    const userIsPro = window.djangosUser.isPro;

    return <Section>
        <p>
            <label htmlFor='id_name'>Title</label>
            <input type='text' id='id_name'
                   value={ title || '' }
                   onChange={ onChangeMetadata('title') } />
        </p>
        <p>
            <label htmlFor='id_privacy'>Privacy</label>
            <div className='Select__wrapper'>
                <select id='id_privacy'
                        value={ privacy }
                        onChange={ onChangeMetadata('privacy') }>
                    <option value={ Privacy.NOTES_HIDDEN }>
                        Public Slides, Private Notes
                    </option>
                    <option value={ Privacy.PUBLIC }>
                        Public Slides & Notes
                    </option>
                    { userIsPro ? 
                        <option value={ Privacy.PRIVATE }>
                            Private Slides & Notes (Pro)
                        </option>
                        : <option disabled>
                            Private Slides & Notes (Pro)
                        </option>
                    }
                </select>
            </div>
        </p>
        { userIsPro ? null
            : <p>With Presenter Club Pro, you create make your presentation
                 (both slides and notes) private.  <a href='/plans'>Learn
                 more about Pro,</a> or <a href='/go-pro'>go pro now</a>.</p>
        }
        <TransitionSelector />
    </Section>;
}
const SectionMetadata = connect((state) => ({
    title: unwrap(state.presentation).metadata.title,
    privacy: unwrap(state.presentation).metadata.privacy
}))(_SectionMetadata);

const _SectionStyle = ({ style, onSelect }) => {
    const userIsPro = window.djangosUser.isPro;

    return <Section n={ 2 }>
        <StyleChooser style={ style } onSelect={ onSelect } />
    </Section>;
}
const SectionStyle = connect(state => ({
    style: unwrap(state.presentation).style
}), dispatch => {
    const onSelect = style => {
        dispatch(PresentationActions.editRoot({ style }));
    };
    return { onSelect };
})(_SectionStyle);

const Overlay = ({ visible }) => {
    /* Is this the react way? */
    if (visible) {
        document.body.classList.add('Overlay--visible__body');
    } else {
        document.body.classList.remove('Overlay--visible__body');
    }

    const close = (event) => {
        dispatch(EditViewActions.setOverlay(false));
    }

    return <div className={ 'Overlay' + (visible ? '' : ' Overlay--hidden') }>
        <SectionMetadata />
        <SectionStyle />
    </div>;
};


const StyleCacheSpeechView = connect(state => ({
    presentationStyle: state.stylecache.data
}))(SpeechView);

const _EditView = ({ overlay }) => {
    return <div>
        <PresentationDownloadingStyleCSS />
        <AppToolbar />
        <PresentationEditorOnboarding />
        <Overlay visible={ overlay } />
        <StyleCacheSpeechView editable={ true } />
    </div>;
};
const EditView = connect((state) => ({
    overlay: state.editview.overlay
}))(_EditView);
export default EditView;
