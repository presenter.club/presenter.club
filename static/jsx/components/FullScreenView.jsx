/*
 * Presenter Club - Make Great Presentations, Faster
 * Copyright (C) 2016 Sam Parkinson <sam@sam.today>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import screenfull from 'screenfull';

class FullScreenView extends React.Component {
    constructor(props) {
        super(props);
        this.fullscreenChange = () => {
            if (!screenfull.isFullscreen)  this.props.onClose();
        }

        this.state = { width: window.innerWidth, height: window.innerHeight };
        this.onResize = () => {
            this.setState({ width: window.innerWidth,
                            height: window.innerHeight });
        }
    }

    componentDidMount() {
        document.body.classList.add('FullScreenView__body');
        screenfull.request();
        document.addEventListener(
            screenfull.raw.fullscreenchange,
            this.fullscreenChange
        );
        document.addEventListener(
            'resize',
            this.onResize
        );
    }

    componentWillUnmount() {
        document.body.classList.remove('FullScreenView__body');
        document.removeEventListener(
            screenfull.raw.fullscreenchange,
            this.fullscreenChange
        );
        document.removeEventListener(
            'resize',
            this.onResize
        );
    }

    render() {
        return <div ref={ (e) => this._e = e }
                    className='FullScreenView'>
            { this.props.children(this.state) }
        </div>
    }
};
FullScreenView.propTypes = {
    onClose: React.PropTypes.func,
    children: React.PropTypes.func
}
export default FullScreenView;
