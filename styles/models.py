# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
from django.db import models

from django.contrib.auth.models import User


DEFAULT_STYLE = 1  # Whatever


def thumb_directory_path(instance, filename):
    return 'style_thumbs/{0}/{1}.png'.format(
        instance.user.id, instance.id)


class Style(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    created_time = models.DateTimeField('Created', auto_now_add=True)
    edited_time = models.DateTimeField('Edited', auto_now=True)

    title = models.TextField('Title (publicly displayed)')
    data = models.TextField(default='{}')

    thumb = models.FileField(upload_to=thumb_directory_path, null=True)
    thumb_edited_time = models.DateTimeField(
        'When the thumbnail was last updated',
        null=True)

    def to_json(self):
        style = json.loads(self.data)
        return {
            'metadata': {
                'title': self.title
            },
            'style': style
        }

    designer = models.BooleanField(
        'is a designer style (public for all)',
        default=False)
    designer_order = models.IntegerField(
        'ordering for designer styles',
        default=1000)
