# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.urlresolvers import reverse
from django.core.files.base import ContentFile
from background_task import background

from u.tasks import GENTHUMB_KEY, get_webview 
from styles.models import Style

@background(queue='thumbnails')
def generate_thumbnail(styleid, host):
    try:
        style = Style.objects.get(id=styleid)
    except Style.DoesNotExist:
        return

    if style.thumb_edited_time == style.edited_time:
        print('Thumb update not needed')
        return 

    url = 'http://' + host + reverse('styles:genthumb',
                         args=[style.user.username,
                               style.id,
                               GENTHUMB_KEY])
    with get_webview() as wv:
        thumb = wv.load(url)
    style.thumb.delete(save=False)
    style.thumb.save('', ContentFile(thumb))
    style.thumb_edited_time = style.edited_time
    style.save()
