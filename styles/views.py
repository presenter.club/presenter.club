# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.staticfiles.storage import staticfiles_storage

from u.decorators import personal_user_view
from u.models import UserProfile, is_pro_user
from u.views import json_escape
from styles.models import Style
from styles.tasks import generate_thumbnail, GENTHUMB_KEY

@personal_user_view
def index(request, user):
    styles = Style.objects.filter(user=user).order_by('-edited_time')
    return render(request, 'styles/index.html',
                  context={'styles': styles,
                           'is_pro': is_pro_user(user)})

def style_readonly_view(wrapping):
    puv = personal_user_view(wrapping)
    def f(request, username, styleid):
        s = get_object_or_404(Style, id=styleid)
        if s.designer or s.id == 1:
            return wrapping(request, username, s)
        else:
            return puv(request, username, s)
    return f

@personal_user_view
def api(request, user):
    def short_json(s):
        return {'id': s.id, 'user': s.user.username, 'title': s.title}
    mine = Style.objects.filter(user=user).order_by('-edited_time')
    j_mine = [short_json(s) for s in mine]
    designer = Style.objects.filter(designer=True).order_by('designer_order')
    j_designer = [short_json(s) for s in designer]
    return JsonResponse({'mine': j_mine, 'designer': j_designer})

@style_readonly_view
def get(request, username, s):
    return JsonResponse(s.to_json())

@personal_user_view
def create(request, user):
    s = Style(title='New Style', user=user)
    s.save()
    return HttpResponseRedirect(
        reverse('styles:edit', args=[user.username, s.id]))

@personal_user_view
def edit(request, user, styleid):
    s = get_object_or_404(Style, id=styleid)
    if s.user != request.user:
        raise PermissionDenied
    profile = get_object_or_404(UserProfile, user=s.user)

    if request.method == 'GET':
        json_string = json_escape(json.dumps(s.to_json()))
        return render(request, 'styles/app.html',
                      context={'style': s,
                               'profile': profile,
                               'json_string': json_string})
    else:
        j = json.loads(request.body.decode('utf8'))

        s.title = j['metadata']['title']
        s.data = json.dumps(j['style'])
        s.save()

        generate_thumbnail(s.id, request.get_host())

        return JsonResponse({'success': True})

def genthumb(request, username, styleid, key):
    if key != GENTHUMB_KEY:
        raise PermissionDenied

    s = get_object_or_404(Style, id=styleid)
    json_string = json_escape(json.dumps(s.to_json()))
    return render(request, 'styles/thumb.html',
                  context={'json_string': json_string})

@style_readonly_view
def thumb(request, username, s):
    if not s.thumb:
        return HttpResponseRedirect(
            staticfiles_storage.url('images/emptythumb.svg'))
    return HttpResponseRedirect(s.thumb.url)
