# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from django.test import TestCase

from u.tests import TestUserTestCase
from styles.models import Style


class StylesIndexTestCase(TestUserTestCase):
    def test_requires_login(self):
        resp = self._c.get('/u/test/styles/')
        self.assertRedirects(resp, '/accounts/login/?next=/u/test/styles/')

    def test_requires_correct_user(self):
        resp = self._logged_in.get('/u/NOTtest/styles/')
        self.assertEqual(resp.status_code, 403)

    def test_exsists(self):
        resp = self._logged_in.get('/u/test/styles/')
        self.assertEqual(resp.status_code, 200)

    def test_lists_my_styles(self):
        title = 'THIS IS MY STYLE 80698t4i'
        s = Style(user=self.user, title=title)
        s.save()
        resp = self._logged_in.get('/u/test/styles/')
        self.assertContains(resp, title)


class CreateEditStyleTestCase(TestUserTestCase):
    def test_create(self):
        resp = self._logged_in.get('/u/test/styles/create')
        self.assertEqual(resp.status_code, 302)
        self.assertIn('/u/test/styles/', resp.url)
        self.assertIn('/edit', resp.url)

    def test_requires_login(self):
        resp = self._c.get('/u/test/styles/create')
        self.assertRedirects(resp, '/accounts/login/?next=/u/test/styles/create')

    def test_edit(self):
        s = Style(user=self.user, title='Title TEST')
        s.save()
        resp = self._logged_in.get('/u/test/styles/%s/edit' % s.id)
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'Title TEST')

    def test_edit_requires_good_user(self):
        s = Style(user=self.other_user, title='fail')
        s.save()
        resp = self._logged_in.get('/u/test/styles/%s/edit' % s.id)
        self.assertEqual(resp.status_code, 403)


class GetStyleTestCase(TestUserTestCase):
    def test_get(self):
        s = Style(user=self.user, title='Title TEST')
        s.save()
        resp = self._logged_in.get('/u/test/styles/%s' % s.id)
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'Title TEST')


class DesignerStylesTestCase(TestUserTestCase):
    def test_get(self):
        s = Style(user=self.other_user, designer=True, title='Title TEST')
        s.save()
        resp = self._logged_in.get('/u/test/styles/%s' % s.id)
        self.assertEqual(resp.status_code, 200)

    def test_listed(self):
        s = Style(user=self.other_user, designer=True, title='Title TEST')
        s.save()
        resp = self._logged_in.get('/u/test/styles/api')
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'Title TEST')
        self.assertContains(resp, s.id)

    def test_not_designer_not_listed(self):
        s = Style(user=self.other_user, designer=False, title='Title2TEST')
        s.save()
        resp = self._logged_in.get('/u/test/styles/api')
        self.assertEqual(resp.status_code, 200)
        self.assertNotContains(resp, 'Title2TEST')
        self.assertNotContains(resp, s.id)

    def test_style_1(self):
        resp = self._logged_in.get('/u/lol/styles/1')
        self.assertEqual(resp.status_code, 200)
