# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
from django import template
from u.views import json_escape
from styles.models import Style

register = template.Library()

@register.filter(is_safe=True)
def djangosStyle(presentation):
    if presentation.get_style_id() is None:
        return ''

    s = Style.objects.get(id=presentation.get_style_id())
    json_string = json.dumps(s.to_json())
    return 'window.djangosStyle = ({}).style'.format(json_string)
