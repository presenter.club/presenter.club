# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import url

from . import views

app_name = 'styles'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^api$', views.api, name='api'),
    url(r'^create$', views.create, name='create'),
    url(r'^(?P<styleid>\d+)$', views.get, name='get'),
    url(r'^(?P<styleid>\d+)/edit$', views.edit, name='edit'),
    url(r'^(?P<styleid>\d+)/thumb.png$', views.thumb, name='thumb'),
    url(r'^(?P<styleid>\d+)/genthumb/(?P<key>.+)$',
        views.genthumb, name='genthumb'),
]
