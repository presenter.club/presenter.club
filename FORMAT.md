# Presenter Club format documentation

This documents the format for the presentations.  The format is JSON based.
To view the JSON for any presentation on Presenter Club, go the view
presentation page:

    https://www.presenter.club/u/news/58/view

Then append ".json" to the url:

    https://www.presenter.club/u/news/58/view.json

The presentation may have some information removed depending on your access
rights.  If the presentation is set to "notes hidden", and you are not viewing
the presentation, the "execution" and "idea" text will not be available.

# Format

The root object is as follows:

```
{
    "metadata": object,
    "transition": string or undefined,
    "style": object or undefined,
    "speech": list of SpeechRows,
}
```

## Metadata

The metadata object is as follows:

```
{
    "privacy": 1 charecter string - one of the privacy choices,
    "title": string - user defined plain text (not html)
}
```

The privacy choices are (from `u.models.Presentation.PRIVACY_CHOICES

| code | description |
|------|:------------|
| `h`  | Public Slides, Private Notes (default for FREE members) |
| `l`  | Private (default for PRO members) |
| `o`  | Public |

For example, this is a presentation with the notes hidden privacy setting:

```
{
    "privacy": "h",
    "title": "Shooting Star (New Style)"
}
```

## Transition

The transition used between the slides.  If this is `undefined`, the none
option is used.  The following values are supported:

| string value | description |
|--------------|:------------|
| `none`       | No Transition |
| `fade`       | Crossfade between slides |
| `zoom`       | Zoom in to the next slide |
| `slide`      | Slide from left to right |
| `fastslide`  | Fade and partially slide from left to right |
| `stack`      | Stack of cards being rotated off |

## Style

If `undefined`, the default style is used.  The default style is embedded in
the javascript code.  It matches the Presnter Club style on www.presenter.club.

If style is an object, it is in the format:

```
{
    "user": string - username of creator,
    "id": int - id of style resource,
    "title": string - plaintext title of style
}
```

For example, this is one style on www.presenter.club:

```
{
    "user": "sam",
    "id": 2,
    "title": "Shooting Star"
}
```

## Speech

The speech is the main part of the presentation.  It is an ordered list of
SpeechRows.  A SpeechRow is one row in the Presenter Club editor.  It can
contain an idea, elaboration (called `execution` in the code) and optionally
a slide.

SpeechRows follow the following format:

```
{
    "uuid": string - unique across all SpeechRows in a presentation,

    "idea": html string or null,
    "execution": html string or null,
    "indent": int - level of dot point indentation - 0 or null is a root level dot point

    "slideType": null or string or SlideType object,
    "background": background object or undefined if slideType == null,
    "blocks": list of Blocks or undefined,
    "slideFgText": css color string - slide text color
    "slideFgShadow": css color string - slide text glow color
}
```

A html string is cleaned before it is saved in the database.  It can only
contain basic tags, such as `<p>`, `<i>` and `<b>`.

### SlideType

If the slide type is `null`, no slide is attached to the SpeechRow.

If the slide type is a string, it refers to a built in preset.  The presets are
defined in `static/jsx/constants/slidetypes.jsx`.

The object is defined as:

```
{
    "suggestedBlocks": list of suggested blocks - see below,
    "root": ContentBlock or SplitBlock
}
```

Suggested blocks are either `null` for no suggestion, `"h1"` or `"h2"` for
the respective header suggestions.  There must be as many suggested blocks
are referenced in the root.

A SplitBlock is:

```
{
    "a": ContentBlock or SplitBlock,
    "b": ContentBlock or SplitBlock,
    "split": "h" or "v" - "h" means a/b are left/right - "v" means a/b are top/bottom,
    "pos": int - 0 to 100 position - left to right or top to bottom based on "split"
}
```

A ContentBlock is:

```
{
    "block": int - referencing a block in the SlideRow's "blocks" and in the
                   "suggestedBlocks" array,
    "align": {
        "x": "left" or "center" or "right",
        "y": "top" or "middle" or "bottom",
    }
}
```

### Background

The background object can vary based on type.

* Color - `{ "type": "color", "color": css color string }`
* See `static/jsx/constants/backgrounds.jsx` for a complete list


### Blocks

Blocks contain content that is arranged within the slide type.  The list of
blocks may exceed the number of blocks in the slide type, to prevent ever
loosing user data.

A Block always an object.  It will contain a type property, marking which
code in `static/jsx/components/blocks/` should handle it.  They include:

#### Heading

Displays a heading, following the format:

```
{
    "type": "heading",
    "size": int - 1 or 2 representing h1 or h2 respectively - defaults to 1,
    "html": html string
}
```

As above, the html string is limited to basic tags like bold and italics.

#### Text

Displays some text, possible multiple lines, following the format:

```
{
    "type": "text",
    "size": float - text size in scaled em - defaults to 1,
    "html": html string
}
```

For example,

```
{
    "type": "text",
    "size": 1.3,
    "html": "<p>Hello World</p>"
}
```

#### Image

Displays a picture, following the format:

```
{
    "type": "image",
    "maxWidth": int percent - defaults to 100,
    "image": MediaLibraryImage
}
```

The MediaLibraryImage must have at least a "full" and "thumb" url property.

maxWidth is used to scale the image.  If it is 100, the image will fill the
width of the area allocated.  If the image is too low resolution, it will not
fill the width.
