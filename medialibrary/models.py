# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from uuid import uuid4
from django.db import models
from django.contrib.auth.models import User

from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill


def user_space_usage(user):
    return Image.objects.filter(user=user) \
                        .aggregate(models.Sum('full_size')) \
                        ['full_size__sum'] or 0


def image_directory_path(instance, filename):
    return 'medialibrary/{0}/{1}/{2}'.format(
        instance.user.id, uuid4(), filename)


def get_user_quota(user):
    return 250 * (2**20)


class Image(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    created_time = models.DateTimeField('Created', auto_now_add=True)

    full = models.ImageField(upload_to=image_directory_path)
    full_size = models.PositiveIntegerField('File size in bytes')
    thumb = ImageSpecField(source='full',
                           processors=[ResizeToFill(192, 108)],
                           format='JPEG',
                           options={'quality': 60})

    def save(self, *args, **kwargs):
        self.full_size = self.full.size
        super().save(*args, **kwargs)

    def to_json(self):
            return {'id': self.id,
                    'thumb': self.thumb.url,
                    'full': self.full.url}
