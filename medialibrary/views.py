# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django import forms
import PIL

from medialibrary.models import Image, user_space_usage, get_user_quota
from u.decorators import personal_user_view


class ImageUploadForm(forms.Form):
    image = forms.ImageField(label='Upload an image')


def _bytes_to_mib(b):
    return round(b / (2**20), 1)


_SUPPORTED_FORMATS = ['JPEG', 'PNG', 'GIF', 'WEBP']
def is_good_image(f):
    try:
        im = PIL.Image.open(f)
        if im.format not in _SUPPORTED_FORMATS:
            raise False
    except Exception as e:
        # PIL doesn't recognize it as an image
        return False

    f.seek(0)
    return True

@personal_user_view
def index(request, user):
    user_bytes = user_space_usage(user)
    user_quota = get_user_quota(user)
    if request.method == 'POST':
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            size = request.FILES['image'].size
            if user_bytes + size > user_quota:
                form.add_error('image', 'You have exceeded your quota')
            elif not is_good_image(request.FILES['images']):
                form.add_error(
                    'image',
                    'Only {} images are supported'.format(
                        ', '.join(_SUPPORTED_FORMATS)
                    )
                )
            else:
                user_bytes += size
                img = Image(full=request.FILES['image'], user=user)
                img.save()
                form = ImageUploadForm()
    else:
        form = ImageUploadForm()

    images = Image.objects.filter(user=user).order_by('-created_time')
    return render(request, 'medialibrary/index.html',
                  context={'form': form, 'images': images,
                           'space': _bytes_to_mib(user_bytes),
                           'max_space': int(_bytes_to_mib(user_quota))})

@personal_user_view
def api_index(request, user):
    just_uploaded = None
    if request.method == 'POST':
        user_bytes = user_space_usage(user)
        user_quota = get_user_quota(user)
        size = request.FILES['image'].size
        if user_bytes + size > user_quota:
            return JsonResponse({'error': 'quota'})
        elif not is_good_image(request.FILES['image']):
            return JsonResponse({'error': 'unsupported',
                                 'supported': _SUPPORTED_FORMATS})
        else:
            img = Image(full=request.FILES['image'], user=user)
            img.save()
            just_uploaded = img.to_json()

    images = Image.objects.filter(user=user).order_by('-created_time')
    l = []
    for image in images:
        l.append(image.to_json())
    return JsonResponse({'images': l,
                         'justUploaded': just_uploaded})
