# Presenter Club - Make Great Presentations, Faster
# Copyright (C) 2016 Sam Parkinson <sam@sam.today>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from django.test import TestCase
from django.core.files.uploadedfile import SimpleUploadedFile

from u.tests import TestUserTestCase
from medialibrary.models import Image


_TEST_IMAGE_PATH = 'static/images/test-image.png'
with open(_TEST_IMAGE_PATH, 'rb') as f:
    _TEST_IMAGE = SimpleUploadedFile('test-image.png', f.read(),
                                     content_type='image/png')


class MediaLibraryTestCase(TestUserTestCase):
    def test_requires_login(self):
        resp = self._c.get('/u/test/medialibrary/')
        self.assertRedirects(resp,
                             '/accounts/login/?next=/u/test/medialibrary/')

class MediaLibraryApi(TestUserTestCase):
    def test_403_loggedout(self):
        resp = self._c.get('/u/test/medialibrary/api/')
        self.assertEqual(resp.status_code, 302)
        self.assertTrue(resp.url.startswith('/accounts/login'))

    def test_empty_response(self):
        resp = self._logged_in.get('/u/test/medialibrary/api/')
        self.assertEqual(resp.status_code, 200)
        self.assertJSONEqual(
            str(resp.content, encoding='utf8'),
            {'images': [], 'justUploaded': None}
        )

    def test_upload_image_empty_account(self):
        with self.assertRaises(Image.DoesNotExist):
            Image.objects.get(user=self.user)
        resp = self._logged_in.post('/u/test/medialibrary/api/',
                                    {'image': open(_TEST_IMAGE_PATH, 'rb')})
        self.assertEqual(resp.status_code, 200)

    def test_upload_image_just_uploaded(self):
        with self.assertRaises(Image.DoesNotExist):
            Image.objects.get(user=self.user)
        resp = self._logged_in.post('/u/test/medialibrary/api/',
                                    {'image': open(_TEST_IMAGE_PATH, 'rb')})
        self.assertEqual(resp.status_code, 200)
        j = json.loads(resp.content.decode())
        self.assertIsNotNone(j['justUploaded'])

    def test_index_one_image(self):
        img = Image(user=self.user, full=_TEST_IMAGE)
        img.full = _TEST_IMAGE
        img.save()

        resp = self._logged_in.get('/u/test/medialibrary/api/')
        self.assertEqual(resp.status_code, 200)
        j = json.loads(resp.content.decode())
        self.assertEqual(len(j['images']), 1)
        i = j['images'][0]
        self.assertEqual(i['id'], img.id)
        self.assertEqual(i['full'], img.full.url)
        self.assertEqual(i['thumb'], img.thumb.url)
